ALTER TABLE user_registration ADD COLUMN profile_pic VARCHAR(255);
ALTER TABLE company_details ADD COLUMN company_logo VARCHAR(255) DEFAULT "/assets/images/50X50.png";
ALTER TABLE job_post_details ADD COLUMN job_description TEXT;
ALTER TABLE user_registration ADD COLUMN last_login DATETIME;