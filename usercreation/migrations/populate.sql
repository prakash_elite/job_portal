INSERT INTO user_map (user_category) VALUES ("admin");
INSERT INTO user_map (user_category) VALUES ("jobseeker");
INSERT INTO user_map (user_category) VALUES ("recruiter");
INSERT INTO user_map (user_category) VALUES ("service provider");

INSERT INTO gender (gender) VALUES ("male");
INSERT INTO gender (gender) VALUES ("female");
INSERT INTO gender (gender) VALUES ("not specified");


INSERT INTO employment_type (type) VALUES ("full time");
INSERT INTO employment_type (type) VALUES ("Contract");
INSERT INTO employment_type (type) VALUES ("internship");


INSERT INTO course_type (type) VALUES ("full time");
INSERT INTO course_type (type) VALUES ("part time");
INSERT INTO course_type (type) VALUES ("correspondence");

INSERT INTO desired_job_type (type) VALUES ("permananet");
INSERT INTO desired_job_type (type) VALUES ("temporary");
INSERT INTO desired_job_type (type) VALUES ("both");

INSERT INTO highest_education (education_level) VALUES ("high school");
INSERT INTO highest_education (education_level) VALUES ("bachelors");
INSERT INTO highest_education (education_level) VALUES ("graduate");


INSERT INTO user_registration (username,password,email,user_map_id) VALUES('admin','f720fa4f08f58eab018c1383cca8c600','admin@admin.admin',1);