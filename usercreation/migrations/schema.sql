SET sql_mode = '';
DROP TABLE IF EXISTS user_map;
DROP TABLE IF EXISTS user_registration;
DROP TABLE IF EXISTS country;
DROP TABLE IF EXISTS city;
DROP TABLE IF EXISTS industrydetails;
DROP TABLE IF EXISTS function_area;
DROP TABLE IF EXISTS skill_set;
DROP TABLE IF EXISTS languages;
DROP TABLE IF EXISTS gender;
DROP TABLE IF EXISTS job_seeker_details;
DROP TABLE IF EXISTS job_seeker_work_experience;
DROP TABLE IF EXISTS employment_type;
DROP TABLE IF EXISTS course_type;
DROP TABLE IF EXISTS desired_job_type;
DROP TABLE IF EXISTS job_post_details;
DROP TABLE IF EXISTS highest_education;
DROP TABLE IF EXISTS company_details;
DROP TABLE IF EXISTS jobseeker_applied_job;
DROP TABLE IF EXISTS job_seeker_details_dump;
DROP TABLE IF EXISTS company_user_map;
DROP TABLE IF EXISTS company_contact_map;
DROP TABLE IF EXISTS company_contact_info;

CREATE TABLE user_map(
  id INT AUTO_INCREMENT PRIMARY KEY,
  user_category VARCHAR(25),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE user_registration(
  id INT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(255),
  password VARCHAR(255),
  email VARCHAR(255),
  user_map_id INT,
  is_active INT DEFAULT 1,
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE country(
  id INT AUTO_INCREMENT PRIMARY KEY,
  country VARCHAR(255),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE city(
  id INT AUTO_INCREMENT PRIMARY KEY,
  country_id INT,
  city VARCHAR(255),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE industrydetails(
  id INT AUTO_INCREMENT PRIMARY KEY,
  industry VARCHAR(100),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE function_area(
  id INT AUTO_INCREMENT PRIMARY KEY,
  function_area VARCHAR(100),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE skill_set(
  id INT AUTO_INCREMENT PRIMARY KEY,
  skill_set VARCHAR(100),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE languages(
  id INT AUTO_INCREMENT PRIMARY KEY,
  language VARCHAR(100),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE gender(
  id INT AUTO_INCREMENT PRIMARY KEY,
  gender VARCHAR(100),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE job_seeker_details(
  id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT,
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  gender INT DEFAULT 1,
  DOB DATETIME,
  contact_number VARCHAR(100),
  profile_pic VARCHAR(255),
  martial_status INT DEFAULT 1,
  address_line_1 VARCHAR(255),
  address_line_2 VARCHAR(255),
  state VARCHAR(255),
  pincode INT,
  city INT,
  country INT,
  countries_authorized_to_work VARCHAR(255),
  languages_known VARCHAR(255),
  current_designation VARCHAR(255),
  current_company VARCHAR(255),
  current_CTC VARCHAR(255),
  expected_CTC VARCHAR(255),
  duration_notice_period INT,
  serving_notice_period INT DEFAULT 1,
  skills VARCHAR(255),
  current_location VARCHAR(255),
  last_working_day DATETIME,
  current_industry VARCHAR(255),
  current_functional_area VARCHAR(255),
  course VARCHAR(255),
  specialization VARCHAR(255),
  university_or_college VARCHAR(255),
  course_type INT,
  passing_year INT,
  highest_education INT,
  desired_employment_type INT,
  desired_job_type INT,
  prefered_location VARCHAR(100),
  website_link VARCHAR(100),
  resume_path VARCHAR(255),
  experience INT,
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE job_seeker_work_experience(
  id INT AUTO_INCREMENT PRIMARY KEY,
  job_seeker_id INT,
  company_name VARCHAR(100),
  industry VARCHAR(100),
  functional_area VARCHAR(100),
  CTC INT,
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE employment_type(
  id INT AUTO_INCREMENT PRIMARY KEY,
  type VARCHAR(100),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE course_type(
  id INT AUTO_INCREMENT PRIMARY KEY,
  type VARCHAR(100),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE desired_job_type(
  id INT AUTO_INCREMENT PRIMARY KEY,
  type VARCHAR(100),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE job_post_details(
  id INT AUTO_INCREMENT PRIMARY KEY,
  jobname VARCHAR(255),
  experience INT,
  skills VARCHAR(255),
  address VARCHAR(255),
  industry VARCHAR(255),
  functional_area VARCHAR(255),
  company_id INT,
  posted_by INT,
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE highest_education(
  id INT AUTO_INCREMENT PRIMARY KEY,
  education_level VARCHAR(20),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE company_details(
  id INT AUTO_INCREMENT PRIMARY KEY,
  company_name VARCHAR(255),
  number_of_employees INT,
  address VARCHAR(255),
  industry VARCHAR(255),
  added_by INT,
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE jobseeker_applied_job(
  id INT AUTO_INCREMENT PRIMARY KEY,
  jobseeker_id INT,
  job_id INT,
  is_applied_by_job_seeker INT DEFAULT 0,
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;


CREATE TABLE job_seeker_details_dump(
  id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT,
  first_name VARCHAR(255),
  DOB TEXT,
  contact_number TEXT(255),
  city TEXT(255),
  current_designation TEXT(255),
  skills TEXT(255),
  resume_path VARCHAR(255),
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

CREATE TABLE company_user_map(
  id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT,
  company_id INT,
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;


CREATE TABLE company_contact_map(
  id INT AUTO_INCREMENT PRIMARY KEY,
  company_id INT,
  job_seeker_user_id INT,
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;


CREATE TABLE company_contact_info(
  id INT AUTO_INCREMENT PRIMARY KEY,
  company_contact_map_id INT,
  message TEXT,
  text_by INT,
  insert_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)DEFAULT CHARSET=utf8;

