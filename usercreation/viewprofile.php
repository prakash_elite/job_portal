<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
    // include_once("includes/redirect_IF_notadmin.php");

?>
<div class="mj_pagetitle2">
        <div class="mj_pagetitleimg">
            <img src="/assets/images/1349X316.png" alt="">
            <div class="mj_mainheading_overlay"></div>
        </div>
        <div class="mj_pagetitle_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="mj_mainheading">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="mj_joblogo">
                                        <img id="candidate_img" src="/assets/images/263X263.png" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div class="mj_pageheading mj_toppadder50">
                                        <h1 id="candidate_name">Jonathan<span>orter</span></h1>
                                        <ul>
                                            <li>
                                                <label><i class="fa fa-map-marker"></i>
                                                </label><a id="candiate_city" href="#">San Francisco</a>
                                            </li>
                                            <li>
                                                <label>Classification:</label><a id="candidate_designation" href="#">UX / Interaction Designer</a>
                                            </li>
                                            <li>
                                                <label><i class="fa fa-envelope" aria-hidden="true"></i></label><a id="candiate_email" href="#">San Francisco</a>
                                            </li>
                                            <li>
                                                <label><i class="fa fa-phone" aria-hidden="true"></i></label><a id="candiate_phone" href="#">San Francisco</a>
                                            </li>
                                            <!-- <li>
                                                <label>Hourly Rate: </label><span class="mj_green_text">$40.00/hr</span>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mj_lightgraytbg mj_bottompadder80">
        <div class="container">
            <!-- <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
                    <div class="mj_social_media_section mj_candidatepage_media mj_toppadder40 mj_bottompadder40">
                        <ul>
                            <li><a href="contact.html" class="mj_mainbtn mj_btnblue" data-text="Contact Candidate"><span>Contact Candidate</span></a>
                            </li>
                            <li><a href="#" class="mj_mainbtn mj_btnred" data-text="Bookmark this Resume"><span>Bookmark this Resume</span></a>
                            </li>
                        </ul>
                        <p><a href="#"><i class="fa fa-download"></i></a>
                        </p>
                        <p><a href="#"><i class="fa fa-print"></i></a>
                        </p>
                        <p><a href="#"><i class="fa fa-share-alt"></i></a>
                        </p>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mj_postdiv mj_jobdetail mj_toppadder50 mj_bottompadder50">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                            <div class="row">
                                <!-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label>Overview</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="mj_detaildiv">
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                                    </div>
                                </div> -->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mj_toppadder30 mj_bottompadder30">
                                    <div class="mj_divider"></div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label>Skills</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="mj_detaildiv">
                                        <ul class="mj_selected_content" id="candidate_skills">
                                            <!-- <li>
                                                <a class="csk" href="#"><img src="/assets/images/cross_white.png" alt=""> Design</a>
                                            </li>
                                            <li>
                                                <a href="#"><img src="/assets/images/cross_white.png" alt=""> Web Designer</a>
                                            </li>
                                            <li>
                                                <a href="#"><img src="/assets/images/cross_white.png" alt=""> User Experience</a>
                                            </li>
                                            <li>
                                                <a href="#"><img src="/assets/images/cross_white.png" alt=""> developer</a>
                                            </li> -->
                                        </ul>
                                        <!-- <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. </p>
                                        <ul>
                                            <li>Time honoured communication, leadership and academic traits strengthened through Product Assistant position and work presentations</li>
                                            <li>Used Visual Studio on unix systems to implement real time BlackBerry algorithms</li>
                                            <li>Thorough presentation, problem solving and leadership skills improved through Product Assistant position and project work</li>
                                            <li>Participated in a team that used CSS, mobile platforms and Oracle Server to improve Web Service technologies for heuristic Windows systems</li>
                                        </ul> -->
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mj_toppadder30 mj_bottompadder30">
                                    <div class="mj_divider"></div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label>Education</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="mj_detaildiv">
                                        <span id="candidate_university">University of Waterland Waterland, ON</span>
                                        <!-- <blockquote class="mj_blueblockquote">
                                            <span><i class="fa fa-calendar"></i> Dec 2010 - Sep 2011</span>
                                            <h5>Honours Co-op BMATH</h5>
                                            <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                                        </blockquote> -->
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mj_toppadder30 mj_bottompadder30">
                                    <div class="mj_divider"></div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <label>Experience</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div class="mj_detaildiv mj_bottompadder30">
                                        <span id="candidate_company">Contrasystems, Azeroth, WOW</span>
                                        <blockquote class="mj_yellowblockquote">
                                            <span id="candidate_experience"><i class="fa fa-calendar"></i> Jul 2012 - Dec 2013 </span>
                                            <span style="display: inline;"> Months</span>
                                            <!-- <h5>Chief Software Analyst</h5>
                                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p> -->
                                        </blockquote>
                                    </div>
                                    <!-- <div class="mj_detaildiv mj_bottompadder30">
                                        <span>World-span, Ottawa, ON</span>
                                        <blockquote class="mj_yellowblockquote">
                                            <span><i class="fa fa-calendar"></i> Jul 2013 - Dec 2014</span>
                                            <h5>Product Assistant</h5>
                                            <p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.</p>
                                        </blockquote>
                                    </div>
                                    <div class="mj_detaildiv mj_bottompadder30">
                                        <span>Amaegg, Seattle, WA</span>
                                        <blockquote class="mj_yellowblockquote">
                                            <span><i class="fa fa-calendar"></i> Jul 2014 - Dec 2015</span>
                                            <h5>Chief Software Software</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </blockquote>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mj_bottompadder30">
                    <div class="mj_about_section mj_toppadder80 mj_bottompadder80">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
                                <h1>A<span>bout</span> C<span>andidate</span></h1>
                                <p>Envato is a bootstrapped Australian company that operates an ecosystem of sites with a global community. We’re passionate about the web, and about enabling creators to make a living doing what they love. At Envato, we make websites that help people from all over the world change the way they earn and learn online.</p>
                                <ul>
                                    <li><a href="#"><i class="fa fa-link"></i> Website</a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-twitter"></i>Twitter</a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-facebook"></i>facebook</a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i>dribbble</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
<?php 
    include_once("includes/foot.php");
?>
    <!-- <script src="assets/js/city.js" type="text/javascript"></script>    -->
    <script type="text/javascript">
    var onSuccessPopulate=function(thisVariable,data){
        console.log(data);
        var jsonData=JSON.parse(data);
        $.each(jsonData, function(i, w){
            var username=(w.username)?w.username:"",
                city = (w.city)?w.city:"",
                designation = (w.current_designation)?w.current_designation:"",
                university = (w.university_or_college)?w.university_or_college:"",
                company = (w.current_company)?w.current_company:"",
                experience = (w.experience)?w.experience:"",
                profile_img = (w.profile_pic)?w.profile_pic:"";
                email = (w.email)?w.email:"";
                phone = (w.contact_number)?w.contact_number:"";

            var skills=w.skills.split(',');
                $.get('/skillsetbycommaseperated/'+skills,function(resp){
                var jsonresp=JSON.parse(resp);
                $.each(jsonresp, function(i, w){
                    var skills_array = w.skills.split(',');
                    for(var i=0;i<skills_array.length;i++){
                        $('#candidate_skills').append('<li><a class="csk" href="#"><img src="/assets/images/cross_white.png" alt="">'+ skills_array[i]+'</a></li>')
                    }
                });
            });
            $('#candidate_name').html(username);
            $('#candiate_city').html(city);
            $('#candidate_designation').html(designation);
            $('#candidate_university').html(university);
            $('#candidate_company').html(company);
            $('#candidate_experience').html(experience);
            $('#candidate_img').attr('src', profile_img);
            $('#candiate_email').html(email);
            $('#candiate_phone').html(phone);
        }); 

    }
        $(document).ready(function(){
            jobportal.utilities.ajaxCalls("GET","/getprofiledetailsbyid",{},onSuccessPopulate,"");
        });
    </script>
</body>

</html>
