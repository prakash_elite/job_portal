jobportal.country={

	init:function(){
				this.getAllCountry();
				this.initShowMore();
				jobportal.utilities.initAddNew(function($thisVariable){
					$('#submit-success').modal('show');
					jobportal.country.initSubmitForm("POST",$('#submit-success form').attr('action')+1);
				    jobportal.utilities.initTriggerElement($('#trigger-update'),$('#update'));
				});
				jobportal.utilities.initSelectJs($('#search-all'),{
					url:'/searchcountries/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					var country_id=e.params.data.id;
					jobportal.country.accesOnclick(country_id);
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.country,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initSubmitForm:function(type,url){
			$('.register-form').on('submit',function(e){
					e.preventDefault();
					var datatoSend=$(this).serialize();
					$.ajax({
			           type: type,
			           url: url,
			           data: datatoSend, // serializes the form's elements.
			           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
			           success: function(data)
			           {
			           		window.location.reload();
			           }
			        });
			 });
	},
	getAllCountry:function(){
		var currentPagelength=$('#list-table tbody tr').length;
		 jobportal.utilities.ajaxCalls("GET",'/getallcountries/'+currentPagelength,{},jobportal.country.getallCountryOnSuccess,"");			 		
	},

	deleteCountry:function(){
		$('.list-text').on('click',function(e){
			e.preventDefault();
			jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{},function(thisVariable,data){
				 			$('#country-'+data).remove();
				 			if($('#list-table tr').length<2){
				 				$('<div id="no-data-available">No Data Available</div>').insertAfter($('.table-responsive'));
				 			}				 			
				},"");
		});
	},

	editCountry:function(){
		$('.edit-country').on('click',function(e){
			e.preventDefault();
			var country_id=$(this).attr('data-country-id');
			jobportal.country.accesOnclick(country_id);
		});

	},

	getallCountryOnSuccess:function(thisVariable,data){

		var jsonData=JSON.parse(data),
			$tableData=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				if(w.id){
					dataToAppend+='<tr id="country-'+w.id+'">'+
		            '<td>'+
		                '<h4><a class="edit-country list-only-text" data-country-id="'+w.id+'" id="country-'+w.id+'" href="javascript:void(0)">'+w.country+'</a></h4>'+
		            '</td>'+                           
		            '<td>'+
		                '<p class="list-text Deactivate" data-url="/countryopr/'+w.id+'">Delete</p>'+
		            '</td></tr>';
		            $('#no-data-available').remove();
				}
			});
			$tableData.append(dataToAppend);
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}
			jobportal.country.editCountry();	   
			jobportal.country.deleteCountry();	   
	},

	accesOnclick:function(country_id){
			jobportal.utilities.ajaxCalls("GET",'/countryopr/'+country_id,{},function(thisVariable,data){
						var jsonData=JSON.parse(data);
						$('#edit-submit-success').modal('show');
						jobportal.country.initSubmitForm("PUT",$('#edit-submit-success form').attr('action')+country_id);
						jobportal.utilities.initTriggerElement($('#trigger-update-edit'),$('#update-edit'));
						$('#country_name-edit').val(jsonData[0].country);
					},"");
	},
    initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.country.getAllCountry();
    	});
    }

}

