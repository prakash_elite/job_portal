jobportal.home={
	init:function(){
			this.initGetAppliedJobs();
			this.initSearchAppliedJob();
	},
	initGetAppliedJobs:function(){
		var url="/getJobAppliedDetailsByUser",
			type="GET";
			jobportal.utilities.ajaxCalls(type,url,{},jobportal.home.onSearchSucces,"");
	},
	onSearchSucces:function(thisVaridable,data){
		var jsonData=JSON.parse(data),
			$tbody=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				var jsonData=JSON.parse(data);
			 	dataToAppend+='<tr id="search-job-'+w.id+'">'+
	                            '<td>'+
	                                '<h4><a class="text-left-a" href="javascript:void(0)">'+w.jobname+'</a></h4>'+
	                            '</td>'+   
	                            '<td>'+
	                                '<p class="list-text">'+w.applied_on+'</p>'+
	                            '</td>'+
	                           	'<td>'+
	                                '<p></p>'+
	                            '</td>'+
	                        '</tr>';
	                        $('#no-data-available').remove();
			});
			$tbody.html(dataToAppend);
	},
	initSearchAppliedJob:function(){
		$("#search-all").select2({
					multiple:false,
				    ajax: {
					    url: function (params) {
					      return '/searchappliedjobs/' + params.term;
					    },
					    data:{},
					    dataType: 'json',
					    beforeSend: function(request){
								 request.setRequestHeader("Authorization",localStorage.getItem('sess'))	
						},
					    processResults: function (data) {
					          var results = [];
					          $.each(data, function(index, item){
					            results.push({
					              id: item.job_id,
					              text: item.jobname,
					              applied_on:item.applied_on
					            });
					          });
					          return {
					              results: results
					          };
		        		},
					  },
						minimumInputLength:2,
				});	
				$('#search-all').on("select2:select", function(e) { 
					var $tableData=$('tbody',$('#list-table')),
						dataToAppend='<tr id="search-job-'+e.params.data.id+'">'+
	                            '<td>'+
	                                '<h4><a class="text-left-a" href="javascript:void(0)">'+e.params.data.text+'</a></h4>'+
	                            '</td>'+   
	                            '<td>'+
	                                '<p class="list-text">'+e.params.data.applied_on+'</p>'+
	                            '</td>'+
	                            '<td>'+
	                                '<p></p>'+
	                            '</td>'+
	                        '</tr>';
					$tableData.html(dataToAppend);
				});				
	}
}