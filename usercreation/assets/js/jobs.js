jobportal.jobs={

	init:function(){
				this.getAllJobs();
				this.initShowMore();
				jobportal.utilities.initSelectJs($('#search-all'),{
					url:'/searchjobtitle/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					var jobs_id=e.params.data.id;
					localStorage.setItem('job',jobs_id);
					setTimeout(function(){
						window.location.href="editjob.php";
					},500);
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.jobname,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	getAllJobs:function(){
		 var currentPagelength=$('#list-table tbody tr').length;
		 jobportal.utilities.ajaxCalls("GET",$('#list-table').attr('data-form-path')+"/"+currentPagelength,{},jobportal.jobs.getallJobsOnSuccess,"");			 		
	},

	deleteJob:function(){
		$('.list-text').on('click',function(e){
			e.preventDefault();
			jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{},function(thisVariable,data){
				 			$('#job-'+data).remove();
				 			if($('#list-table tr').length<2){
				 				$('<div id="no-data-available">No Data Available</div>').insertAfter($('.table-responsive'));
				 			}				 			
				},"");
		});
	},

	editJob:function(){
		$('.edit-job').on('click',function(e){
			var jobs_id=$(this).attr('data-job-id');
			localStorage.setItem('job',jobs_id);
			setTimeout(function(){
				window.location.href="editjob.php";
			},500);
		});

	},

	getallJobsOnSuccess:function(thisVariable,data){

		var jsonData=JSON.parse(data),
			$tableData=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				if(w.id){
					var company_name=(w.company_name)?w.company_name:" ",
			        industry=(w.industry)?w.industry:" ",
			        function_area=(w.function_area)?w.function_area:" ",
			        username=(w.username)?w.username:"";skills
					dataToAppend+='<tr id="job-'+w.id+'">'+
					            '<td>'+
					                '<h4><a class="edit-job list-only-text" data-job-id="'+
					            			w.id+'" id="jobs-'+w.id+'" href="javascript:void(0)">'+w.jobname+
					            	'</a></h4>';
					            	if(w.skills){
					            		dataToAppend+='<p class="skill-set-list skill-set_ids_'+w.id+'"></p>';
					            	}
					            dataToAppend+='</td>'+
					            '<td>'+
					                '<p class="list-text">'+w.experience+'</p>'+
					            '</td>'+ 
					            '<td>'+
					                '<p class="list-text">'+w.industry+'</p>'+
					            '</td>'+     
					            '<td>'+
					                '<p class="list-text">'+w.function_area+'</p>'+
					            '</td>'+ 	 
					            '<td>'+
					                '<p class="list-text">'+w.company_name+'</p>'+
	           					 '</td>';
			           			dataToAppend+='<td><p class="list-text">'+username+'</p></td>';                                     
	            				dataToAppend+='<td>'+
				                	'<p class="list-text Deactivate" data-url="/jobs/'+w.id+'">Delete</p>'+
				            		'</td></tr>';
				    	if(w.skills){
			           				var skills=w.skills.split(',');
			           					$.get('/skillsetbycommaseperated/'+skills,function(resp){
			           						var jsonresp=JSON.parse(resp);
			           						$('.skill-set_ids_'+w.id).html(jsonresp[0].skills);
			           				});
			           			}
			           	 $('#no-data-available').remove();
				}
				
			});
			$tableData.append(dataToAppend);
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}
			jobportal.jobs.editJob();	   
			jobportal.jobs.deleteJob();	   
	},
    initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.jobs.getAllJobs();
    	});
    }

}

