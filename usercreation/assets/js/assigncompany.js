jobportal.assigncompany={
	init:function(){
		jobportal.utilities.initSelectAllCompany();
		jobportal.utilities.initTriggerElement($('#trigger-assign-comp'),$('#update'));
		this.initSubmitForm();
		this.initGetAllAssigns();
		this.initShowMore();
		jobportal.utilities.initSelectJs($('#users'),{
					url:'/searchusers/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.main_id,
					    text: item.username,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initSubmitForm:function(jobid){
			$('#register-form').on('submit',function(e){
				e.preventDefault();
				var url=$(this).attr('action'),
					type="POST";
				 jobportal.utilities.ajaxCalls(type,url,
				 										$(this).serialize(),
				 										function(data){			           
											           		window.location.href="assigncompany.php";
											           },"");
					
			 });
	},
	initGetAllAssigns:function(){
		 var currentPagelength=$('#list-table tbody tr').length;
		 jobportal.utilities.ajaxCalls("GET","/assignCompanies/"+currentPagelength,{},jobportal.assigncompany.onSuccesgetAll,"");
	},
	onSuccesgetAll:function(thisVariable,data){
		var jsonData=JSON.parse(data),
			$tableData=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				if(w.map_id){
						dataToAppend+='<tr id="map-'+w.map_id+'"><td>'+
                          '<a href="#">'+
                          '<img src="'+w.company_logo+'" class="img-responsive img-size-70" alt="">'+
                                '</a>'+
	                      '</td>'+
	                      '<td>'+
	                          '<p class="list-text">'+w.username+'</p>'+
	                      '</td>'+
	                      '<td>'+
	                          '<p class="list-text">'+w.company_name+'</p>'+
	                      '</td>'+
	                      '<td>'+
	                          '<p class="list-text delete Deactivate" data-url="/assignCompanies" data-map-id="'+w.map_id+'">Delete</p>'+
	                      '</td></tr>';
	                      $('#no-data-available').remove();
				}
			});
			$tableData.append(dataToAppend);
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}
			jobportal.assigncompany.deleteIndustry();
	},
	deleteIndustry:function(){
		$('.delete').on('click',function(e){
			e.preventDefault();
			var map_id=$(this).attr('data-map-id');
			jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{'delete':map_id},function(thisVariable,data){
				 			$('#map-'+data).remove();
				 			if($('#list-table tr').length<2){
				 				$('<div id="no-data-available">No Data Available</div>').insertAfter($('.table-responsive'));
				 			}
				},'');
		});
	},
	initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.assigncompany.initGetAllAssigns();
    	});
    }
}