jobportal.city={

	init:function(){
				this.getAllcity();
				this.initShowMore();
				jobportal.utilities.initAddNew(function($thisVariable){
					$('#submit-success').modal('show');
					jobportal.utilities.initSelectCountry();
					jobportal.city.initSubmitForm("POST",$('#submit-success form').attr('action')+1);
				    jobportal.utilities.initTriggerElement($('#trigger-update'),$('#update'));
				});
				jobportal.utilities.initSelectJs($('#search-all'),{
					url:'/searchcities/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					var city_id=e.params.data.id;
					jobportal.city.accesOnclick(city_id);
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.city_id,
					    text: item.city,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initSubmitForm:function(type,url){
			$('.register-form').on('submit',function(e){
					e.preventDefault();
					var datatoSend=$(this).serialize();
					$.ajax({
			           type: type,
			           url: url,
			           data: datatoSend, // serializes the form's elements.
			           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
			           success: function(data)
			           {
			           		window.location.reload();
			           }
			        });
			 });
	},
	getAllcity:function(){
		var currentPagelength=$('#list-table tbody tr').length;
		 jobportal.utilities.ajaxCalls("GET",'/getallcities/'+currentPagelength,{},jobportal.city.getallcityOnSuccess,"");			 		
	},

	deleteCity:function(){
		$('.list-text').on('click',function(e){
			e.preventDefault();
			jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{},function(thisVariable,data){
				 			$('#city-'+data).remove();
				 			if($('#list-table tr').length<2){
				 				$('<div id="no-data-available">No Data Available</div>').insertAfter($('.table-responsive'));
				 			}
				},"");
		});
	},

	editCity:function(){
		$('.edit-city').on('click',function(e){
			e.preventDefault();
			var city_id=$(this).attr('data-city-id');
			jobportal.city.accesOnclick(city_id);
		});

	},

	getallcityOnSuccess:function(thisVariable,data){

		var jsonData=JSON.parse(data),
			$tableData=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				if(w.city_id){
					dataToAppend+='<tr id="city-'+w.city_id+'">'+
		            '<td>'+
		                '<h4><a class="edit-city list-only-text" data-city-id="'+w.city_id+'" id="city-'+w.city_id+'" href="javascript:void(0)">'+w.city+'</a></h4>'+
		            '</td>'+
		            '<td>'+
		                '<p class="list-text">'+w.country+'</p>'+
		            '</td>'+                            
		            '<td>'+
		                '<p class="list-text Deactivate" data-url="/cities/'+w.city_id+'">Delete</p>'+
		            '</td></tr>';
		            $('#no-data-available').remove();	
				}
			});
			$tableData.append(dataToAppend);
			jobportal.city.editCity();	   
			jobportal.city.deleteCity();
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}	   
	},

	accesOnclick:function(city_id){
			jobportal.utilities.ajaxCalls("GET",'/cities/'+city_id,{},function(thisVariable,data){
						var jsonData=JSON.parse(data);
						$('#edit-submit-success').modal('show');
						jobportal.city.initSubmitForm("PUT",$('#edit-submit-success form').attr('action')+city_id);
						jobportal.utilities.initTriggerElement($('#trigger-update-edit'),$('#update-edit'));
						$('#city_name').val(jsonData[0].city);
						$('#country-selected-edit').html(jsonData[0].country);
						jobportal.city.initSelectCountryEdit();
					
					},"");
	},

	initSelectCountryEdit:function(){
			jobportal.utilities.initSelectJs($('#country-edit'),{
					url:'/getcountryList/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.country,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.city.getAllcity();
    	});
    }

}

