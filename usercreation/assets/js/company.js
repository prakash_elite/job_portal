jobportal.company={

	init:function(){
				this.getAllCompany();
				this.initShowMore();
				jobportal.utilities.initAddNew(function($thisVariable){
					$('#submit-success').modal('show');
					jobportal.utilities.initSelectCurrentIndustry();
					jobportal.company.initSubmitForm("POST",$('#submit-success form').attr('action'));
				    jobportal.utilities.initTriggerElement($('#trigger-update'),$('#update'));
				    jobportal.utilities.initTriggerElement($('#trigger-profile-img'),$('#profile-pic'));
				});
				jobportal.utilities.initSelectJs($('#search-all'),{
					url:'/searchcompanies/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					var comp_id=e.params.data.id;
					jobportal.company.accesOnclick(comp_id);
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.company_name,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initSubmitForm:function(type,url){
			$('.register-form').on('submit',function(e){
					e.preventDefault();
					 var formData = new FormData($(this)[0]);
					$.ajax({
			           type: type,
			           url: url,
			           data: formData,
			           processData: false, 
                       contentType: false, // serializes the form's elements.
			           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
			           success: function(data)
			           {
			           		window.location.reload();
			           }
			        });
			 });
	},
	getAllCompany:function(){
		var currentPagelength=$('#list-table tbody tr').length;
		 jobportal.utilities.ajaxCalls("GET",$('#get-all-path').val()+"/"+currentPagelength,{},jobportal.company.getallCompanyOnSuccess,"");			 		
	},

	deleteCompany:function(){
		$('.list-text').on('click',function(e){
			e.preventDefault();
			jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{},function(thisVariable,data){
				 			$('#comp-'+data).remove();
				 			if($('#list-table tr').length<2){
				 				$('<div id="no-data-available">No Data Available</div>').insertAfter($('.table-responsive'));
				 			}				 			
				},"");
		});
	},

	editCompany:function(){
		$('.edit-company').on('click',function(e){
			e.preventDefault();
			var comp_id=$(this).attr('data-comp-id');
			jobportal.company.accesOnclick(comp_id);
		});

	},

	getallCompanyOnSuccess:function(thisVariable,data){

							var jsonData=JSON.parse(data);
			           		var $tableData=$('tbody',$('#list-table')),
			           			dataToAppend="";
			           		$.each(jsonData,function(i,w){
			           			if(w.id){
			           				var industry=(w.industry)?w.industry:"";
					           			dataToAppend+='<tr id="comp-'+w.id+'"><td>'+
		                                '<a href="#">'+
		                                 '<img src="'+w.company_logo+'" class="img-responsive img-size-70" alt="logo" title="logo">'+
		                                '</a>'+
			                            '</td>'+
			                            '<td>'+
			                                '<h4><a class="edit-company" data-comp-id="'+w.id+'" id="comp-'+w.id+'" href="javascript:void(0)">'+w.company_name+'</a></h4>'+
			                                '<p>Employees &nbsp;'+w.number_of_employees+'</p>'+
			                            '</td>'+
			                            '<td>'+
			                                '<p class="list-text">'+w.address+'</p>'+
			                            '</td>'+
			                             '<td>'+
			                                '<p class="list-text">'+w.added_by+'</p>'+
			                            '</td>'+
			                             '<td>'+
			                                '<p class="list-text">'+w.industry+'</p>'+
			                            '</td>'+	                            
			                            '<td>'+
			                                '<p class="list-text Deactivate" data-url="/companies/'+w.id+'">Delete</p>'+
			                            '</td></tr>';
	                            	$('#no-data-available').remove();
			           			}
			           		});
			$tableData.append(dataToAppend);
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}
			jobportal.company.editCompany();	   
			jobportal.company.deleteCompany();	   
	},

	accesOnclick:function(comp_id){
			jobportal.utilities.ajaxCalls("GET",'/getCompanyforid/'+comp_id,{},function(thisVariable,data){
						var jsonData=JSON.parse(data);
						$('#edit-submit-success').modal('show');
						jobportal.company.initSubmitForm("POST",$('#edit-submit-success form').attr('action')+comp_id);
						jobportal.utilities.initTriggerElement($('#trigger-update-edit'),$('#update-edit'));
						jobportal.utilities.initTriggerElement($('#trigger-profile-img-edit'),$('#profile-pic-edit'));
						$('#company_name-edit').val(jsonData[0].company_name);
						$('#address-edit').val(jsonData[0].address);
						$('#number_of_employees-edit').val(jsonData[0].number_of_employees);
						$('#industry_selected-edit').html(jsonData[0].industry);
						$('#uploaded-img').attr('src',jsonData[0].company_logo);
						$('#uploaded-img-hidden').val(jsonData[0].company_logo);
						jobportal.utilities.initSelectJs($('#current_industry-edit'),{
							url:'/getindustryList/',
							isMultiple:false,
							minimumInputLength:1,
						},function(e){
							
						},function (data) {
							var results = [];
							$.each(data, function(index, item){
							  results.push({
							    id: item.id,
							    text: item.industry,
							  });
							});
							return {
							    results: results
							};
				        });
					
					},"");
	},
	initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.company.getAllCompany();
    	});
    }


}

