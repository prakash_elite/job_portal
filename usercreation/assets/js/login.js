jobportal.login={

	init:function(){
			jobportal.utilities.clearAllLocalStorage();
			$('#register-form-link').on('click',function(e){
				e.preventDefault();
				$('#register-form-btn').trigger('click');
			});
			this.initSubmit();

	},

	initSubmit:function(){
			$('#register-form').on('submit',function(e){
				e.preventDefault();
				var datatoSend=$(this).serialize();
				$.ajax({
		           type: "POST",
		           url: $(this).attr('action'),
		           data: datatoSend, // serializes the form's elements.
		           success: function(data)
		           {

		           		var tokenData=JSON.parse(data);
		           		localStorage.setItem('sess',tokenData.auth_token);
		           		setTimeout(function(){
		           			window.location.href=tokenData.page; // show response from the php script.
		           		},700);
		           		
		           },
		           error:function(){
		           		$('#error').show();
		           }
		        });
			});
	}

}

jobportal.register={

	init:function(){

			$('.mj_pricing_footer').on('click',function(e){
				e.preventDefault();
				$('.register-form-btn',$(this).closest('form')).trigger('click')
			});
			this.initSubmit();

	},

	initSubmit:function(){
			$('.register-form').on('submit',function(e){
				e.preventDefault();
				var $thisForm=$(this);
				var datatoSend=$thisForm.serialize();
				$.ajax({
		           type: "POST",
		           url: $(this).attr('action'),
		           data: datatoSend,
		           beforeSend: function(request){
				 	request.setRequestHeader("Authorization",localStorage.getItem('sess'))	
				   }, 
		           success: function(data){		           		
		           		$('.close').trigger('click');
		           		window.location.href="users.php";
		           },
		           error:function(xhr){
		           	$('.error',$thisForm).html(xhr.responseText).show();
		           }
		        });
			});
	},

	initRecruiter:function(){

			$('.mj_pricing_footer').on('click',function(e){
				e.preventDefault();
				$('.register-form-btn',$(this).closest('form')).trigger('click')
			});
			this.initSubmitRecruiter();

	},

	initSubmitRecruiter:function(){
			$('.register-form').on('submit',function(e){
				e.preventDefault();
				var datatoSend=$(this).serialize();
				$.ajax({
		           type: "POST",
		           url: $(this).attr('action'),
		           data: datatoSend,
		           beforeSend: function(request){
				 	
				   }, 
		           success: function(data){		           		
		           		$('.close').trigger('click');
		           		window.location.href="/login.php";
		           },
		           error:function(xhr){
		           	$('#error').html(xhr.responseText).show();
		           }
		        });
			});
	}
}