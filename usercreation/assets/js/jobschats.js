jobportal.jobschats={
	init:function(){
		this.getAllChats();
		this.initShowMore();
	},
	getAllChats:function(){
		var currentPagelength=$('#list-table tbody tr').length;
		jobportal.utilities.ajaxCalls("GET",'/getinboxdetails/'+currentPagelength,{},jobportal.jobschats.onSuccessGetMessage,"");
	},
	onSuccessGetMessage:function(thisVariable,data){
	if(data){
		var jsonData=JSON.parse(data),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				if(w.company_contact_map_id){
					var jobseekerid=(w.job_seeker_user_id)?w.job_seeker_user_id:0;
					dataToAppend+='<tr id="search-job-'+w.company_contact_map_id+'">'+
	                            '<td>'+
	                                '<h4><a class="text-left-a" href="javascript:void(0)">'+w.company_name+'</a></h4>'+
	                            '</td>'+   
	                            '<td>'+
	                                '<p class="list-text">'+w.message+'</p>'+
	                            '</td>'+
	                           	'<td>'+
	                                '<p>'+w.message_on+'</p>'+
	                            '</td>'+
	                            '<td>'+
	                            '<p class="list-text mj_mainbtn mj_btnblue contact-recruiter"'+
	                                ' data-url="/candidateapplyJob" data-attr-id="'+
	                                w.company_contact_map_id+'" data-attr-seeker-id="'+jobseekerid+'" data-text="Reply">Reply</p>'+
	                            '</td>'+
	                        '</tr>';      
	                        $('#no-data-available').remove();  
				}				
			});
			$('#list-table').append(dataToAppend);
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}
			jobportal.jobschats.initContactRecruiter();
		
		}
	},
	initContactRecruiter:function(){
			$('.contact-recruiter').on('click',function(){
				 var contactmapid=$(this).attr('data-attr-id'),
				 		jobSeekerId=$(this).attr('data-attr-seeker-id'),
				 		url=$('#list-table').attr('data-path');
				 if(parseInt(jobSeekerId)){
				 	localStorage.setItem('contactseeker',jobSeekerId);
				 }
				 localStorage.setItem('contactmapid',contactmapid);
				 setTimeout(function() {
				  window.location.href=url;
				 },500);
			});
	},
   initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.jobschats.getAllChats();
    	});
    }
}