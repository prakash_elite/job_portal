      jobportal.searchalljobs={
            init:function(){
              $('.mj_pricing_footer').on('click',function(e){
                    e.preventDefault();
                    $('.register-form-btn',$(this).closest('form')).trigger('click')
                  });
                  this.initSubmit();
                  jobportal.utilities.initTriggerElement($('#trigger-upload-resume'),$('#resume'));
                  this.initJobSearchOthers();
              },

              initSubmit:function(){
                  $('.register-form').on('submit',function(e){
                    e.preventDefault();
                    var formData = new FormData($(this)[0]);
                    $.ajax({
                           type: "POST",
                           url: $(this).attr('action'),
                           data: formData,
                           processData: false, 
                            contentType: false,// serializes the form's elements.
                           beforeSend: function(request){
                                  request.setRequestHeader("Authorization",localStorage.getItem('sess'));
                                  $('.error').hide();
                         },
                           success: function(data){
                            window.location.href="/login.php";
                           },
                           error:function(xhr){
                            $('.error').html(xhr.responseText).show();
                           }
                        });
                  });
              },
              initJobSearchOthers:function(){

                $.ajax({
                           type: "POST",
                           url:"/jobapi/index.php",
                           data: {
                            'what':"<?php echo $what; ?>",
                            'where':"<?php echo $where; ?>"
                           },
                           beforeSend: function(request){

                         },
                           success: function(data){
                              var jsonData=JSON.parse(data),

                                  $tableBody=$('#job-list'),

                                  appendHtml="";
                              $.each(jsonData,function(i,w){
                                var experience=(w.experience)?w.experience:"Not mentioned";
                                appendHtml+="<div class='row' style='margin-top:10px;'>"+
                                "<div class='mj_postdiv mj_jobdetail mj_toppadder10'>"+
                                  "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' id='list-table'>"+
                                    "<div class='row'>"+
                                      "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 job_details_sec'>"+
                                        "<h2>"+w.title+"</h2>"+
                                        "<div class='mj_detaildiv'>"+
                                          "<blockquote class='mj_blueblockquote'>"+
                                            "<span class='company_name_span'>"+w.company+"</span>"+
                                            "<span><i class='fa fa-suitcase'></i> "+experience+" years</span>"+
                                            "<div class='mj_detaildiv'>"+
                                              "<ul class='mj_selected_content'>"+
                                                "<li><a href='#'>"+w.skill+"</a></li>"+
                                              "</ul>"+
                                            "</div>"+
                                            "<h5>Description</h5>"+
                                            "<p class='job_desc'>"+w.description+"</p>"+
                                            "<span><i class='fa fa-map-marker'></i> "+w.location+"</span>"+
                                            "<a href='"+w.url+"' class='mj_mainbtn mj_btnblue job-apply-btn' data-text='Apply'><span>Apply</span></a>"+
                                            "</div>"+
                                          "</blockquote>"+
                                        "</div>"+
                                    "</div>"+
                                  "</div>"+
                                "</div>"+
                              "</div>";
            
                              });
                              $tableBody.append(appendHtml);
                              $('.mj_preloaded_serach').removeClass('show-must').hide();
                              $('.mj_preloader_serach').removeClass('show-must').hide();

                           },
                           error:function(xhr){
                           }
                        });
              },
            }   