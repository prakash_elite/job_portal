jobportal.useredit={

	init:function(){
		jobportal.utilities.initDatePicker($('#DOB'));
		jobportal.utilities.initTriggerElement($('#trigger-upload-resume'),$('#resume'));
		jobportal.utilities.initTriggerElement($('#trigger-profile-upload'),$('#update'));
		jobportal.utilities.initTriggerElement($('#trigger-profile-img'),$('#profile-pic'));
		jobportal.utilities.initSelectCity();
		jobportal.utilities.initSelectCountry();
		jobportal.utilities.initSelectLanguages();
		jobportal.utilities.initSelectSkills();
		jobportal.utilities.initSelectCurrentIndustry();
		jobportal.utilities.initSelectCurrentFunctionArea();
		jobportal.utilities.initSelectPreferredLocation();
		this.initGetDataForUserEdit();
		this.initFormSubmit();
	},

	

	initGetDataForUserEdit:function(){
		var url='/getprofiledetailsbyid',
			userId=localStorage.getItem('user');
		if(userId){
			url='/users/'+userId
		}
		jobportal.utilities.ajaxCalls("GET",url,{},jobportal.useredit.getUserOnSuccess,"");
	},


	initFormSubmit:function(){
		$('#register-form').on('submit',function(e){
			e.preventDefault();
			var formData = new FormData($(this)[0]);
			$.ajax({
	           type: "POST",
	           url: $(this).attr('action')+"/"+localStorage.getItem('user'),
	           data: formData,
	           processData: false, 
	            contentType: false,// serializes the form's elements.
	           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		       },
	           success: function(data){
	              $('#submit-success').modal('show');
	           },
	           error:function(xhr){
	           	var $errorElement='#'+xhr.responseText;
	           	$('.error').hide();
	           	$($errorElement).show();
	           	$($errorElement).next('input').focus();
	           }
	        });
		});
	},
	
	getUserOnSuccess:function(thisVariable,data){
			var jsonData=JSON.parse(data);
			           		if(jsonData[0].skills){
			           			var skills=jsonData[0].skills.split(',');
			           			jobportal.utilities.ajaxCalls("GET",'/skillsetbycommaseperated/'+skills,{},function(thisvariable,data){
			           					var jsonresp=JSON.parse(data);
			           					$('#skills-selected').html(jsonresp[0].skills);
			           				},"");
			           				
			           		}
			           		// if(jsonData[0].languages_known){
			           		// 	var languages_known=jsonData[0].languages_known.split(',');
			           		// 	jobportal.utilities.ajaxCalls("GET",'/languagesknownbycommaseperated/'+languages_known,{},function(thisvariable,data){
			           		// 			var jsonresp=JSON.parse(data);
			           		// 			$('#languages-known-selected').html(jsonresp[0].language);
			           		// 		},"");
			           		// }	
							var username=(jsonData[0].username)?jsonData[0].username:" ",
									first_name=(jsonData[0].first_name)?jsonData[0].first_name:" ",
									last_name=(jsonData[0].last_name)?jsonData[0].last_name:" ",
									email=(jsonData[0].email)?jsonData[0].email:" ",
									contact_number=(jsonData[0].contact_number)?jsonData[0].contact_number:" ",
									address_line_1=(jsonData[0].address_line_1)?jsonData[0].address_line_1:" ",
									address_line_2=(jsonData[0].address_line_2)?jsonData[0].address_line_2:" ",
									state=(jsonData[0].state)?jsonData[0].state:" ",
									pincode=(jsonData[0].pincode)?jsonData[0].pincode:" ",
									city=(jsonData[0].city)?jsonData[0].city:" ",
									country=(jsonData[0].country)?jsonData[0].country:" ",
									current_title=(jsonData[0].current_designation)?jsonData[0].current_designation:" ",
									experience=(jsonData[0].experience)?jsonData[0].experience:" ",
									current_company=(jsonData[0].current_company)?jsonData[0].current_company:" ",
									existing_sal=(jsonData[0].current_CTC)?jsonData[0].current_CTC:" ",
									expected_sal=(jsonData[0].expected_CTC)?jsonData[0].expected_CTC:" ",
									notice_period=(jsonData[0].duration_notice_period)?jsonData[0].duration_notice_period:" ",
									current_industry=(jsonData[0].industry)?jsonData[0].industry:" ",
									current_functional_area=(jsonData[0].function_area)?jsonData[0].function_area:" ",
									gender=(jsonData[0].gender)?jsonData[0].gender:1,
									highest_education=(jsonData[0].highest_education)?jsonData[0].highest_education:1,
									desired_job_type=(jsonData[0].desired_job_type)?jsonData[0].desired_job_type:1,
									desired_employment_type=(jsonData[0].desired_employment_type)?jsonData[0].desired_employment_type:1,
									major=(jsonData[0].major)?jsonData[0].major:" ",
									university_or_college=(jsonData[0].university_or_college)?jsonData[0].university_or_college:" ",
									prefered_location=(jsonData[0].prefered_location)?jsonData[0].prefered_location:" ",
									website_link=(jsonData[0].website_link)?jsonData[0].website_link:" ",
									resume=(jsonData[0].resume_path)?"/"+jsonData[0].resume_path:"",
									uploadedImg=(jsonData[0].profile_pic)?jsonData[0].profile_pic:"/assets/images/50X50.png";
										$('#name').val(username);
										$('#first_name').val(first_name);
										$('#last_name').val(last_name);
										$('#email').val(email);
										$('#contact_number').val(contact_number);
										$('#address_line_1').val(address_line_1);
										$('#address_line_2').val(address_line_2);
										$('#state').val(state);
										$('#pincode').val(pincode);
										$('#city-selected').html(city);
										$('#country-selected').html(country);
										$('#current_designation').val(current_title);
										$('#experience').val(experience);
										$('#current_company').val(current_company);
										$('#current_CTC').val(existing_sal);
										$('#expected_CTC').val(expected_sal);
										$('#duration_notice_period').val(notice_period);
										$('#current_industry-selected').html(current_industry);
										$('#current_functional_area-selected').html(current_functional_area);
										//$('#specialization').val(major);
										$('#university_or_college').val(university_or_college);
										$('#prefered_location-selected').html(prefered_location);
										$('#website_link').val(website_link);
										$('#resume-selected a').attr('href',resume);
										//$('#gender option[value="'+gender+'"]').prop('selected',true);
										$('#highest_education option[value="'+highest_education+'"]').prop('selected',true);
										$('#desired_job_type option[value="'+desired_job_type+'"]').prop('selected',true);
										$('#desired_employment_type option[value="'+desired_employment_type+'"]').prop('selected',true);
										jobportal.utilities.initDatePicker($( "#DOB" ),jsonData[0].DOB);
										$('#uploaded-img').attr('src',uploadedImg);
										$('#uploaded-img-hidden').val(uploadedImg);
	},

}

jobportal.usereprofile={
	init:function(){
		jobportal.utilities.initDatePicker($('#DOB'));
		jobportal.utilities.initTriggerElement($('#trigger-upload-resume'),$('#resume'));
		jobportal.utilities.initTriggerElement($('#trigger-profile-upload'),$('#update'));
		jobportal.utilities.initTriggerElement($('#trigger-profile-img'),$('#profile-pic'));		
		jobportal.utilities.initSelectCity();
		jobportal.utilities.initSelectCountry();
		jobportal.utilities.initSelectLanguages();
		jobportal.utilities.initSelectSkills();
		jobportal.utilities.initSelectCurrentIndustry();
		jobportal.utilities.initSelectCurrentFunctionArea();
		jobportal.utilities.initSelectPreferredLocation();
		jobportal.useredit.initGetDataForUserEdit();
		this.initFormSubmit();
	},
	initFormSubmit:function(){
		$('#register-form').on('submit',function(e){
			e.preventDefault();
			var formData = new FormData($(this)[0]);
			$.ajax({
	           type: "POST",
	           url: $(this).attr('action'),
	           data: formData,
	           processData: false, 
	            contentType: false,// serializes the form's elements.
	           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		       },
	           success: function(data){
	              $('#submit-success').modal('show');
	           },
	           error:function(xhr){
	           	var $errorElement='#'+xhr.responseText;
	           	$('.error').hide();
	           	$($errorElement).show();
	           	$($errorElement).next('input').focus();
	           }
	        });
		});
	}
}