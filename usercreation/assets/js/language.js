jobportal.language={

	init:function(){
				this.getAllLanguage();
				this.initShowMore();
				jobportal.utilities.initAddNew(function($thisVariable){
					$('#submit-success').modal('show');
					jobportal.language.initSubmitForm("POST",$('#submit-success form').attr('action')+1);
				    jobportal.utilities.initTriggerElement($('#trigger-update'),$('#update'));
				});
				jobportal.utilities.initSelectJs($('#search-all'),{
					url:'/searchlang/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					var language_id=e.params.data.id;
					jobportal.language.accesOnclick(language_id);
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.language,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initSubmitForm:function(type,url){
			$('.register-form').on('submit',function(e){
					e.preventDefault();
					var datatoSend=$(this).serialize();
					$.ajax({
			           type: type,
			           url: url,
			           data: datatoSend, // serializes the form's elements.
			           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
			           success: function(data)
			           {
			           		window.location.reload();
			           }
			        });
			 });
	},
	getAllLanguage:function(){
		var currentPagelength=$('#list-table tbody tr').length;
		 jobportal.utilities.ajaxCalls("GET",'/getalllanguages/'+currentPagelength,{},jobportal.language.getallLanguageOnSuccess,"");			 		
	},

	deleteLanguage:function(){
		$('.list-text').on('click',function(e){
			e.preventDefault();
			jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{},function(thisVariable,data){
				 			$('#language-'+data).remove();
				 			if($('#list-table tr').length<2){
				 				$('<div id="no-data-available">No Data Available</div>').insertAfter($('.table-responsive'));
				 			}				 			
				},"");
		});
	},

	editLanguage:function(){
		$('.edit-language').on('click',function(e){
			e.preventDefault();
			var language_id=$(this).attr('data-language-id');
			jobportal.language.accesOnclick(language_id);
		});

	},

	getallLanguageOnSuccess:function(thisVariable,data){

		var jsonData=JSON.parse(data),
			$tableData=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				if(w.id){
					dataToAppend+='<tr id="language-'+w.id+'">'+
		            '<td>'+
		                '<h4><a class="edit-language list-only-text" data-language-id="'+w.id+'" id="language-'+w.id+'" href="javascript:void(0)">'+w.language+'</a></h4>'+
		            '</td>'+                           
		            '<td>'+
		                '<p class="list-text Deactivate" data-url="/languageopr/'+w.id+'">Delete</p>'+
		            '</td></tr>';
		            $('#no-data-available').remove();
				}
			});
			$tableData.append(dataToAppend);
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}
			jobportal.language.editLanguage();	   
			jobportal.language.deleteLanguage();	   
	},

	accesOnclick:function(language_id){
			jobportal.utilities.ajaxCalls("GET",'/languageopr/'+language_id,{},function(thisVariable,data){
						var jsonData=JSON.parse(data);
						$('#edit-submit-success').modal('show');
						jobportal.language.initSubmitForm("PUT",$('#edit-submit-success form').attr('action')+language_id);
						jobportal.utilities.initTriggerElement($('#trigger-update-edit'),$('#update-edit'));
						$('#language-edit').val(jsonData[0].language);
					},"");
	},
    initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.language.getAllLanguage();
    	});
    }

}

