jobportal.usersbulkupload={

	init:function(){
				this.getAllUsers();
				this.initShowMore();
	},

	getAllUsers:function(){
				var currentPagelength=$('#list-table tbody tr').length;
		 		jobportal.utilities.ajaxCalls("GET",'/getalldumpusers/'+currentPagelength,{},jobportal.usersbulkupload.getallUsersOnSuccess,"");	
	},

	getallUsersOnSuccess:function(thisVariable,data){
			var jsonData=JSON.parse(data);
			     var $tableData=$('tbody',$('#list-table')),
			      dataToAppend="";
			     $.each(jsonData,function(i,w){
			     	if(w.main_id){
			     		var username=(w.username)?w.username:"",
									user_category=(w.user_category)?w.user_category:"",	
									first_name=(w.first_name)?w.first_name:"",
									contact_number=(w.contact_number)?w.contact_number:"",
									title=(w.current_designation)?w.current_designation:"",
									city=(w.city)?w.city:"",
									dob=(w.DOB)?w.DOB:"",
									skills=(w.skills)?w.skills:"",
									email=(w.email)?w.email:"",
									is_active=(parseInt(w.is_active))?"yes":"no",
									activate= (parseInt(w.is_active))?"Deactivate":"Activate";
						dataToAppend+='<tr id="user-'+w.main_id+'">'+
										'<td>'+
			                                '<p class="list-text">'+username+'</p>'+
			                            '</td>'+
			                            '<td>'+
			                                '<p class="list-text">'+email+'</p>'+
			                            '</td>'+
			                            '<td>'+
			                                '<p class="list-text">'+first_name+'</p>'+
			                            '</td>'+
			                            '<td>'+
			                                '<p class="list-text">'+contact_number+'</p>'+
			                            '</td>'+
			                             '<td>'+
			                                '<p class="list-text">'+title+'</p>'+
			                            '</td>'+
			                        	'<td>'+
			                                '<p class="list-text">'+skills+'</p>'+
			                            '</td>'+
			                        	'<td>'+
			                                '<p class="list-text">'+city+'</p>'+
			                            '</td>'+
			                        	'<td>'+
			                                '<p class="list-text">'+dob+'</p>'+
			                            '</td>'+
			                           	'<td>'+
			                                '<p class="list-text">'+user_category+'</p>'+
			                           '</tr>';
			     	}
			     });
			$tableData.append(dataToAppend);
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}   
	},
    initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.usersbulkupload.getAllUsers();
    	});
    }

}

