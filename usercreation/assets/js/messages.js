jobportal.messagesrecruiter={
	init:function(){
		this.getAllHistory();
		this.initSendChat();
	},

	getAllHistory:function(){
		var url='/getconversationistoryjobseeker/'+localStorage.getItem('contactmapid');
		jobportal.utilities.ajaxCalls("GET",url,{},jobportal.messagesrecruiter.onChatHistorySuccess,"");
	},
	onChatHistorySuccess:function(thisVariable,data){
		if(data){
			var jsonData=JSON.parse(data),
			 	appendData="",
			 	$messageElement=$('#message-history'),
			 	userId=$messageElement.attr('data-attr-id');
			    $.each(jsonData,function(i,w){
			    	if(userId==w.user_id){
			    	  appendData+='<li tabindex="-1" class="message right appeared">'+
				    	  				'<div class="avatar">'+
				    	  				'</div>'+
				    	  				'<div class="text_wrapper">'+
				    	  					'<div class="text">'+w.message+'</div>'+
				    	  					'<span class="text-username">- '+w.username+'</span>'+
				    	  				'</div>'+
			    	  				'</li>';
			    	}
			    	else{
			    	   appendData+='<li tabindex="-1" class="message left appeared">'+
			    	   					'<div class="avatar"></div>'+
			    	   					'<div class="text_wrapper">'+
			    	   						'<div class="text">'+w.message+'</div>'+
			    	   						'<span class="text-username">- '+w.username+'</span>'+
			    	   					'</div>'+
			    	   				'</li>';
			    	}		
			    });
			$messageElement.append(appendData);
			$('.message').last().focus();
		}
	},
	initSendChat:function(){
			$('#send-chat').on('click',function(e){
				var message=$('.message_input').val().trim();
				if(message){
					var dataToSend={
			           			'contactmapid':localStorage.getItem('contactmapid'),
			           			'message':message
			           		}
					jobportal.utilities.ajaxCalls("POST","/addconversationjobseeker",dataToSend,jobportal.messagesrecruiter.onSuccessChat,"");
				}				
			});
	},
	onSuccessChat:function(thisVariable,data){
		if(data){
			var jsonData=JSON.parse(data);
      		localStorage.setItem('contactmapid',jsonData.id);
      		var appendData='<li tabindex="-1" class="message right appeared">'+
				    	  				'<div class="avatar">'+
				    	  				'</div>'+
				    	  				'<div class="text_wrapper">'+
				    	  					'<div class="text">'+jsonData.message+'</div>'+
				    	  					'<span class="text-username">- '+jsonData.username+'</span>'+
				    	  				'</div>'+
			    	  				'</li>';
			$('#message-history').append(appendData);
			$('.message').last().focus();
			$('.message_input').val(null);
		}
		else{
			alert('No company assigned');
		}
	}
}

jobportal.messagesseeker={
	init:function(){
		this.getAllHistory();
		this.initSendChat();
	},

	getAllHistory:function(){
		var url='/getconversationistory/'+localStorage.getItem('contactseeker');
		jobportal.utilities.ajaxCalls("GET",url,{},jobportal.messagesseeker.onChatHistorySuccess,"");
	},
	onChatHistorySuccess:function(thisVariable,data){
		if(data){
			var jsonData=JSON.parse(data),
			 	appendData="",
			 	$messageElement=$('#message-history'),
			 	userId=$messageElement.attr('data-attr-id');
			    $.each(jsonData,function(i,w){
			    	if(userId==w.user_id){
			    	  appendData+='<li tabindex="-1" class="message right appeared">'+
				    	  				'<div class="avatar">'+
				    	  				'</div>'+
				    	  				'<div class="text_wrapper">'+
				    	  					'<div class="text">'+w.message+'</div>'+
				    	  					'<span class="text-username">- '+w.username+'</span>'+
				    	  				'</div>'+
			    	  				'</li>';
			    	}
			    	else{
			    	   appendData+='<li tabindex="-1" class="message left appeared">'+
			    	   					'<div class="avatar"></div>'+
			    	   					'<div class="text_wrapper">'+
			    	   						'<div class="text">'+w.message+'</div>'+
			    	   						'<span class="text-username">- '+w.username+'</span>'+
			    	   					'</div>'+
			    	   				'</li>';
			    	}
			    	localStorage.setItem('contactmapid',w.company_contact_map_id);		
			    });
			$messageElement.append(appendData);
			$('.message').last().focus();
		}
	},
	initSendChat:function(){
			$('#send-chat').on('click',function(e){
				var message=$('.message_input').val().trim();
				if(message){
					var dataToSend={
			           			'contactmapid':localStorage.getItem('contactmapid'),
			           			'seekerid':localStorage.getItem('contactseeker'),
			           			'message':message
			           		}
					jobportal.utilities.ajaxCalls("POST","/addconversation",dataToSend,jobportal.messagesseeker.onSuccessChat,"");
				}				
			});
	},
	onSuccessChat:function(thisVariable,data){
		if(data){
			var jsonData=JSON.parse(data);
      		localStorage.setItem('contactmapid',jsonData.id);
      		var appendData='<li tabindex="-1" class="message right appeared">'+
				    	  				'<div class="avatar">'+
				    	  				'</div>'+
				    	  				'<div class="text_wrapper">'+
				    	  					'<div class="text">'+jsonData.message+'</div>'+
				    	  					'<span class="text-username">- '+jsonData.username+'</span>'+
				    	  				'</div>'+
			    	  				'</li>';
			$('#message-history').append(appendData);
			$('.message').last().focus();
			$('.message_input').val(null);
		}
		else{
			alert('No company assigned');
		}
	}
}