jobportal.searchCandidates={
	init:function(){
			jobportal.utilities.initSelectSkills();
			jobportal.utilities.initSelectCurrentIndustry();
			jobportal.utilities.initSelectCurrentFunctionArea();
			jobportal.utilities.initSelectAllCompany();
			jobportal.utilities.initSelectCurrentTitle();
			jobportal.utilities.initSelectCity();
			jobportal.utilities.initTriggerElement($('#trigger-search'),$('#update'));
			this.initSubmitForm();
	},
	initSubmitForm:function(){
			$('#register-form').on('submit',function(e){
				e.preventDefault();
				var url=$(this).attr('action'),
					type="POST";
				 jobportal.utilities.ajaxCalls(type,url,$(this).serialize(),jobportal.searchCandidates.onSearchSucces,"");
			 });
	},
	onSearchSucces:function(thisVaridable,data){
		var jsonData=JSON.parse(data),
			$tbody=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				var profile_pic=(w.profile_pic)?w.profile_pic:"/assets/images/50X50.png";
			 dataToAppend+='<tr id="user-'+w.id+'"><td>'+
                                '<a href="#">'+
                                 '<img src="'+profile_pic+'" class="img-responsive img-size-70" alt="logo" title="logo">'+
                                '</a>'+
	                            '</td>'+
	                            '<td>'+
	                                '<h4><a href="javascript:void(0)">'+w.first_name+" "+w.last_name+'</a></h4>'+
	                                '<p><a href="/'+w.resume_path+'">Download resume</a></p>'+
	                            '</td>'+                       
	                            '<td>'+
	                                '<p class="list-text mj_mainbtn mj_btnblue contact-seeker" data-attr-id="'+w.user_id+'" data-text="Contact">Contact</p>'+
	                            '</td></tr>';
	                             $('#no-data-available').remove();
			});
			$tbody.html(dataToAppend);
			jobportal.searchCandidates.initContactSeeker();
	},
	initContactSeeker:function(){
			$('.contact-seeker').on('click',function(){
				 var jobseekerid=$(this).attr('data-attr-id');
				 localStorage.setItem('contactseeker',jobseekerid);
				 setTimeout(function(){
				  window.location.href="messageseeker.php";
				 },500);
			});
	}
}

jobportal.searchJobs={
	init:function(){
			jobportal.utilities.initSelectSkills();
			jobportal.utilities.initSelectCurrentIndustry();
			jobportal.utilities.initSelectCurrentFunctionArea();
			jobportal.utilities.initTriggerElement($('#trigger-search'),$('#update'));
			this.initSubmitForm();
	},
	initSubmitForm:function(){
			$('#register-form').on('submit',function(e){
				e.preventDefault();
				var url=$(this).attr('action'),
					type="POST";
				 jobportal.utilities.ajaxCalls(type,url,$(this).serialize(),jobportal.searchJobs.onSearchSucces,"");
			 });
	},
	onSearchSucces:function(thisVaridable,data){
		var jsonData=JSON.parse(data),
			$tbody=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
			 dataToAppend+='<tr id="search-job-'+w.id+'">'+
	                            '<td>'+
	                                '<h4><a href="javascript:void(0)">'+w.jobname+'</a></h4>'+
	                            '</td>'+   
	                            '<td>'+
	                                '<p class="list-text">'+w.username+'</p>'+
	                            '</td>'+     
	                            '<td>'+
	                                '<p class="list-text">'+w.email+'</p>'+
	                            '</td>'+                 
	                            '<td>'+
	                                '<p class="list-text mj_mainbtn mj_btnblue contact-seeker" data-url="/candidateapplyJob" data-attr-id="'+w.job_id+'" data-text="Apply">Apply</p>'+
	                            '</td>'+
	                        '</tr>';
	                         $('#no-data-available').remove();
			});
			$tbody.html(dataToAppend);
			jobportal.searchJobs.initApplyJob();
	},
	initApplyJob:function(){
			$('.contact-seeker').on('click',function(){
				 var url=$(this).attr('data-url'),
			        job_id=$(this).attr('data-attr-id');
			    jobportal.utilities.ajaxCalls("POST",url,{'id':job_id},function(){ $('#submit-success').modal('show'); },"");
			});
	}
}