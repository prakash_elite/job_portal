jobportal.function={

	init:function(){
				this.getAllFunction();
				this.initShowMore();
				jobportal.utilities.initAddNew(function($thisVariable){
					$('#submit-success').modal('show');
					jobportal.function.initSubmitForm("POST",$('#submit-success form').attr('action')+1);
				    jobportal.utilities.initTriggerElement($('#trigger-update'),$('#update'));
				});
				jobportal.utilities.initSelectJs($('#search-all'),{
					url:'/searchfunctionalarea/',
					isMultiple:false,
					minimumInputLength:1,
				},function(e){
					var function_id=e.params.data.id;
					jobportal.function.accesOnclick(function_id);
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.function_area,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initSubmitForm:function(type,url){
			$('.register-form').on('submit',function(e){
					e.preventDefault();
					var datatoSend=$(this).serialize();
					$.ajax({
			           type: type,
			           url: url,
			           data: datatoSend, // serializes the form's elements.
			           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
			           success: function(data)
			           {
			           		window.location.reload();
			           }
			        });
			 });
	},
	getAllFunction:function(){
		var currentPagelength=$('#list-table tbody tr').length;
		 jobportal.utilities.ajaxCalls("GET",'/getfunctionalreas/'+currentPagelength,{},jobportal.function.getallFunctionOnSuccess,"");			 		
	},

	deleteFunction:function(){
		$('.list-text').on('click',function(e){
			e.preventDefault();
			jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{},function(thisVariable,data){
				 			$('#function-'+data).remove();
				 			if($('#list-table tr').length<2){
				 				$('<div id="no-data-available">No Data Available</div>').insertAfter($('.table-responsive'));
				 			}				 			
				},"");
		});
	},

	editFunction:function(){
		$('.edit-function').on('click',function(e){
			e.preventDefault();
			var function_id=$(this).attr('data-function-id');
			jobportal.function.accesOnclick(function_id);
		});

	},

	getallFunctionOnSuccess:function(thisVariable,data){

		var jsonData=JSON.parse(data),
			$tableData=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				if(w.id){
					dataToAppend+='<tr id="function-'+w.id+'">'+
			            '<td>'+
			                '<h4><a class="edit-function list-only-text" data-function-id="'+w.id+'" id="function-'+w.id+'" href="javascript:void(0)">'+w.function_area+'</a></h4>'+
			            '</td>'+                           
			            '<td>'+
			                '<p class="list-text Deactivate" data-url="/functionalareas/'+w.id+'">Delete</p>'+
			            '</td></tr>';
		            $('#no-data-available').remove();
				}
			});
			$tableData.append(dataToAppend);
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}
			jobportal.function.editFunction();	   
			jobportal.function.deleteFunction();	   
	},

	accesOnclick:function(function_id){
			jobportal.utilities.ajaxCalls("GET",'/functionalareas/'+function_id,{},function(thisVariable,data){
						var jsonData=JSON.parse(data);
						$('#edit-submit-success').modal('show');
						jobportal.function.initSubmitForm("PUT",$('#edit-submit-success form').attr('action')+function_id);
						jobportal.utilities.initTriggerElement($('#trigger-update-edit'),$('#update-edit'));
						$('#function-edit').val(jsonData[0].function_area);
					},"");
	},
    initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.function.getAllFunction();
    	});
    }

}

