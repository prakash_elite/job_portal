jobportal.skill={

	init:function(){
				this.getAllSkill();
				this.initShowMore();
				jobportal.utilities.initAddNew(function($thisVariable){
					$('#submit-success').modal('show');
					jobportal.skill.initSubmitForm("POST",$('#submit-success form').attr('action')+1);
				    jobportal.utilities.initTriggerElement($('#trigger-update'),$('#update'));
				});
				jobportal.utilities.initSelectJs($('#search-all'),{
					url:'/searchskills/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					var skill_id=e.params.data.id;
					jobportal.skill.accesOnclick(skill_id);
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.skill_set,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initSubmitForm:function(type,url){
			$('.register-form').on('submit',function(e){
					e.preventDefault();
					var datatoSend=$(this).serialize();
					$.ajax({
			           type: type,
			           url: url,
			           data: datatoSend, // serializes the form's elements.
			           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
			           success: function(data)
			           {
			           		window.location.reload();
			           }
			        });
			 });
	},
	getAllSkill:function(){
		var currentPagelength=$('#list-table tbody tr').length;
		 jobportal.utilities.ajaxCalls("GET",'/getAllSkill/'+currentPagelength,{},jobportal.skill.getallSkillOnSuccess,"");			 		
	},

	deleteskill:function(){
		$('.list-text').on('click',function(e){
			e.preventDefault();
			jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{},function(thisVariable,data){
				 			$('#skill-'+data).remove();
				 			if($('#list-table tr').length<2){
				 				$('<div id="no-data-available">No Data Available</div>').insertAfter($('.table-responsive'));
				 			}				 			
				},"");
		});
	},

	editskill:function(){
		$('.edit-skill').on('click',function(e){
			e.preventDefault();
			var skill_id=$(this).attr('data-skill-id');
			jobportal.skill.accesOnclick(skill_id);
		});

	},

	getallSkillOnSuccess:function(thisVariable,data){

		var jsonData=JSON.parse(data),
			$tableData=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				if(w.id){
					dataToAppend+='<tr id="skill-'+w.id+'">'+
		            '<td>'+
		                '<h4><a class="edit-skill list-only-text" data-skill-id="'+w.id+'" id="skill-'+w.id+'" href="javascript:void(0)">'+w.skill_set+'</a></h4>'+
		            '</td>'+                           
		            '<td>'+
		                '<p class="list-text Deactivate" data-url="/skillset/'+w.id+'">Delete</p>'+
		            '</td></tr>';
		            $('#no-data-available').remove();
				}
			});
			$tableData.append(dataToAppend);
			jobportal.skill.editskill();	
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}   
			jobportal.skill.deleteskill();	   
	},

	accesOnclick:function(skill_id){
			jobportal.utilities.ajaxCalls("GET",'/skillset/'+skill_id,{},function(thisVariable,data){
						var jsonData=JSON.parse(data);
						$('#edit-submit-success').modal('show');
						jobportal.skill.initSubmitForm("PUT",$('#edit-submit-success form').attr('action')+skill_id);
						jobportal.utilities.initTriggerElement($('#trigger-update-edit'),$('#update-edit'));
						$('#skill-edit').val(jsonData[0].skill_set);
					},"");
	},
    initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.skill.getAllSkill();
    	});
    }

}

