var jobportal=jobportal || {};


jobportal.utilities={
		 ajaxCalls:function (type,url,dataToSend,successFunc,thisVariable){
			$.ajax({
						type:type,
						url:url,
						data:dataToSend, // serializes the form's elements.
						beforeSend: function(request){
									 request.setRequestHeader("Authorization",localStorage.getItem('sess'))	
						},
						success: function(data){
							successFunc(thisVariable,data);
						}
					});
		},
		initSelectJs:function($element,reqObj,onSelectEventCallback,processResultsCallBack){
				$element.select2({
					multiple:reqObj.isMultiple,
				    ajax: {
					    url: function (params) {
					      return reqObj.url+params.term;
					    },
					    data:{},
					    dataType: 'json',
					    beforeSend: function(request){
								 request.setRequestHeader("Authorization",localStorage.getItem('sess'))	
						},
					    processResults: processResultsCallBack,
					  },
					  minimumInputLength: reqObj.minimumInputLength,
				});	

				$element.on("select2:select", function(e) { 
					onSelectEventCallback(e);
				});				
		},
	clearAllLocalStorage:function(){
		localStorage.clear();
	},
	initDatePicker:function($element,defaultDate){
		$element.datepicker({dateFormat: "yy-mm-dd",changeYear: true, changeMonth: true,yearRange: "1900:2100"}).datepicker("setDate",defaultDate);
	},

	initTriggerElement:function($element,$triggerElement,eventAction='click'){
		$element.on(eventAction,function(){
				$triggerElement.trigger('click');
		});
	},
	initAddNew:function(callBack){
		$('#add-new').on('click',function(e){
			e.preventDefault();
			callBack($(this));
		})
	},
	initSelectCity:function(){
			jobportal.utilities.initSelectJs($('#city'),{
					url:'/getcityList/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.city,
					  });
					});
					return {
					    results: results
					};
		        });
	},

	initSelectCountry:function(){
			jobportal.utilities.initSelectJs($('#country'),{
					url:'/getcountryList/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.country,
					  });
					});
					return {
					    results: results
					};
		        });
	},

	initSelectLanguages:function(){
			jobportal.utilities.initSelectJs($('#languages_known'),{
					url:'/getlanguageList/',
					isMultiple:true,
					minimumInputLength:3,
				},function(e){
					
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.language,
					  });
					});
					return {
					    results: results
					};
		        });
	},

	initSelectSkills:function(){
			jobportal.utilities.initSelectJs($('#skills'),{
					url:'/getsklillList/',
					isMultiple:true,
					minimumInputLength:3,
				},function(e){
					
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.skill_set,
					  });
					});
					return {
					    results: results
					};
		        });
	},

	initSelectCurrentIndustry:function(){
			jobportal.utilities.initSelectJs($('#current_industry'),{
					url:'/getindustryList/',
					isMultiple:false,
					minimumInputLength:1,
				},function(e){
					
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.industry,
					  });
					});
					return {
					    results: results
					};
		        });
	},
			
	initSelectCurrentFunctionArea:function(){
			jobportal.utilities.initSelectJs($('#current_functional_area'),{
					url:'/getfunctionList/',
					isMultiple:false,
					minimumInputLength:1,
				},function(e){
					
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.function_area,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	
	initSelectPreferredLocation:function(){
			jobportal.utilities.initSelectJs($('#prefered_location'),{
					url:'/getcityList/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.city,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initSelectCompanyByUserId:function(){
				$("#company-by-user").select2({
				    ajax: {
					    url: function (params) {
					      return '/searchCompanybyuserid/' + params.term;
					    },
					    data:{},
					    dataType: 'json',
					    beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
					    processResults: function (data) {
					          var results = [];
					          $.each(data, function(index, item){
					            results.push({
					              id: item.id,
					              text: item.company_name,
					            });
					          });
					          return {
					              results: results
					          };
		        		},
					  }
				});	
	},
	initSelectAllCompany:function(){
				$("#company").select2({
				    ajax: {
					    url: function (params) {
					      return '/searchAllCompany/' + params.term;
					    },
					    data:{},
					    dataType: 'json',
					    beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
					    processResults: function (data) {
					          var results = [];
					          $.each(data, function(index, item){
					            results.push({
					              id: item.id,
					              text: item.company_name,
					            });
					          });
					          return {
					              results: results
					          };
		        		},
					  }
				});	
	},
	initSelectCurrentTitle:function(){
			jobportal.utilities.initSelectJs($('#current-title'),{
					url:'/getalltitles/',
					isMultiple:false,
					minimumInputLength:1,
				},function(e){
					
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.current_designation,
					    text: item.current_designation,
					  });
					});
					return {
					    results: results
					};
		        });
	}
}