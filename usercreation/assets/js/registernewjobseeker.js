jobportal.registernewjobseeker={

	init:function(){
		jobportal.utilities.initDatePicker($('#DOB'));
		jobportal.utilities.initTriggerElement($('#trigger-upload-resume'),$('#resume'));
		jobportal.utilities.initTriggerElement($('#trigger-profile-upload'),$('#update'));
		jobportal.utilities.initSelectCity();
		jobportal.utilities.initSelectCountry();
		jobportal.utilities.initSelectLanguages();
		jobportal.utilities.initSelectSkills();
		jobportal.utilities.initSelectCurrentIndustry();
		jobportal.utilities.initSelectCurrentFunctionArea();
		jobportal.utilities.initSelectPreferredLocation();
		this.initFormSubmit();
	},

	
	initFormSubmit:function(){
		$('#register-form').on('submit',function(e){
			e.preventDefault();
			var formData = new FormData($(this)[0]);
			$.ajax({
	           type: "POST",
	           url: $(this).attr('action'),
	           data: formData,
	           processData: false, 
	            contentType: false,// serializes the form's elements.
	           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		       },
	           success: function(data){
	           	window.location.href="/login.php";
	           },
	           error:function(xhr){
	           	var $errorElement='#'+xhr.responseText;
	           	$('.error').hide();
	           	$($errorElement).show();
	           	$($errorElement).next('input').focus();
	           }
	        });
		});
	}
	
}

