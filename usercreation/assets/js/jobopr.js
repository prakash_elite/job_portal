jobportal.jobopr={
	init:function(){
			var jobid=localStorage.getItem('job');
			if(jobid){
				this.getJobById(jobid);
			}
			jobportal.jobopr.initSubmitForm(jobid);
			jobportal.utilities.initTriggerElement($('#trigger-job-upload'),$('#update'));
			jobportal.utilities.initSelectSkills();
			jobportal.utilities.initSelectCurrentIndustry();
			jobportal.utilities.initSelectCurrentFunctionArea();
			jobportal.utilities.initSelectCompanyByUserId();
	},
	initSubmitForm:function(jobid){
			$('#register-form').on('submit',function(e){
				e.preventDefault();
				var url=$(this).attr('action'),
					type="POST";
				if(jobid){
					$('#hidden-job').val(jobid);
					 url=$(this).attr('action');
					 type="PUT";
				}
				 jobportal.utilities.ajaxCalls(type,url,
				 										$(this).serialize(),
				 										function(data){			           
											           		window.location.href="jobs.php";
											           },"");
					
			 });
	},

	getJobById:function(jobid){
		 jobportal.utilities.ajaxCalls("GET",$('#register-form').attr('data-job-url')+"/"+jobid,
		 								{},jobportal.jobopr.onSuccessGetById,"");
		},
	onSuccessGetById:function(thisVariable,data){
		var jsonData=JSON.parse(data);
			           		if(jsonData[0].skills){
			           				var skills=jsonData[0].skills.split(',');
			           					$.get('/skillsetbycommaseperated/'+skills,function(resp){
			           						var jsonresp=JSON.parse(resp);
			           						$('#skills-selected').html(jsonresp[0].skills);
			           				});
			           		}
			           		$('#jobname').val(jsonData[0].jobname);
			           		var company_name=(jsonData[0].company_name)?jsonData[0].company_name:" ",
			           				industry=(jsonData[0].industry)?jsonData[0].industry:" ",
			           				function_area=(jsonData[0].function_area)?jsonData[0].function_area:" ",
			           				job_description=(jsonData[0].job_description)?jsonData[0].job_description:" ";
			           		$('#experience').val(jsonData[0].experience);
			           		$('#address').val(jsonData[0].address);
			           		$('#company_selected').html(company_name);
			           		$('#industry_selected').html(industry);
			           		$('#functional_area_selected').html(function_area);
			           		$('#job_description').html(job_description);
	}
	
}