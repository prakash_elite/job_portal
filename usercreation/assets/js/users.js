jobportal.users={

	init:function(){
				this.getAllUsers();
				this.initShowMore();
				jobportal.utilities.initSelectJs($('#search-all'),{
					url:'/searchusers/',
					isMultiple:false,
					minimumInputLength:3,
				},function(e){
					localStorage.setItem('user',e.params.data.id);
			        setTimeout(function(){
			           		window.location.href="useredit.php"
			        },500);
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.main_id,
					    text: item.username,
					  });
					});
					return {
					    results: results
					};
		        });
	},

	getAllUsers:function(){
					var currentPagelength=$('#list-table tbody tr').length;
		 			jobportal.utilities.ajaxCalls("GET",'/getallusers/'+currentPagelength,{},jobportal.users.getallUsersOnSuccess,"");	
	},

	deleteUser:function(){
				$('.list-text').on('click',function(e){
				     e.preventDefault();
				     var thisVariable=$(this);
				     jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{},function(thisVariable,data){
				       var isactive=parseInt(thisVariable.attr('data-is-active')),
				        className=(isactive)?"Activate":"Deactivate",
				        flag=(isactive)?0:1;
				        thisVariable.html(className).removeClass('Deactivate').removeClass('Activate').addClass(className).attr('data-is-active',flag);
				     },thisVariable);
			    });	
	},

	editUser:function(){
				$('.edit-user').on('click',function(e){
				     e.preventDefault();
				     localStorage.setItem('user',$(this).attr('data-user-id'));
				     setTimeout(function(){
				      window.location.href="useredit.php"
				     },500);
			    });

	},

	getallUsersOnSuccess:function(thisVariable,data){
			var jsonData=JSON.parse(data);
			     var $tableData=$('tbody',$('#list-table')),
			      dataToAppend="";
			     $.each(jsonData,function(i,w){
			     	if(w.main_id){
			     	var username=(w.username)?w.username:"",
						user_category=(w.user_category)?w.user_category:"",	
						first_name=(w.first_name)?w.first_name:"",
						last_name=(w.last_name)?w.last_name:"",
						contact_number=(w.contact_number)?w.contact_number:"",
						email=(w.email)?w.email:"",
						profile_pic=(w.profile_pic)?w.profile_pic:"assets/images/70X70.png";
						is_active=(parseInt(w.is_active))?"yes":"no",
						activate= (parseInt(w.is_active))?"Deactivate":"Activate";
						dataToAppend+='<tr><td>'+
                                '<a href="javascript:void(0)">'+
                                 '<img src="'+profile_pic+'" class="img-responsive img-size-70" alt="">'+
                                '</a>'+
	                            '</td>'+
	                            '<td>'+
	                                '<h4><a class="edit-user" data-user-id="'+w.main_id+'" id="user-'+w.main_id+'" href="javascript:void(0)">'+username+'</a></h4>'+
	                                '<p>'+w.email+'</p>'+
	                            '</td>'+
	                            '<td>'+
	                                '<p class="list-text">'+contact_number+'</p>'+
	                            '</td>'+
	                             '<td>'+
	                                '<p class="list-text">'+user_category+'</p>'+
	                            '</td>'+
	                            '<td>'+
	                                '<p class="list-text '+activate+'" data-is-active="'+w.is_active+'" data-url="/users/'+w.main_id+'">'+activate+'</p>'+
	                            '</td></tr>';
			     	}
			     });
			$tableData.append(dataToAppend);
			jobportal.users.editUser();	   
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}
			jobportal.users.deleteUser();
	},
    initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.users.getAllUsers();
    	});
    }

}

