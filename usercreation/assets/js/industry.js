jobportal.industry={

	init:function(){
				this.getAllIndustry();
				this.initShowMore();
				jobportal.utilities.initAddNew(function($thisVariable){
					$('#submit-success').modal('show');
					jobportal.industry.initSubmitForm("POST",$('#submit-success form').attr('action')+1);
				    jobportal.utilities.initTriggerElement($('#trigger-update'),$('#update'));
				});
				jobportal.utilities.initSelectJs($('#search-all'),{
					url:'/searchindustry/',
					isMultiple:false,
					minimumInputLength:1,
				},function(e){
					var industry_id=e.params.data.id;
					jobportal.industry.accesOnclick(industry_id);
				},function (data) {
					var results = [];
					$.each(data, function(index, item){
					  results.push({
					    id: item.id,
					    text: item.industry,
					  });
					});
					return {
					    results: results
					};
		        });
	},
	initSubmitForm:function(type,url){
			$('.register-form').on('submit',function(e){
					e.preventDefault();
					var datatoSend=$(this).serialize();
					$.ajax({
			           type: type,
			           url: url,
			           data: datatoSend, // serializes the form's elements.
			           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
			           success: function(data)
			           {
			           		window.location.reload();
			           }
			        });
			 });
	},
	getAllIndustry:function(){
		var currentPagelength=$('#list-table tbody tr').length;		
		jobportal.utilities.ajaxCalls("GET",'/getindustry/'+currentPagelength,{},jobportal.industry.getallIndustryOnSuccess,"");			 		
	},

	deleteIndustry:function(){
		$('.list-text').on('click',function(e){
			e.preventDefault();
			jobportal.utilities.ajaxCalls("DELETE",$(this).attr('data-url'),{},function(thisVariable,data){
				 			$('#industry-'+data).remove();
				 			if($('#list-table tr').length<2){
				 				$('<div id="no-data-available">No Data Available</div>').insertAfter($('.table-responsive'));
				 			}				 			
				},"");
		});
	},

	editIndustry:function(){
		$('.edit-industry').on('click',function(e){
			e.preventDefault();
			var industry_id=$(this).attr('data-industry-id');
			jobportal.industry.accesOnclick(industry_id);
		});

	},

	getallIndustryOnSuccess:function(thisVariable,data){

		var jsonData=JSON.parse(data),
			$tableData=$('tbody',$('#list-table')),
			dataToAppend="";
			$.each(jsonData,function(i,w){
				if(w.id){
					dataToAppend+='<tr id="industry-'+w.id+'">'+
		            '<td>'+
		                '<h4><a class="edit-industry list-only-text" data-industry-id="'+w.id+'" id="industry-'+w.id+'" href="javascript:void(0)">'+w.industry+'</a></h4>'+
		            '</td>'+                           
		            '<td>'+
		                '<p class="list-text Deactivate" data-url="/industries/'+w.id+'">Delete</p>'+
		            '</td></tr>';
		            $('#no-data-available').remove();
				}
			});
			$tableData.append(dataToAppend);
			if(!jsonData.has_more){
				$('#show-more').html('No more data');
			}
			jobportal.industry.editIndustry();	   
			jobportal.industry.deleteIndustry();	   
	},

	accesOnclick:function(industry_id){
			jobportal.utilities.ajaxCalls("GET",'/industries/'+industry_id,{},function(thisVariable,data){
						var jsonData=JSON.parse(data);
						$('#edit-submit-success').modal('show');
						jobportal.industry.initSubmitForm("PUT",$('#edit-submit-success form').attr('action')+industry_id);
						jobportal.utilities.initTriggerElement($('#trigger-update-edit'),$('#update-edit'));
						$('#industry-edit').val(jsonData[0].industry);
					},"");
	},
    initShowMore:function(){
    	$('#show-more').on('click',function(){
    		jobportal.industry.getAllIndustry();
    	});
    }

}

