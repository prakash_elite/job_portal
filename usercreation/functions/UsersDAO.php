<?php

class UsersDAO extends AbstractDAO{
    public static function registerUser($name,$password,$email,$user_map_id){
    	if(check_form_validation(func_get_args())){
	    	$query="INSERT INTO user_registration (username,password,email,user_map_id) VALUES(:username,:password,:email,:user_map_id)";
	         return self::insertQuery($query,array('username'=>$name,'password'=>$password,'email'=>$email,'user_map_id'=>$user_map_id));
	      }
    }
    public static function updateRegisteredUserEmail($user_id,$email,$target_profile_file){
    	if(check_form_validation(func_get_args())){
	    	$query="UPDATE user_registration SET email=:email,profile_pic=:profile_pic WHERE id=:user_id";
	         return self::insertQuery($query,array('user_id'=>$user_id,'email'=>$email,'profile_pic'=>$target_profile_file));
	      }
    }
    public static function updateRegisteredUserPassword($user_id,$password){
    	if(check_form_validation(func_get_args())){
	    	$query="UPDATE user_registration SET password=:password WHERE id=:user_id";
	         return self::insertQuery($query,array('user_id'=>$user_id,'password'=>$password));
	      }
    }

    public static function checkUserNameAlreadyExists($username){
            $query="SELECT * FROM user_registration WHERE username=:username";
            $result=self::fetchQuery($query,array('username'=>$username));
            return count($result);
    }

    public static function checkEmailAlreadyExists($email){
            $query="SELECT * FROM user_registration WHERE email=:email";
            $result=self::fetchQuery($query,array('email'=>$email));
            return count($result);
    }
    
    public static function checkContactAlreadyExists($contact_number){
            $query="SELECT * FROM job_seeker_details WHERE contact_number=:contact_number";
            $result=self::fetchQuery($query,array('contact_number'=>$contact_number));
            return count($result);
    }

    public static function checkUserNameAlreadyExistsForUserId($username,$user_id){
            $query="SELECT * FROM user_registration WHERE username=:username AND id !=:user_id";
            $result=self::fetchQuery($query,array('username'=>$username,'user_id'=>$user_id));
            return count($result);
    }

    public static function checkEmailAlreadyExistsForUserId($email,$user_id){
            $query="SELECT * FROM user_registration WHERE email=:email AND id !=:user_id";
            $result=self::fetchQuery($query,array('email'=>$email,'user_id'=>$user_id));
            return count($result);
    }
    
    public static function checkContactAlreadyExistsForUserId($contact_number,$user_id){
            $query="SELECT * FROM job_seeker_details WHERE contact_number=:contact_number AND user_id !=:user_id";
            $result=self::fetchQuery($query,array('contact_number'=>$contact_number,'user_id'=>$user_id));
            return count($result);
    }

    public static function validUser($username,$password){
    	$isValidUser=false;
    	if(check_form_validation(func_get_args())){
    		$validPassword=md5(Configuration::$salt . $password);
	    	$query="SELECT * FROM user_registration WHERE username=:username AND password=:password AND is_active=1";
	         $result= self::fetchQuery($query,array('username'=>$username,'password'=>$validPassword));
	         if(count($result)){
	         	$isValidUser=array('id'=>$result[0]['id'],'usertype'=>$result[0]['user_map_id'],'profile_pic'=>$result[0]['profile_pic']);
	         }
	      }
	    return $isValidUser;
    }

    public static function insertLoginTime($user_id){
            $query="UPDATE user_registration SET last_login=now() WHERE id=:user_id";
            return self::updateQuery($query,array('user_id'=>$user_id));
    }

    public static function getAllUsers($userId,$pageoffset){
	    	$query="SELECT ur.id as main_id,ur.username,ur.email,um.user_category,jsd.first_name,jsd.last_name,jsd.contact_number,ur.is_active,ur.profile_pic as profile_pic FROM user_registration ur JOIN user_map um ON um.id=ur.user_map_id LEFT JOIN job_seeker_details jsd ON jsd.user_id =ur.id WHERE ur.id !=:user_id".$pageoffset;
            $result=self::fetchQuery($query,array('user_id'=>$userId));	
            $result['has_more']=count($result);        
            return $result;
    }

   public static function deleteUser($user_id){
	    	$query="UPDATE user_registration SET is_active=CASE WHEN is_active=0 THEN 1 ELSE 0 END WHERE id=:user_id";
	        return self::deleteQuery($query,array('user_id'=>$user_id));
    }

    public static function getSkill($skillId){
	    	$query="SELECT * FROM skill_set WHERE id=:skill_id";
	        return self::fetchQuery($query,array('skill_id'=>$skillId));
    }

    public static function addSkill($skillName){
	    	$query="INSERT INTO skill_set(skill_set) VALUES (:skill)";
	        return self::insertQuery($query,array('skill'=>$skillName));
    }

    public static function updateSkill($skillId,$skillName){
	    	$query="UPDATE skill_set SET skill_set=:skill WHERE id=:skill_id";
	        return self::updateQuery($query,array('skill_id'=>$skillId,'skill'=>$skillName));
    }

    public static function deleteSkill($skillId){
	    	$query="DELETE FROM skill_set WHERE id=:skill_id";
	        return self::deleteQuery($query,array('skill_id'=>$skillId));
    }

   public static function insertJobSeekerId($JobseekerUserId){
        $query="INSERT INTO job_seeker_details(user_id) VALUES (:user_id)";
        return self::insertQuery($query,array("user_id"=>$JobseekerUserId));
    }
    
    public static function insertJobSeekerProfile($user_id,$profileObj,$resume_path){
    	$query="INSERT INTO job_seeker_details(user_id,first_name,last_name,gender,DOB,contact_number,martial_status,address_line_1,address_line_2,state,pincode,city,country,countries_authorized_to_work,languages_known,current_designation,current_company,current_CTC,expected_CTC,duration_notice_period,serving_notice_period,skills,current_location,last_working_day,current_industry,current_functional_area,course,specialization,university_or_college,course_type,passing_year,desired_employment_type,desired_job_type,prefered_location,website_link,resume_path,highest_education,experience) VALUES (:user_id,:first_name,:last_name,:gender,:DOB,:contact_number,:martial_status,:address_line_1,:address_line_2,:state,:pincode,:city,:country,:countries_authorized_to_work,:languages_known,:current_designation,:current_company,:current_CTC,:expected_CTC,:duration_notice_period,:serving_notice_period,:skills,:current_location,:last_working_day,:current_industry,:current_functional_area,:course,:specialization,:university_or_college,:course_type,:passing_year,:desired_employment_type,:desired_job_type,:prefered_location,:website_link,:resume_path,:highest_education,:experience)";
        $profileObj = add_key_to_array($profileObj);
	    return self::insertQuery($query,array("user_id"=>$user_id,"first_name"=>$profileObj['first_name'],"last_name"=>$profileObj['last_name'],"gender"=>$profileObj['gender'],"DOB"=>$profileObj['DOB'],"contact_number"=>$profileObj['contact_number'],"martial_status"=>"","address_line_1"=>$profileObj['address_line_1'],"address_line_2"=>$profileObj['address_line_2'],"state"=>$profileObj['state'],"pincode"=>$profileObj['pincode'],"city"=>$profileObj['city'],"country"=>$profileObj['country'],"countries_authorized_to_work"=>"","languages_known"=>implode(",",$profileObj['languages_known']),"current_designation"=>$profileObj['current_designation'],"current_company"=>$profileObj['current_company'],"current_CTC"=>$profileObj['current_CTC'],"expected_CTC"=>$profileObj['expected_CTC'],"duration_notice_period"=>$profileObj['duration_notice_period'],"serving_notice_period"=>"","skills"=>implode(",",$profileObj['skills']),"current_location"=>"","last_working_day"=>"","current_industry"=>$profileObj['current_industry'],"current_functional_area"=>$profileObj['current_functional_area'],"course"=>"","specialization"=>$profileObj['specialization'],"university_or_college"=>$profileObj['university_or_college'],"course_type"=>"","passing_year"=>"","desired_employment_type"=>$profileObj['desired_employment_type'],"desired_job_type"=>$profileObj['desired_job_type'],"prefered_location"=>implode(",",$profileObj['prefered_location']),"website_link"=>$profileObj['website_link'],"resume_path"=>$resume_path,"highest_education"=>$profileObj['highest_education'],"experience"=>$profileObj['experience']));
    }


    public static function updateJobSeekerProfile($user_id,$profileObj,$resume_path){
    	$query="UPDATE job_seeker_details SET first_name=:first_name,last_name=:last_name,gender=:gender,DOB=:DOB,contact_number=:contact_number,address_line_1=:address_line_1,address_line_2=:address_line_2,state=:state,pincode=:pincode,city=:city,country=:country,languages_known=:languages_known,current_designation=:current_designation,current_company=:current_company,current_CTC=:current_CTC,expected_CTC=:expected_CTC,duration_notice_period=:duration_notice_period,skills=:skills,current_location=:current_location,current_industry=:current_industry,current_functional_area=:current_functional_area,specialization=:specialization,university_or_college=:university_or_college,desired_employment_type=:desired_employment_type,desired_job_type=:desired_job_type,prefered_location=:prefered_location,website_link=:website_link,resume_path=:resume_path,highest_education=:highest_education,experience=:experience WHERE user_id=:user_id";
            $profileObj = add_key_to_array($profileObj);
	    return self::insertQuery($query,array("user_id"=>$user_id,"first_name"=>$profileObj['first_name'],"last_name"=>$profileObj['last_name'],"gender"=>$profileObj['gender'],"DOB"=>$profileObj['DOB'],"contact_number"=>$profileObj['contact_number'],"address_line_1"=>$profileObj['address_line_1'],"address_line_2"=>$profileObj['address_line_2'],"state"=>$profileObj['state'],"pincode"=>$profileObj['pincode'],"city"=>$profileObj['city'],"country"=>$profileObj['country'],"languages_known"=>implode(",",$profileObj['languages_known']),"current_designation"=>$profileObj['current_designation'],"current_company"=>$profileObj['current_company'],"current_CTC"=>$profileObj['current_CTC'],"expected_CTC"=>$profileObj['expected_CTC'],"duration_notice_period"=>$profileObj['duration_notice_period'],"skills"=>implode(",",$profileObj['skills']),"current_location"=>(isset($profileObj['current_location']))?$profileObj['current_location']:"","current_industry"=>$profileObj['current_industry'],"current_functional_area"=>$profileObj['current_functional_area'],"specialization"=>$profileObj['specialization'],"university_or_college"=>$profileObj['university_or_college'],"desired_employment_type"=>$profileObj['desired_employment_type'],"desired_job_type"=>$profileObj['desired_job_type'],"prefered_location"=>$profileObj['prefered_location'],"website_link"=>$profileObj['website_link'],"resume_path"=>$resume_path,"highest_education"=>$profileObj['highest_education'],"experience"=>$profileObj['experience']));
    }

    public static function searchCityList($searchParam){
    		self::set_char_UTF();
	    	$query='SELECT * FROM city WHERE city LIKE "'.$searchParam.'%"';
	        return self::fetchQuery($query,array());
    }

    public static function searchCountryList($searchParam){
    		self::set_char_UTF();
	    	$query='SELECT * FROM country WHERE country LIKE "'.$searchParam.'%"';
	        return self::fetchQuery($query,array());
    }   

    public static function searchLanguageList($searchParam){
    		self::set_char_UTF();
	    	$query='SELECT * FROM languages WHERE language LIKE "'.$searchParam.'%"';
	        return self::fetchQuery($query,array());
    }  
    public static function searchSkillList($searchParam){
    		self::set_char_UTF();
	    	$query='SELECT * FROM skill_set WHERE skill_set LIKE "'.$searchParam.'%"';
	        return self::fetchQuery($query,array());
    }  

    public static function searchIndustryList($searchParam){
    		self::set_char_UTF();
	    	$query='SELECT * FROM industrydetails WHERE industry LIKE "'.$searchParam.'%"';
	        return self::fetchQuery($query,array());
    }  

    public static function searchFunctionList($searchParam){
    		self::set_char_UTF();
	    	$query='SELECT * FROM function_area WHERE function_area LIKE "'.$searchParam.'%"';
	        return self::fetchQuery($query,array());
    }  

    public static function postJobDetails($user_id,$jobObj){
	    	$query="INSERT INTO job_post_details (jobname,experience,skills,address,industry,functional_area,posted_by,company_id,job_description) VALUES(:jobname,:experience,:skills,:address,:industry,:functional_area,:posted_by,:company_id,:job_description)";
	         return self::insertQuery($query,array("jobname"=>$jobObj['jobname'],"experience"=>$jobObj['experience'],"skills"=>implode(",",$jobObj['skills']),"address"=>$jobObj['address'],"industry"=>$jobObj['industry'],"functional_area"=>$jobObj['functional_area'],"company_id"=>$jobObj['company'],"posted_by"=>$user_id,'job_description'=>$jobObj['job_description']));
	      }

    public static function updateJobDetails($user_id,$jobObj){
	    	$query="UPDATE job_post_details SET jobname=:jobname,experience=:experience,skills=:skills,address=:address,industry=:industry,functional_area=:functional_area,company_id=:company_id,job_description=:job_description WHERE posted_by=:posted_by AND id=:job_id";
	         return self::insertQuery($query,array("jobname"=>$jobObj['jobname'],"experience"=>$jobObj['experience'],"skills"=>implode(",",$jobObj['skills']),"address"=>$jobObj['address'],"industry"=>$jobObj['industry'],"functional_area"=>$jobObj['functional_area'],"company_id"=>$jobObj['company'],"job_id"=>$jobObj['job_id'],"posted_by"=>$user_id,'job_description'=>$jobObj['job_description']));
	      }

	public static function addCompany($user_id,$profileObj,$target_profile_file){
	    	$query="INSERT INTO company_details(company_name,number_of_employees,address,industry,added_by,company_logo) VALUES (:company_name,:number_of_employees,:address,:industry,:added_by,:company_logo)";
	        return self::insertQuery($query,array("company_name"=>$profileObj['company_name'],"number_of_employees"=>$profileObj['number_of_employees'],"address"=>$profileObj['address'],"industry"=>$profileObj['industry'],"added_by"=>$user_id,'company_logo'=>$target_profile_file));
    }

    public static function listCompany($userId,$pageoffset){
	    	$query="SELECT cd.id,cd.company_name,cd.number_of_employees,cd.address,ur.username as added_by,id.industry,cd.company_logo  FROM company_details cd LEFT JOIN company_user_map cu ON cu.company_id=cd.id JOIN user_registration ur on ur.id = cu.user_id LEFT JOIN industrydetails id ON id.id=cd.industry WHERE cu.user_id=:user_id GROUP BY cd.id ".$pageoffset;
            $result=self::fetchQuery($query,array('user_id'=>$userId));
            $result['has_more']=count($result); 
	        return $result;
    }

    public static function updateCompany($userId,$companyid,$profileObj,$target_profile_file){
	    	$query="UPDATE company_details SET company_name=:company_name,number_of_employees=:number_of_employees,address=:address,industry=:industry,company_logo=:company_logo WHERE id=:companyid AND added_by=:added_by";
	        return self::updateQuery($query,array('companyid'=>$companyid,'added_by'=>$userId,"company_name"=>$profileObj['company_name'],"number_of_employees"=>$profileObj['number_of_employees'],"address"=>$profileObj['address'],"industry"=>$profileObj['industry'],'company_logo'=>$target_profile_file));
    }    
    public static function getCompanyDetails($userId,$companyId){
	    	$query="SELECT * FROM company_details cd LEFT JOIN industrydetails id ON id.id=cd.industry WHERE cd.added_by=:user_id AND cd.id=:companyid";
	        return self::fetchQuery($query,array('user_id'=>$userId,'companyid'=>$companyId));
    }
 	public static function searchCompanybyuserid($searchParam,$quertString){
	    	$query='SELECT * FROM company_details cd WHERE'.$quertString.' company_name LIKE "'.$searchParam.'%"';
	        return self::fetchQuery($query,array());
    }	    
   public static function listJobsByUserId($userId,$pageoffset){
	    	$query="SELECT jpd.id,jpd.jobname,jpd.experience,jpd.skills,id.industry,fa.function_area,cd.company_name FROM job_post_details jpd LEFT JOIN company_details cd ON cd.id = jpd.company_id LEFT JOIN industrydetails id ON id.id=jpd.industry LEFT JOIN function_area fa ON fa.id=jpd.functional_area WHERE jpd.posted_by=:user_id ".$pageoffset;
            $result=self::fetchQuery($query,array('user_id'=>$userId));
            $result['has_more']=count($result); 
	        return $result;
    }    
   public static function getSkillByCommaseperated($skillId){
	    	$query="SELECT GROUP_CONCAT(skill_set) as skills FROM skill_set WHERE id IN ($skillId)";
	        return self::fetchQuery($query,array());
    }        

    public static function getJobDetails($userId,$jobid){
	    	$query="SELECT jpd.id,jpd.address,jpd.jobname,jpd.experience,jpd.skills,id.industry,fa.function_area,cd.company_name,jpd.job_description FROM job_post_details jpd LEFT JOIN company_details cd ON cd.id = jpd.company_id LEFT JOIN industrydetails id ON id.id=jpd.industry LEFT JOIN function_area fa ON fa.id=jpd.functional_area WHERE jpd.posted_by=:user_id AND jpd.id=:jobid";
	        return self::fetchQuery($query,array('user_id'=>$userId,'jobid'=>$jobid));
    }

    public static function getCandidatesSearch($candidateObj){
            $query="SELECT usr.profile_pic,jsd.first_name,jsd.last_name,jsd.resume_path,jsd.user_id,jsd.id FROM job_seeker_details jsd JOIN user_registration usr ON usr.id=jsd.user_id WHERE";
            $candidateObj=add_key_to_array($candidateObj);
            if(count($candidateObj['skills'])){
                $query.=" skills IN (".implode(",",$candidateObj['skills']).") OR";
            }  
            if(isset($candidateObj['highest_education']) && !empty(isset($candidateObj['highest_education']))){
                $query.=" highest_education=".$candidateObj['highest_education']." OR ";
            } 
            if(isset($candidateObj['experience']) && !empty($candidateObj['experience'])){
                $query.=" experience<=".$candidateObj['experience']." OR ";
            } 
            if(isset($candidateObj['industry']) && !empty($candidateObj['industry'])){
                $query.=" current_industry='".$candidateObj['industry']."' OR ";
            } 
            if(isset($candidateObj['current_functional_area']) && !empty($candidateObj['current_functional_area'])){
                $query.=" current_functional_area='".$candidateObj['current_functional_area']."' OR ";
            } 
            if(isset($candidateObj['current_designation']) && !empty($candidateObj['current_designation'])){
                $query.=" current_designation='".$candidateObj['current_designation']."' OR ";
            }                                                             
            if(isset($candidateObj['expected_CTC']) && !empty($candidateObj['expected_CTC'])){
                $query.=" expected_CTC='".$candidateObj['expected_CTC']."' OR ";
            }  
            if(isset($candidateObj['current_company']) && !empty($candidateObj['current_company'])){
                $query.=" current_company='".$candidateObj['current_company']."' OR ";
            }  
            if(isset($candidateObj['city']) && !empty($candidateObj['city'])){
                $query.=" city=".$candidateObj['city']." OR ";
            }  
            if(isset($candidateObj['last_login']) && !empty($candidateObj['last_login'])){
                $query.=" PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM NOW()), EXTRACT(YEAR_MONTH FROM usr.last_login))<=".$candidateObj['last_login']." OR ";
            }  
            if(isset($candidateObj['desired_employment_type']) && !empty($candidateObj['desired_employment_type'])){
                $query.=" desired_employment_type=".$candidateObj['desired_employment_type'];
            }            
	        return self::fetchQuery($query,array());
    }


    public static function getJobSearch($candidateObj){

	    	$query="SELECT * ,jpd.id as job_id FROM job_post_details jpd JOIN user_registration ur ON ur.id=jpd.posted_by WHERE";
	    	if(isset($candidateObj['skills']) && !empty($candidateObj['skills'])){
    			$query.=" jpd.skills IN (".implode(",",$candidateObj['skills']).") OR ";
    		}
    		$functional_area="";
	    	if(isset($candidateObj['functional_area']) && !empty($candidateObj['functional_area'])){
    			$functional_area=$candidateObj['functional_area'];
    		}
    		$industry="";
	    	if(isset($candidateObj['industry']) && !empty($candidateObj['industry'])){
    			$industry=$candidateObj['industry'];
    		}
    		$experience="";
	    	if(isset($candidateObj['experience']) && !empty($candidateObj['experience'])){
    			$experience=$candidateObj['experience'];
    		}
    		$query.=" jpd.experience=:experience OR jpd.industry=:industry OR jpd.functional_area=:function_area";
	        return self::fetchQuery($query,array('experience'=>$experience,'industry'=>$industry,'function_area'=>$functional_area));
    }
   public static function getUserDetailsByid($userId){
	    	$query="SELECT *,ur.profile_pic as profile_pic,DATE(jsd.DOB) AS DOB,(SELECT city FROM city WHERE id=jsd.prefered_location) as prefered_location FROM user_registration ur LEFT JOIN job_seeker_details jsd ON jsd.user_id=ur.id LEFT JOIN industrydetails id ON id.id=jsd.current_industry LEFT JOIN function_area fa ON fa.id=jsd.current_functional_area LEFT JOIN city ci ON ci.id=jsd.city LEFT JOIN country co ON co.id =jsd.country WHERE ur.id=:user_id";
	        return self::fetchQuery($query,array('user_id'=>$userId));
    }    

    public static function getlanguagecommaseperated($languages){
	    	$query="SELECT GROUP_CONCAT(language) as language FROM languages WHERE id IN ($languages)";
	        return self::fetchQuery($query,array());
    }

    public static function candidateApplyJob($jobseeker_id,$job_id){
	    	$query="INSERT INTO jobseeker_applied_job(jobseeker_id,job_id,is_applied_by_job_seeker) VALUES (:jobseeker_id,:job_id,1)";
	        return self::insertQuery($query,array('jobseeker_id'=>$jobseeker_id,'job_id'=>$job_id));
    }    
  public static function assignCompany($userid,$companyid){
            $query="INSERT INTO company_user_map(user_id,company_id) VALUES (:user_id,:company_id)";
            return self::insertQuery($query,array('user_id'=>$userid,'company_id'=>$companyid));
    } 
    public static function getJobAppliedDetailsByUser($user_id){
	    	$query="SELECT jpd.jobname,DATE_FORMAT(jaj.insert_datetime,'%b %d %Y') as applied_on FROM jobseeker_applied_job jaj JOIN job_post_details jpd ON jpd.id=jaj.job_id WHERE jaj.jobseeker_id=:user_id GROUP BY jpd.id;";
	        return self::fetchQuery($query,array('user_id'=>$user_id));
    }    


   public static function listJobsByUserIdSearch($userId,$searchParam){
            $query="SELECT jpd.id,jpd.jobname,jpd.experience,jpd.skills,id.industry,fa.function_area,cd.company_name FROM job_post_details jpd LEFT JOIN company_details cd ON cd.id = jpd.company_id LEFT JOIN industrydetails id ON id.id=jpd.industry LEFT JOIN function_area fa ON fa.id=jpd.functional_area WHERE jpd.posted_by=:user_id AND jpd.jobname LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array('user_id'=>$userId));
    }  

    public static function listCompanySearchByUserId($userId,$searchParam){
            $query="SELECT * FROM company_details WHERE added_by=:user_id AND company_name LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array('user_id'=>$userId));
    }


    public static function SearchAllAppliedJobsForJobSeeker($user_id,$searchParam){
            $query="SELECT jpd.id as job_id,jpd.jobname,DATE_FORMAT(jaj.insert_datetime,'%b %d %Y') as applied_on FROM jobseeker_applied_job jaj JOIN job_post_details jpd ON jpd.id=jaj.job_id WHERE jaj.jobseeker_id=:user_id AND jpd.jobname LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array('user_id'=>$user_id));
    } 
    //admin

    public static function listcompanies($pageoffset){
	    	$query="SELECT cd.id,cd.company_name,cd.number_of_employees,cd.address,ur.username as added_by,id.industry,cd.company_logo FROM company_details cd JOIN user_registration ur on ur.id = cd.added_by LEFT JOIN industrydetails id ON id.id=cd.industry ".$pageoffset;
            $result=self::fetchQuery($query,array());
            $result['has_more']=count($result); 
	        return $result;
    }   

      public static function updateCompaniesByAdmin($companyid,$profileObj,$target_profile_file){
	    	$query="UPDATE company_details SET company_name=:company_name,number_of_employees=:number_of_employees,address=:address,industry=:industry,company_logo=:company_logo WHERE id=:companyid";
	        return self::updateQuery($query,array('companyid'=>$companyid,"company_name"=>$profileObj['company_name'],"number_of_employees"=>$profileObj['number_of_employees'],"address"=>$profileObj['address'],"industry"=>$profileObj['industry'],'company_logo'=>$target_profile_file));
    } 

    public static function deleteCompanies($companyid){
	    	$query="DELETE FROM company_details WHERE id=:companyid";
	        return self::deleteQuery($query,array('companyid'=>$companyid));
    } 

    public static function deleteCompanyUsermap($user_id,$companyid){
            $query="DELETE FROM company_user_map WHERE company_id=:companyid AND user_id=:user_id";
            return self::deleteQuery($query,array('user_id'=>$user_id,'companyid'=>$companyid));
    } 

    public static function deleteCompanyUsermapAll($companyid){
            $query="DELETE FROM company_user_map WHERE company_id=:companyid";
            return self::deleteQuery($query,array('companyid'=>$companyid));
    }

    public static function getCompanyDetailsByIdAdmin($companyId){
	    	$query="SELECT * FROM company_details cd LEFT JOIN industrydetails id ON id.id=cd.industry WHERE cd.id=:companyid";
	        return self::fetchQuery($query,array('companyid'=>$companyId));
    }

    public static function listJobs($pageoffset){
	    	$query="SELECT ur.username,jpd.id,jpd.jobname,jpd.experience,jpd.skills,id.industry,fa.function_area,cd.company_name FROM job_post_details jpd JOIN user_registration ur on ur.id = jpd.posted_by LEFT JOIN company_details cd ON cd.id = jpd.company_id LEFT JOIN industrydetails id ON id.id=jpd.industry LEFT JOIN function_area fa ON fa.id=jpd.functional_area ".$pageoffset;
             $result= self::fetchQuery($query,array());
             $result['has_more']=count($result);   
	        return $result;
    }   

    public static function deleteJobs($jobsid){
	    	$query="DELETE FROM job_post_details WHERE id=:jobid";
	        return self::deleteQuery($query,array('jobid'=>$jobsid));
    } 

    public static function getJobsDetailsByIdAdmin($jobid){
	    	$query="SELECT jpd.id,jpd.address,jpd.jobname,jpd.experience,jpd.skills,id.industry,fa.function_area,cd.company_name,jpd.job_description FROM job_post_details jpd LEFT JOIN company_details cd ON cd.id = jpd.company_id LEFT JOIN industrydetails id ON id.id=jpd.industry LEFT JOIN function_area fa ON fa.id=jpd.functional_area WHERE jpd.id=:jobid";
	        return self::fetchQuery($query,array('jobid'=>$jobid));
    }

    public static function searchCompanybyAdminid($searchParam){
	    	$query='SELECT * FROM company_details cd WHERE company_name LIKE "'.$searchParam.'%"';
	        return self::fetchQuery($query,array());
    }	


    public static function updateJobsByAdmin($jobObj){
	    	$query="UPDATE job_post_details SET jobname=:jobname,experience=:experience,skills=:skills,address=:address,industry=:industry,functional_area=:functional_area,company_id=:company_id,job_description=:job_description WHERE id=:job_id";
	         return self::insertQuery($query,array("jobname"=>$jobObj['jobname'],"experience"=>$jobObj['experience'],"skills"=>implode(",",$jobObj['skills']),"address"=>$jobObj['address'],"industry"=>$jobObj['industry'],"functional_area"=>$jobObj['functional_area'],"company_id"=>$jobObj['company'],"job_id"=>$jobObj['job_id'],"job_description"=>$jobObj['job_description']));
	      }
	public static function getAllSkill($pageoffset){
	    	$query="SELECT * FROM skill_set ".$pageoffset;
            $result=self::fetchQuery($query,array());
            $result['has_more']=count($result); 
	        return $result;
    }

    public static function getAllCountry($pageoffset){
	    	$query="SELECT * FROM country ".$pageoffset;
            $result=self::fetchQuery($query,array());
            $result['has_more']=count($result); 
	        return $result;
    }

   public static function deleteCountry($countryid){
	    	$query="DELETE FROM country WHERE id=:countryid";
	        return self::deleteQuery($query,array('countryid'=>$countryid));
    }     

	public static function getCountryById($countryid){
		    	$query="SELECT * FROM country WHERE id=:id";
		        return self::fetchQuery($query,array('id'=>$countryid));	
	}   

	public static function updateCountry($countryid,$country){
		    	$query="UPDATE country SET country =:country WHERE id=:id";
		        return self::updateQuery($query,array('id'=>$countryid,'country'=>$country));	
	} 	

	public static function addCountry($country){
		    	$query="INSERT INTO country (country) VALUES (:country)";
		        return self::insertQuery($query,array('country'=>$country));	
	} 

    public static function getAlllanguages($pageoffset){
	    	$query="SELECT * FROM languages ".$pageoffset;
            $result=self::fetchQuery($query,array());
            $result['has_more']=count($result); 
	        return $result;
    }	

    public static function getLanguageById($languageid){
	    	$query="SELECT * FROM languages WHERE id=:id";
	        return self::fetchQuery($query,array('id'=>$languageid));
    }

	public static function updateLanguage($languageid,$language){
		    	$query="UPDATE languages SET language =:language WHERE id=:id";
		        return self::updateQuery($query,array('id'=>$languageid,'language'=>$language));	
	} 	

	public static function addLanguage($language){
		    	$query="INSERT INTO languages (language) VALUES (:language)";
		        return self::insertQuery($query,array('language'=>$language));	
	} 	

   public static function deleteLanguage($languageid){
	    	$query="DELETE FROM languages WHERE id=:language";
	        return self::deleteQuery($query,array('language'=>$languageid));
    }  


    public static function getAllFunctionalAreas($pageoffset){
	    	$query="SELECT * FROM function_area ".$pageoffset;
            $result=self::fetchQuery($query,array());
            $result['has_more']=count($result); 
	        return $result;
    }	

    public static function getFunctionalAreasById($function_areaid){
	    	$query="SELECT * FROM function_area WHERE id=:id";
	        return self::fetchQuery($query,array('id'=>$function_areaid));
    }

	public static function updateFunctionalAreas($function_areaid,$function_area){
		    	$query="UPDATE function_area SET function_area =:function_area WHERE id=:id";
		        return self::updateQuery($query,array('id'=>$function_areaid,'function_area'=>$function_area));	
	} 	

	public static function addFunctionalAreas($function_area){
		    	$query="INSERT INTO function_area (function_area) VALUES (:function_area)";
		        return self::insertQuery($query,array('function_area'=>$function_area));	
	} 	

   public static function deleteFunctionalArea($function_areaid){
	    	$query="DELETE FROM function_area WHERE id=:function_area";
	        return self::deleteQuery($query,array('function_area'=>$function_areaid));
    }	


    public static function getAllIndustries($pageoffset){
	    	$query="SELECT * FROM industrydetails ".$pageoffset;
            $result=self::fetchQuery($query,array());
            $result['has_more']=count($result); 
            return $result;
    }	

    public static function getIndustryById($industryId){
	    	$query="SELECT * FROM industrydetails WHERE id=:id";
	        return self::fetchQuery($query,array('id'=>$industryId));
    }

	public static function updateIndustry($industryId,$industry){
		    	$query="UPDATE industrydetails SET industry =:industry WHERE id=:id";
		        return self::updateQuery($query,array('id'=>$industryId,'industry'=>$industry));	
	} 	

	public static function addIndustry($industry){
		    	$query="INSERT INTO industrydetails (industry) VALUES (:industry)";
		        return self::insertQuery($query,array('industry'=>$industry));	
	} 	

   public static function deleteIndustry($industryId){
	    	$query="DELETE FROM industrydetails WHERE id=:id";
	        return self::deleteQuery($query,array('id'=>$industryId));
    }

    public static function getAllCities($pageoffset){
    		self::set_char_UTF();
	    	$query="SELECT *,ci.id as city_id FROM city ci JOIN country co ON co.id=ci.country_id ".$pageoffset;
            $result=self::fetchQuery($query,array());
            $result['has_more']=count($result); 
	        return $result;
    }	

    public static function getCityById($cityId){
	    	$query="SELECT * FROM city ci JOIN country co ON co.id=ci.country_id WHERE ci.id=:id";
	        return self::fetchQuery($query,array('id'=>$cityId));
    }

   	public static function updateCity($cityid,$city,$countryId){
		    	$query="UPDATE city SET city =:city ,country_id =:country_id WHERE id=:id";
		        return self::updateQuery($query,array('id'=>$cityid,'city'=>$city,'country_id'=>$countryId));	
	} 

	public static function addCity($city,$countryId){
		$query="INSERT INTO city (city,country_id) VALUES (:city,:country_id)";
		return self::insertQuery($query,array('city'=>$city,'country_id'=>$countryId));	
	} 	

   public static function deleteCity($industryId){
	    	$query="DELETE FROM city WHERE id=:id";
	        return self::deleteQuery($query,array('id'=>$industryId));
    }	 
    public static function SearchAllJobsForAdmin($searchParam){
            $query="SELECT jpd.id,jpd.jobname,jpd.experience,jpd.skills,id.industry,fa.function_area,cd.company_name FROM job_post_details jpd LEFT JOIN company_details cd ON cd.id = jpd.company_id LEFT JOIN industrydetails id ON id.id=jpd.industry LEFT JOIN function_area fa ON fa.id=jpd.functional_area WHERE jpd.jobname LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    }

    public static function SearchAllUsersForAdmin($searchParam){
            $query="SELECT ur.id as main_id,ur.username,ur.email,um.user_category,jsd.first_name,jsd.last_name,jsd.contact_number,ur.is_active FROM user_registration ur JOIN user_map um ON um.id=ur.user_map_id LEFT JOIN job_seeker_details jsd ON jsd.user_id =ur.id WHERE ur.username LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    }

    public static function SearchCurrentTitle($searchParam){
            $query="SELECT id,current_designation FROM job_seeker_details WHERE current_designation LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    }
   public static function getAllDumpUsersForAdmin($pageoffset){
            $query="SELECT ur.id as main_id,ur.username,ur.email,um.user_category,jsd.first_name,jsd.contact_number,ur.is_active,jsd.city,jsd.DOB,jsd.current_designation,jsd.skills FROM user_registration ur JOIN user_map um ON um.id=ur.user_map_id JOIN job_seeker_details_dump jsd ON jsd.user_id =ur.id".$pageoffset;
            $result=self::fetchQuery($query,array());
            $result['has_more']=count($result);  
            return  $result;
    }
    public static function SearchAllCompaniesForAdmin($searchParam){
            $query="SELECT cd.id,cd.company_name,cd.number_of_employees,cd.address,ur.username as added_by,id.industry FROM company_details cd JOIN user_registration ur on ur.id = cd.added_by LEFT JOIN industrydetails id ON id.id=cd.industry WHERE cd.company_name LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    }


    public static function SearchAllCitiessForAdmin($searchParam){
            $query="SELECT *,ci.id as city_id FROM city ci JOIN country co ON co.id=ci.country_id  WHERE ci.city LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    }  

    public static function SearchAllLangForAdmin($searchParam){
            $query="SELECT * FROM languages WHERE language LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    }  
    public static function SearchAllSkillForAdmin($searchParam){
            $query="SELECT * FROM skill_set WHERE skill_set LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    } 

    public static function SearchAllCountryForAdmin($searchParam){
            $query="SELECT * FROM country WHERE country LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    }  
    

    public static function SearchAllFunctionalAreasForAdmin($searchParam){
            $query="SELECT * FROM function_area WHERE function_area LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    }   

    
    public static function SearchAllIndustriesForAdmin($searchParam){
            $query="SELECT * FROM industrydetails WHERE industry LIKE '".$searchParam."%'";
            return self::fetchQuery($query,array());
    }      
    //
   public static function getCompanycontactId($seekerId,$company_id){
            $query="SELECT * FROM company_contact_map WHERE company_id =:company_id AND job_seeker_user_id=:job_seeker_user_id";
            return self::fetchQuery($query,array('company_id'=>$company_id,'job_seeker_user_id'=>$seekerId));
    }
    public static function getMappedCompanyByUserId($userid){
            $query="SELECT * FROM company_user_map WHERE user_id =:user_id";
            return self::fetchQuery($query,array('user_id'=>$userid));
    }

      public static function getMappedCompanyAllinfo($seekerId,$userid,$company_id){
            $query="SELECT cci.company_contact_map_id,cci.message,DATE_FORMAT(cci.insert_datetime,'%b %d %Y') as messaged_on,usr.username,usr.id as user_id FROM company_contact_info cci JOIN company_contact_map ccm ON ccm.id=cci.company_contact_map_id JOIN company_user_map cm ON cm.company_id =ccm.company_id JOIN user_registration usr ON usr.id=cci.text_by WHERE ccm.job_seeker_user_id =:seekerId AND cm.user_id=:user_id AND ccm.company_id=:company_id";
            return self::fetchQuery($query,array('seekerId'=>$seekerId,'user_id'=>$userid,'company_id'=>$company_id));
    }

    public static function addCompanyContactMap($company_id,$job_seeker_user_id){
        $query="INSERT INTO company_contact_map (company_id,job_seeker_user_id) VALUES (:company_id,:job_seeker_user_id)";
        return self::insertQuery($query,array('company_id'=>$company_id,'job_seeker_user_id'=>$job_seeker_user_id)); 
    } 

    public static function addCompanyContactMessageInfo($company_contact_map_id,$message,$text_by){
        $query="INSERT INTO company_contact_info (company_contact_map_id,message,text_by) VALUES (:company_contact_map_id,:message,:text_by)";
        return self::insertQuery($query,array('company_contact_map_id'=>$company_contact_map_id,'message'=>$message,'text_by'=>$text_by)); 
    } 

       public static function getAllInboxMessages($userId,$pageoffset){
            $query="SELECT cd.company_name,cci.company_contact_map_id,cci.message,DATE_FORMAT(cci.insert_datetime,'%b %d %Y') as message_on FROM company_contact_map ccm LEFT JOIN company_contact_info cci ON cci.company_contact_map_id=ccm.id JOIN company_details cd ON cd.id=ccm.company_id WHERE job_seeker_user_id =:user_id GROUP BY cci.company_contact_map_id ORDER BY cci.insert_datetime ASC ".$pageoffset;
            $result=self::fetchQuery($query,array('user_id'=>$userId));
            $result['has_more']=count($result);
            return $result;
    }

       public static function getAllconversationforJobseeker($contactmapid,$userId){
            $query="SELECT cci.message,DATE_FORMAT(cci.insert_datetime,'%b %d %Y') as messaged_on,usr.username,usr.id as user_id FROM company_contact_map ccm LEFT JOIN company_contact_info cci ON cci.company_contact_map_id=ccm.id JOIN user_registration usr ON usr.id=cci.text_by WHERE job_seeker_user_id =:user_id AND cci.company_contact_map_id=:contact_map_id";
            return self::fetchQuery($query,array('contact_map_id'=>$contactmapid,'user_id'=>$userId));
    }

    public static function getAllInboxMessagesForRecruiter($userId,$pageoffset){
            $query="SELECT cd.company_name,cci.company_contact_map_id,cci.message,DATE_FORMAT(cci.insert_datetime,'%b %d %Y') as message_on,ccm.job_seeker_user_id  FROM company_user_map cm LEFT JOIN company_contact_map ccm ON ccm.company_id=cm.company_id LEFT JOIN company_contact_info cci ON cci.company_contact_map_id=ccm.id JOIN company_details cd ON cd.id=ccm.company_id WHERE cm.user_id=:user_id GROUP BY cci.company_contact_map_id ORDER BY cci.insert_datetime ASC ".$pageoffset;
            $result=self::fetchQuery($query,array('user_id'=>$userId));
            $result['has_more']=count($result);
            return $result;
    }

    public static function getAllUsersAndAssignedCompanies($pageoffset){
            $query="SELECT cu.id as map_id,ur.username,cd.company_name,cd.company_logo FROM company_user_map cu JOIN user_registration ur ON ur.id=cu.user_id JOIN company_details cd ON cd.id=cu.company_id ".$pageoffset;
            $result=self::fetchQuery($query,array());
            $result['has_more']=count($result); 
            return $result;
    }

    public static function getAllAssignedCompaniesForUser($userId,$pageoffset){
            $query="SELECT cu.id as map_id,ur.username,cd.company_name,cd.company_logo FROM company_user_map cu JOIN user_registration ur ON ur.id=cu.user_id JOIN company_details cd ON cd.id=cu.company_id WHERE cu.user_id=:user_id ".$pageoffset;
            $result=self::fetchQuery($query,array('user_id'=>$userId));
            $result['has_more']=count($result); 
            return $result;
    }
    public static function UnAssignCompanies($mapId){
            $query="DELETE FROM company_user_map WHERE id=:map_id";
            return self::fetchQuery($query,array('map_id'=>$mapId));
    }  

    public static function updateProfileImage($userId,$profile_pic_path){
            $query="UPDATE user_registration SET profile_pic=:profile_pic WHERE id=:user_id";
            self::updateQuery($query,array('profile_pic'=>$profile_pic_path,'user_id'=>$userId));
            return $profile_pic_path;
    }  
    public static function getProfileImage($userId){
            $query="SELECT profile_pic FROM user_registration WHERE id=:user_id";
            $result=self::fetchQuery($query,array('user_id'=>$userId));
            $profile_pic="/assets/images/50X50.png";
            if(isset($result[0]['profile_pic']) && !empty($result[0]['profile_pic'])){
                $profile_pic=$result[0]['profile_pic'];
            }
            return $profile_pic;
    }  
    public static function searchAllJobs($skill, $where){            
            if(!isset($where) || empty($where)){
                $where="";
            }
            else{
                $where='%'.$where.'%';
            }
            if(!isset($skill) || empty($skill)){
                $skill="";
            }
            else{
                $skill='%'.$skill.'%';
            }
            $query="SELECT jpd.id as id,jpd.jobname,jpd.skills,jpd.industry,jpd.insert_datetime,cd.company_name,jpd.skills,jpd.address,ind.industry,fa.function_area,ss.skill_set,jpd.experience,SUBSTRING(jpd.job_description,1,100) as description FROM job_post_details jpd LEFT JOIN company_details cd ON cd.id=jpd.company_id LEFT JOIN function_area fa ON fa.id=jpd.functional_area LEFT JOIN industrydetails ind ON ind.id=jpd.industry LEFT JOIN skill_set ss ON ss.id=jpd.skills WHERE jpd.address LIKE '".$where."' OR jpd.skills LIKE '".$skill."' GROUP BY jpd.id";
            $result=self::fetchQuery($query,array());
            return $result;
    }        
}

?>
