<?php

	function check_form_validation($formArray){
		$is_validated=true;
		if(!count($formArray)){
			$is_validated=false;
		}
		foreach ($formArray as $key => $value) {
			if(!isset($formArray[$key]) || empty($formArray[$key])){
				$is_validated=false;
				break;			
			}
		}
		return $is_validated;
	}
        
    function apache_request_headers() {
            foreach($_SERVER as $key=>$value) {
                if (substr($key,0,5)=="HTTP_") {
                    $key=str_replace(" ","-",ucwords(strtolower(str_replace("_"," ",substr($key,5)))));
                    $out[$key]=$value;
                }else{
                    $out[$key]=$value;
        }
            }
            return $out;
        }
	function add_key_to_array($inputArr){
		$allKeyArr=array('first_name'=>0,'last_name'=>0,'gender'=>0,'DOB'=>0,'contact_number'=>0,'address_line_1'=>0,'address_line_2'=>0,'state'=>0,'pincode'=>0,'city'=>0,'country'=>0,'languages_known'=>1,'current_designation'=>0,'current_company'=>0,'current_CTC'=>0,'expected_CTC'=>0,'duration_notice_period'=>0,'skills'=>1,'current_location'=>0,'current_industry'=>0,'current_functional_area'=>0,'specialization'=>0,'university_or_college'=>0,'desired_employment_type'=>0,'desired_job_type'=>0,'prefered_location'=>0,'website_link'=>0,'highest_education'=>0,'experience'=>0,'industry'=>0,'functional_area'=>0,'last_login'=>0);
		foreach ($allKeyArr as $key => $value) {
			if (!array_key_exists($key,$inputArr)){
				if($allKeyArr[$key]){
					$inputArr[$key]=array();
				}	
				else{
					$inputArr[$key]=0;
				} 
			  }
		}
		return $inputArr;
	}
      
