<?php 

require 'vendor/autoload.php';
require_once("includes.php");
$app = new \Slim\App();
use \Firebase\JWT\JWT;
require_once("searchapi.php");
$app->post('/register', function ($request, $response, $args) {
	$registerParams=$request->getParsedBody();
	if(check_form_validation($registerParams)){
		$jwt=$request->getHeaderLine('Authorization');
		$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
		$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
		if($tokenDetails->data->userType==1){	
			$usernameExists=UsersDAO::checkUserNameAlreadyExists($registerParams['name']);
			$emailExists=UsersDAO::checkEmailAlreadyExists($registerParams['email']);
			if($usernameExists){
				return $response->withStatus(400)->write('Username already exists!!!');
			}
			elseif ($emailExists) {
				return $response->withStatus(400)->write('Email already exists!!!');
			}
			else{
				$password=md5(Configuration::$salt . $registerParams['password']);
				$registerUserType=$registerParams['user_map'];
				$userid=UsersDAO::registerUser($registerParams['name'],$password,$registerParams['email'],$registerUserType);
				if($registerUserType==2){
					UsersDAO::insertJobSeekerId($userid);
				}
			}
		}
		else{
			return $response->withStatus(400)->write('Bad Request');
		}
	}
	else{
		return $response->withStatus(400)->write('Bad Request');
	}
	
});

$app->post('/registerrecruiter', function ($request, $response, $args) {
		$registerParams=$request->getParsedBody();
		if(check_form_validation($registerParams)){
			$password=md5(Configuration::$salt . $registerParams['password']);
			$registerUserType=$registerParams['user_map'];
			$usernameExists=UsersDAO::checkUserNameAlreadyExists($registerParams['name']);
			$emailExists=UsersDAO::checkEmailAlreadyExists($registerParams['email']);
			if($usernameExists){
				return $response->withStatus(400)->write('Username already exists!!!');
			}
			elseif ($emailExists) {
				return $response->withStatus(400)->write('Email already exists!!!');
			}
			else{
				$userid=UsersDAO::registerUser($registerParams['name'],$password,$registerParams['email'],$registerUserType);
			}
		}
		else{
			return $response->withStatus(400)->write('Bad Request');
		}
});
$app->post('/login', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();

	if(check_form_validation($requestObj)){
		$username=$requestObj['username'];
		$password=$requestObj['password'];
		$isLogin=UsersDAO::validUser($username,$password);
		if($isLogin){
				$tokenId    = base64_encode(openssl_random_pseudo_bytes(true));//base64_encode(mcrypt_create_iv(23));
			    $issuedAt   = time();
			    $notBefore  = $issuedAt + 10;             //Adding 10 seconds
			    $expire     = $notBefore + 600000;            // Adding 600000 seconds
			    $serverName = Configuration::$hostName; // Retrieve the server name from config file
			    
			    /*
			     * Create the token as an array
			     */
			    $data = [
			        'iat'  => $issuedAt,         // Issued at: time when the token was generated
			        'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
			        'iss'  => $serverName,       // Issuer
			        'nbf'  => $notBefore,        // Not before
			        'exp'  => $expire,           // Expire
			        'data' => [                  // Data related to the signer user
			            'userId'   => $isLogin['id'], // userid from the users table
			            'userName' => $username,
			            'userType' => $isLogin['usertype']
			        ]
			    ];
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			    
			    /*
			     * Encode the array to a JWT string.
			     * Second parameter is the key to encode the token.
			     * 
			     * The output string can be validated at http://jwt.io/
			     */
			    $jwt = JWT::encode(
			        $data,      //Data to be encoded in the JWT
			        $secretKey, // The signing key
			        'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
			        );
			    $_SESSION['authkey']=$jwt;
			    $_SESSION['profile_pic']=$isLogin['profile_pic'];
			    $page="/home.php";
			    $unencodedArray = ['auth_token' => $jwt,'page'=>$page];
			    $isLogin=UsersDAO::insertLoginTime($isLogin['id']);
			    echo json_encode($unencodedArray);
		}
		else{
			return $response->withStatus(401)->write('Unauthorized');
		}
	}
	else{
		return $response->withStatus(401)->write('Unauthorized');
	}
});

$app->post('/searchcandidate',function($request, $response, $args){
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==3 || $tokenDetails->data->userType==1){	
				echo json_encode(UsersDAO::getCandidatesSearch($requestObj));
			}
			else{
				return $response->withStatus(401)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->post('/searchjob',function($request, $response, $args){
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==2 || $tokenDetails->data->userType==1){	
				echo json_encode(UsersDAO::getJobSearch($requestObj));
			}
			else{
				return $response->withStatus(401)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
$app->post('/candidateapplyJob',function($request, $response, $args){
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==2){	
				$userId=$tokenDetails->data->userId;
				echo json_encode(UsersDAO::candidateApplyJob($userId,$requestObj['id']));
			}
			else{
				return $response->withStatus(401)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
$app->any('/skillset/{id}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
				if($request->isGet()){
						$skillId=$args['id'];
						echo json_encode(UsersDAO::getSkill($skillId));
				}
				if($request->isDelete()){
						$skillId=$args['id'];
						UsersDAO::deleteSkill($skillId);
						echo $skillId;
				}
				$skillId=$args['id'];
				if($request->isPost()){
					$skillName=$requestObj['name'];
					UsersDAO::addSkill($skillName);
				}
				if($request->isPut()){
					$skillName=$requestObj['name'];
					UsersDAO::updateSkill($skillId,$skillName);
				}	
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
$app->get('/getAllSkill/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
						$currentPageoffset=$args['page'];
					    $limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					    $pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
						echo json_encode(UsersDAO::getAllSkill($pageoffset));
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
$app->get('/getconversationistory/{seekerid}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			$jobseekerid=$args['seekerid'];
			$companyIds=UsersDAO::getMappedCompanyByUserId($userId);
			$comapanyId=0;
			if(count($companyIds)){
							$comapanyId=$companyIds[0]['company_id'];
			}

			$results=UsersDAO::getMappedCompanyAllinfo($jobseekerid,$userId,$comapanyId);
			echo json_encode($results);
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
	}
});

$app->get('/getconversationistoryjobseeker/{contactmapid}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			$contactmapid=$args['contactmapid'];
			$results=UsersDAO::getAllconversationforJobseeker($contactmapid,$userId);
			echo json_encode($results);
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
	}
});

$app->get('/getinboxdetails/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			$currentPageoffset=$args['page'];
			$limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
			$pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
			if($tokenDetails->data->userType==2){
					$results=UsersDAO::getAllInboxMessages($userId,$pageoffset);
			}
			else{
				$results=UsersDAO::getAllInboxMessagesForRecruiter($userId,$pageoffset);
			}
			echo json_encode($results);
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
	}
});

$app->post('/addconversation', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			$contactmapid=$requestObj['contactmapid'];
			$jobseekerid=$requestObj['seekerid'];
			$message=trim($requestObj['message']);
			$companyIds=UsersDAO::getMappedCompanyByUserId($userId);
			$comapanyId=0;
			$userName=$tokenDetails->data->userName;
			if(count($companyIds)){
							$comapanyId=$companyIds[0]['company_id'];
			}
			if($contactmapid){
				UsersDAO::addCompanyContactMessageInfo($contactmapid,$message,$userId);
				echo json_encode(array('id'=>$contactmapid,'message'=>$message,'username'=>$userName));
			}
			else{
				if($comapanyId){
						$contactmapid=UsersDAO::addCompanyContactMap($comapanyId,$jobseekerid);
						UsersDAO::addCompanyContactMessageInfo($contactmapid,$message,$userId);
						echo json_encode(array('id'=>$contactmapid,'message'=>$message,'username'=>$userName));
				}
				else{
					echo false;
				}
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
	}
});


$app->post('/addconversationjobseeker', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			$userName=$tokenDetails->data->userName;
			$contactmapid=$requestObj['contactmapid'];
			$message=trim($requestObj['message']);
			if($contactmapid){
				UsersDAO::addCompanyContactMessageInfo($contactmapid,$message,$userId);
				echo json_encode(array('id'=>$contactmapid,'message'=>$message,'username'=>$userName));
			}
			else{
					echo false;
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
	}
});


$app->post('/jobseekerprofile', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				// $jwt=$request->getHeaderLine('Authorization');
				// $secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				// $tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				// $userId=$tokenDetails->data->userId;
				$usernameExists=UsersDAO::checkUserNameAlreadyExists($requestObj['name']);
				$emailExists=UsersDAO::checkEmailAlreadyExists($requestObj['email']);
				$contactExists=UsersDAO::checkContactAlreadyExists($requestObj['contact_number']);
				if($usernameExists){
					return $response->withStatus(400)->write('error-username');
				}
				elseif ($emailExists) {
					return $response->withStatus(400)->write('error-email');
				}
				elseif($contactExists){
					return $response->withStatus(400)->write('error-contactnumber');
				}
				else{
					$target_file="";
					if($_FILES['resume']['size']){
						$files = $request->getUploadedFiles();
						$target_dir = "static/resumes/";
						$target_file = $target_dir .time().basename($files['resume']->getClientFilename());
						$files['resume']->moveTo($target_file);
					}
					$password=md5(Configuration::$salt . $requestObj['password']);
					$userId=UsersDAO::registerUser($requestObj['name'],$password,$requestObj['email'],2);
					UsersDAO::insertJobSeekerProfile($userId,$requestObj,$target_file);
				}
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});

$app->post('/jobseekerprofileOnHome', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				// $jwt=$request->getHeaderLine('Authorization');
				// $secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				// $tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				// $userId=$tokenDetails->data->userId;
				$usernameExists=UsersDAO::checkUserNameAlreadyExists($requestObj['name']);
				$emailExists=UsersDAO::checkEmailAlreadyExists($requestObj['email']);
				if($usernameExists){
					return $response->withStatus(400)->write('Username already exists');
				}
				elseif ($emailExists) {
					return $response->withStatus(400)->write('Email already exists');
				}
				else{
					$target_file="";
					if($_FILES['resume']['size']){
						$files = $request->getUploadedFiles();
						$target_dir = "static/resumes/";
						$target_file = $target_dir .time().basename($files['resume']->getClientFilename());
						$files['resume']->moveTo($target_file);
					}
					$password=md5(Configuration::$salt . $requestObj['password']);
					$userId=UsersDAO::registerUser($requestObj['name'],$password,$requestObj['email'],2);
					UsersDAO::insertJobSeekerProfile($userId,$requestObj,$target_file);
				}
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});

$app->post('/changeprofilepic', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
					$jwt=$request->getHeaderLine('Authorization');
					$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
					JWT::$leeway = 10; 
					$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
					$userId=$tokenDetails->data->userId;
					$target_profile_file="";
					if($_FILES['profile_pic']['size']){
							$files = $request->getUploadedFiles();
							$target_profile_file = "static/profile_pics/";
							$target_profile_file = $target_profile_file .time().basename($files['profile_pic']->getClientFilename());
							$files['profile_pic']->moveTo($target_profile_file);
						}
					else{
							$target_profile_file=$requestObj['uploaded-img-hidden'];
					}
					$_SESSION['profile_pic']=$target_profile_file;
					return UsersDAO::updateProfileImage($userId,$target_profile_file);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});
$app->get('/getprofilepic', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
					$jwt=$request->getHeaderLine('Authorization');
					$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
					JWT::$leeway = 10; 
					$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
					$userId=$tokenDetails->data->userId;
					return UsersDAO::getProfileImage($userId);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});
$app->post('/updatejobseekerprofile', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				$usernameExists=UsersDAO::checkUserNameAlreadyExistsForUserId($requestObj['name'],$userId);
				$emailExists=UsersDAO::checkEmailAlreadyExistsForUserId($requestObj['email'],$userId);
				$contactExists=UsersDAO::checkContactAlreadyExistsForUserId($requestObj['contact_number'],$userId);
				if($usernameExists){
					return $response->withStatus(400)->write('error-username');
				}
				elseif ($emailExists) {
					return $response->withStatus(400)->write('error-email');
				}
				elseif($contactExists){
					return $response->withStatus(400)->write('error-contactnumber');
				}
				else{
					$target_file="";
					$target_profile_file="";
					if($_FILES['resume']['size']){
						$files = $request->getUploadedFiles();
						$target_dir = "static/resumes/";
						$target_file = $target_dir .time().basename($files['resume']->getClientFilename());
						$files['resume']->moveTo($target_file);
					}
					if($_FILES['profile_pic']['size']){
							$files = $request->getUploadedFiles();
							$target_profile_file = "static/profile_pics/";
							$target_profile_file = $target_profile_file .time().basename($files['profile_pic']->getClientFilename());
							$files['profile_pic']->moveTo($target_profile_file);
						}
					else{
							$target_profile_file=$requestObj['uploaded-img-hidden'];
					}
					$_SESSION['profile_pic']=$target_profile_file;
					UsersDAO::updateRegisteredUserEmail($userId,$requestObj['email'],$target_profile_file);
					UsersDAO::updateJobSeekerProfile($userId,$requestObj,$target_file);
				}
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});



$app->get('/getprofiledetailsbyid', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				if($request->isGet()){
					echo json_encode(UsersDAO::getUserDetailsByid($userId));
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});


$app->any('/postjob', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				if($request->isPost()){
					echo UsersDAO::postJobDetails($userId,$requestObj);
				}
				if($request->isPut()){
					UsersDAO::updateJobDetails($userId,$requestObj);
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});
$app->any('/addCompany/{companyid}', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				if($request->isGet()){
					$currentPageoffset=$args['companyid'];
					$limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					$pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
					echo json_encode(UsersDAO::listCompany($userId,$pageoffset));
				}
				if($request->isPost()){
					$target_profile_file="";
					if($_FILES['profile_pic']['size']){
						$files = $request->getUploadedFiles();
						$target_profile_file = "static/company_logo/";
						$target_profile_file = $target_profile_file .time().basename($files['profile_pic']->getClientFilename());
						$files['profile_pic']->moveTo($target_profile_file);
					}
					else{
						$target_profile_file="/assets/images/50X50.png";
					}
					$company_id=UsersDAO::addCompany($userId,$requestObj,$target_profile_file);
					UsersDAO::assignCompany($userId,$company_id);
				}
				if($request->isPut()){
					$target_profile_file="";
					if($_FILES['profile_pic']['size']){
						$files = $request->getUploadedFiles();
						$target_profile_file = "static/company_logo/";
						$target_profile_file = $target_profile_file .time().basename($files['profile_pic']->getClientFilename());
						$files['profile_pic']->moveTo($target_profile_file);
					}
					else{
						$target_profile_file=$requestObj['uploaded-img-hidden'];
					}
					$companyid=$args['companyid'];
					UsersDAO::updateCompany($userId,$companyid,$requestObj,$target_profile_file);
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});
$app->any('/assigncompany', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				$userType=$tokenDetails->data->userType;
				if($request->isPost()){
					if($userType==1){
						UsersDAO::assignCompany($requestObj['user'],$requestObj['company']);
					}
					else{
						UsersDAO::assignCompany($userId,$requestObj['company']);
					}
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});
$app->get('/getCompany/{companyid}', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				if($request->isGet()){
					$companyid=$args['companyid'];
					echo json_encode(UsersDAO::getCompanyDetails($userId,$companyid));
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});

$app->get('/getJobAppliedDetailsByUser', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				if($request->isGet()){
					echo json_encode(UsersDAO::getJobAppliedDetailsByUser($userId));
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});

$app->get('/getjob/{jobid}', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				if($request->isGet()){
					$jobid=$args['jobid'];
					echo json_encode(UsersDAO::getJobDetails($userId,$jobid));
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});
$app->get('/searchCompanybyuserid/{param}', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				$quertString=" cd.added_by=".$userId." AND ";
				if($request->isGet()){
					$searchParam=$args['param'];
					if($tokenDetails->data->userType==1){	
						$quertString="";
					}
					echo json_encode(UsersDAO::searchCompanybyuserid($searchParam,$quertString));
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});
$app->get('/searchAllCompany/{param}', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				$quertString=" cd.added_by=".$userId." AND ";
				if($request->isGet()){
					$searchParam=$args['param'];
					$quertString="";
					echo json_encode(UsersDAO::searchCompanybyuserid($searchParam,$quertString));
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});
$app->get('/listjobsbyuserid/{page}', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				if($request->isGet()){
					$currentPageoffset=$args['page'];
					$limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					$pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
					echo json_encode(UsersDAO::listJobsByUserId($userId,$pageoffset));
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});
$app->any('/getcityList/{param}', function ($request, $response, $args) {
		if($request->isGet()){
			$searchParam=$args['param'];
			$results=UsersDAO::searchCityList($searchParam);
			echo json_encode($results);
		}
});

$app->any('/getcountryList/{param}', function ($request, $response, $args) {
		if($request->isGet()){
			$searchParam=$args['param'];
			$results=UsersDAO::searchCountryList($searchParam);
			echo json_encode($results);
		}
});

$app->any('/getlanguageList/{param}', function ($request, $response, $args) {
		if($request->isGet()){
			$searchParam=$args['param'];
			$results=UsersDAO::searchLanguageList($searchParam);
			echo json_encode($results);
		}
});

$app->any('/getsklillList/{param}', function ($request, $response, $args) {
		if($request->isGet()){
			$searchParam=$args['param'];
			$results=UsersDAO::searchSkillList($searchParam);
			echo json_encode($results);
		}
});
$app->get('/skillsetbycommaseperated/{skills}', function ($request, $response, $args) {
		if($request->isGet()){
			$skills=$args['skills'];
			$results=UsersDAO::getSkillByCommaseperated($skills);
			echo json_encode($results);
		}
});

$app->get('/languagesknownbycommaseperated/{languages}', function ($request, $response, $args) {
		if($request->isGet()){
			$languages=$args['languages'];
			$results=UsersDAO::getlanguagecommaseperated($languages);
			echo json_encode($results);
		}
});

$app->any('/getindustryList/{param}', function ($request, $response, $args) {
		if($request->isGet()){
			$searchParam=$args['param'];
			$results=UsersDAO::searchIndustryList($searchParam);
			echo json_encode($results);
		}
});

$app->any('/getfunctionList/{param}', function ($request, $response, $args) {
		if($request->isGet()){
			$searchParam=$args['param'];
			$results=UsersDAO::searchFunctionList($searchParam);
			echo json_encode($results);
		}
});


//admin

	$app->get('/companies/{page}', function ($request, $response, $args) {
		 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					$currentPageoffset=$args['page'];
					$limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					$pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
					echo json_encode(UsersDAO::listcompanies($pageoffset));
			}	
			else{
					return $response->withStatus(401)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
			
		});

		$app->delete('/companies/{companyid}', function ($request, $response, $args) {
		 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			if($tokenDetails->data->userType==1 || $tokenDetails->data->userType==3){	
					$companyid=$args['companyid'];	
					if($tokenDetails->data->userType==1 ){
						UsersDAO::deleteCompanies($companyid);
						UsersDAO::deleteCompanyUsermapAll($companyid);
					}
					UsersDAO::deleteCompanyUsermap($userId,$companyid);
					echo $companyid;
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
			
		});
		$app->get('/getCompanyforid/{companyid}', function ($request, $response, $args) {
		 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1 || $tokenDetails->data->userType==3){		
					$companyid=$args['companyid'];
					echo json_encode(UsersDAO::getCompanyDetailsByIdAdmin($companyid));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
			
		});

		$app->post('/companies/{companyid}', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
		 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1 || $tokenDetails->data->userType==3){		
					$companyid=$args['companyid'];
					$target_profile_file="";
					if($_FILES['profile_pic']['size']){
						$files = $request->getUploadedFiles();
						$target_profile_file = "static/company_logo/";
						$target_profile_file = $target_profile_file .time().basename($files['profile_pic']->getClientFilename());
						$files['profile_pic']->moveTo($target_profile_file);
					}
					else{
						$target_profile_file=$requestObj['uploaded-img-hidden'];
					}
					echo json_encode(UsersDAO::updateCompaniesByAdmin($companyid,$requestObj,$target_profile_file));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
			
		});



$app->get('/jobs/{page}', function ($request, $response, $args) {
		 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
				    $currentPageoffset=$args['page'];
					$limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					$pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
					echo json_encode(UsersDAO::listJobs($pageoffset));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
			
		});

		$app->delete('/jobs/{jobsid}', function ($request, $response, $args) {
		 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1 || $tokenDetails->data->userType==3){	
					$jobsid=$args['jobsid'];	
					UsersDAO::deleteJobs($jobsid);
					echo $jobsid;
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
			
		});
		$app->get('/getjobsforid/{jobsid}', function ($request, $response, $args) {
		 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					$jobsid=$args['jobsid'];
					echo json_encode(UsersDAO::getJobsDetailsByIdAdmin($jobsid));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
			
		});

		$app->put('/jobs', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
		 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::updateJobsByAdmin($requestObj));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
			
		});
$app->get('/searchCompanybyAdminid/{param}', function ($request, $response, $args) {
			$requestObj=$request->getParsedBody();
			try {
				$jwt=$request->getHeaderLine('Authorization');
				$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
				$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
				$userId=$tokenDetails->data->userId;
				if($request->isGet()){
					$searchParam=$args['param'];
					if($tokenDetails->data->userType==1){	
						echo json_encode(UsersDAO::searchCompanybyAdminid($searchParam));
					}
					
				}	
				//echo json_encode($requestObj);
			} catch (Exception $e) {
			                /*
			                 * the token was not able to be decoded.
			                 * this is likely because the signature was not able to be verified (tampered token)
			                 */
			                header('HTTP/1.0 401 Unauthorized');
			}
});


$app->any('/countryopr/{id}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
				if($request->isGet()){
						$countryid=$args['id'];
						echo json_encode(UsersDAO::getCountryById($countryid));
				}
				if($request->isDelete()){
						$countryid=$args['id'];
						UsersDAO::deleteCountry($countryid);
						echo $countryid;
				}
				$countryid=$args['id'];
				if($request->isPost()){
					$country=$requestObj['name'];
					UsersDAO::addCountry($country);
				}
				if($request->isPut()){
					$country=$requestObj['name'];
					UsersDAO::updateCountry($countryid,$country);
				}	
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
$app->get('/getallcountries/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
						$currentPageoffset=$args['page'];
					    $limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					    $pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
						echo json_encode(UsersDAO::getAllCountry($pageoffset));
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});


$app->any('/languageopr/{id}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
				if($request->isGet()){
						$languageid=$args['id'];
						echo json_encode(UsersDAO::getLanguageById($languageid));
				}
				if($request->isDelete()){
						$languageid=$args['id'];
						UsersDAO::deleteLanguage($languageid);
						echo $languageid;
				}
				$languageid=$args['id'];
				if($request->isPost()){
					$language=$requestObj['name'];
					UsersDAO::addLanguage($language);
				}
				if($request->isPut()){
					$language=$requestObj['name'];
					UsersDAO::updateLanguage($languageid,$language);
				}	
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});


$app->get('/getalllanguages/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){	
						$currentPageoffset=$args['page'];
					    $limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					    $pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
						echo json_encode(UsersDAO::getAlllanguages($pageoffset));
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});


$app->any('/functionalareas/{id}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
				if($request->isGet()){
						$functionalareasid=$args['id'];
						echo json_encode(UsersDAO::getFunctionalAreasById($functionalareasid));
				}
				if($request->isDelete()){
						$functionalareasid=$args['id'];
						UsersDAO::deleteFunctionalArea($functionalareasid);
						echo $functionalareasid;
				}
				$functionalareasid=$args['id'];
				if($request->isPost()){
					$functionalareas=$requestObj['name'];
					UsersDAO::addFunctionalAreas($functionalareas);
				}
				if($request->isPut()){
					$functionalareas=$requestObj['name'];
					UsersDAO::updateFunctionalAreas($functionalareasid,$functionalareas);
				}	
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});


$app->get('/getfunctionalreas/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){	
						$currentPageoffset=$args['page'];
					    $limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					    $pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
						echo json_encode(UsersDAO::getAllFunctionalAreas($pageoffset));
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});


$app->any('/industries/{id}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
				if($request->isGet()){
						$industryid=$args['id'];
						echo json_encode(UsersDAO::getIndustryById($industryid));
				}
				if($request->isDelete()){
						$industryid=$args['id'];
						UsersDAO::deleteIndustry($industryid);
						echo $industryid;
				}
				$industryid=$args['id'];
				if($request->isPost()){
					$industry=$requestObj['name'];
					UsersDAO::addIndustry($industry);
				}
				if($request->isPut()){
					$industry=$requestObj['name'];
					UsersDAO::updateIndustry($industryid,$industry);
				}	
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});


$app->get('/getindustry/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){	
						$currentPageoffset=$args['page'];
					    $limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					    $pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
						echo json_encode(UsersDAO::getAllIndustries($pageoffset));
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->any('/cities/{id}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
				if($request->isGet()){
						$cityid=$args['id'];
						echo json_encode(UsersDAO::getCityById($cityid));
				}
				if($request->isDelete()){
						$cityid=$args['id'];
						UsersDAO::deleteCity($cityid);
						echo $cityid;
				}
				$cityid=$args['id'];
				if($request->isPost()){
					$city=$requestObj['name'];
					$countryId=$requestObj['countryid'];
					UsersDAO::addCity($city,$countryId);
				}
				if($request->isPut()){
					$city=$requestObj['name'];
					$countryId=$requestObj['countryid'];
					UsersDAO::updateCity($cityid,$city,$countryId);
				}	
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
	
		   }
});

$app->get('/getallcities/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){	
						$currentPageoffset=$args['page'];
					    $limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					    $pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
						echo json_encode(UsersDAO::getAllCities($pageoffset));
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->any('/users/{id}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
				if($request->isGet()){
						$userId=$args['id'];
						echo json_encode(UsersDAO::getUserDetailsByid($userId));
				}
				if($request->isDelete()){
						$userid=$args['id'];
						UsersDAO::deleteUser($userid);
						echo $userid;
				}
				$cityid=$args['id'];
				if($request->isPost()){
					$userId=$args['id'];
					$usernameExists=UsersDAO::checkUserNameAlreadyExistsForUserId($requestObj['name'],$userId);
					$emailExists=UsersDAO::checkEmailAlreadyExistsForUserId($requestObj['email'],$userId);
					$contactExists=UsersDAO::checkContactAlreadyExistsForUserId($requestObj['contact_number'],$userId);
					if($usernameExists){
						return $response->withStatus(400)->write('error-username');
					}
					elseif ($emailExists) {
						return $response->withStatus(400)->write('error-email');
					}
					elseif($contactExists){
						return $response->withStatus(400)->write('error-contactnumber');
					}
					else{
						$target_file="";
						$target_profile_file="";
						if($_FILES['resume']['size']){
							$files = $request->getUploadedFiles();
							$target_dir = "static/resumes/";
							$target_file = $target_dir .time().basename($files['resume']->getClientFilename());
							$files['resume']->moveTo($target_file);
						}
						if($_FILES['profile_pic']['size']){
							$files = $request->getUploadedFiles();
							$target_profile_file = "static/profile_pics/";
							$target_profile_file = $target_profile_file .time().basename($files['profile_pic']->getClientFilename());
							$files['profile_pic']->moveTo($target_profile_file);
						}
						else{
							$target_profile_file=$requestObj['uploaded-img-hidden'];
						}
						//$password=md5(Configuration::$salt . $requestObj['password']);
						//UsersDAO::updateRegisteredUserPassword($userId,$password);
						UsersDAO::updateRegisteredUserEmail($userId,$requestObj['email'],$target_profile_file);
						UsersDAO::updateJobSeekerProfile($userId,$requestObj,$target_file);
					}
				}
				if($request->isPut()){
					// $city=$requestObj['name'];
					// $countryId=$requestObj['countryid'];
					// UsersDAO::updateCity($cityid,$city,$countryId);
				}	
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
	
		   }
});

$app->get('/getallusers/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			if($tokenDetails->data->userType==1){	
					$currentPageoffset=$args['page'];
					$limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					$pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
					echo json_encode(UsersDAO::getAllUsers($userId,$pageoffset));
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
$app->get('/getalldumpusers/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			if($tokenDetails->data->userType==1){
					$currentPageoffset=$args['page'];
					$limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
					$pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
					echo json_encode(UsersDAO::getAllDumpUsersForAdmin($pageoffset));
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
$app->post('/changepassword', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			if($userId){	
					$password=md5(Configuration::$salt . $requestObj['password']);
					UsersDAO::updateRegisteredUserPassword($userId,$password);
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
//


$app->get('/assignCompanies/{page}', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			$userType=$tokenDetails->data->userType;
			$currentPageoffset=$args['page'];
			$limitPage=$currentPageoffset+Configuration::$defaultPageOffset;
			$pageoffset=" LIMIT ".$currentPageoffset.",".$limitPage;
			if($userType==1){	
				echo json_encode(UsersDAO::getAllUsersAndAssignedCompanies($pageoffset));
			}
			elseif($userType==3){	
				echo json_encode(UsersDAO::getAllAssignedCompaniesForUser($userId,$pageoffset));
			}
			else{
				return $response->withStatus(401)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->delete('/assignCompanies', function ($request, $response, $args) {
	$requestObj=$request->getParsedBody();
	 try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
				JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			$userType=$tokenDetails->data->userType;
			if($userType==1 || $userType==3){	
				$mapId=$requestObj['delete'];
				UsersDAO::UnAssignCompanies($mapId);
				echo $mapId;
			}
			else{
					return $response->withStatus(400)->write('BadRequest');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});















// test apis




$app->get('/some',function($request, $response, $args){
echo '<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>';
echo '<script>
			$(document).ready(function(){
		        $.ajax({
		            url: "/gettoken",
		            beforeSend: function(request){
		                request.setRequestHeader("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE0ODU0MzQ3MjQsImp0aSI6Ik9rclZJR2VQSmM4VkxKRXBIeVlnRzcrXC9uVjRvZU93PSIsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdCIsIm5iZiI6MTQ4NTQzNDczNCwiZXhwIjoxNDg2MDM0NzM0LCJkYXRhIjp7InVzZXJJZCI6IjQiLCJ1c2VyTmFtZSI6ImRkIn19.w9RhWsP5R8b1_mQiipReuM_gzmxjy7R0VQL_lG_UF5MmkLhvwaQx_mV1fGEmjO_Zuh8gnVn3sPiZeZEM73fDaw");
		            },
		            type: "GET",
		            success: function(data) {
		            	console.log(data);
		            	
		                // Decode and show the returned data nicely.
		            },
		            error: function() {
		                alert("error");
		            }
		        });
	    	});
		</script>';
});
$app->post('/gettoken', function ($request, $response, $args) {
	 	$jwt=$request->getHeaderLine('Authorization');
	 	 try {

				$secretKey = base64_decode('ItShouldbeSomething@!#shanghai');
				                
				                $token = JWT::decode($jwt, $secretKey, array('HS512'));
				                /*
				                 * return protected asset
				                 */
				                //header('Content-type: application/json');
				     			echo json_encode($token);

		            } catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		            }
		       
});

// End of test apis
$app->run();
?>