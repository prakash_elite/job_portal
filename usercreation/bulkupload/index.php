<?php
  session_start();
  require '../vendor/autoload.php';
  require_once("../config/config.php");
    use \Firebase\JWT\JWT;
     $user_id=false;
     $userType=false;
     $homepage="index.html";

        if(isset($_SESSION['authkey']) && !empty($_SESSION['authkey'])){
            $jwt=$_SESSION['authkey'];
            $secretKey = base64_decode(Configuration::$webTokenPrivateKey);
                JWT::$leeway = 10; 
            $tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
            if($_SERVER['PHP_SELF']=="/login.php"){
                   unset($_SESSION['authkey']);
                    session_destroy();
            }
            else{
                 $user_id=$tokenDetails->data->userId;
                 $userType=$tokenDetails->data->userType;
                 $homepage="/home.php";
            }
        }
  include_once("../includes/redirectuser.php");
  include_once("../includes/redirect_IF_notadmin.php");

?>
<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Job Portal</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320">
    <link href="/assets/css/main.css?v=1" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" type="image/png" href="/assets/images/favicon.png" />
</head>
   <div class="mj_preloaded">
        <div class="mj_preloader">
            <div class="lines">
                <div class="line line-1"></div>
                <div class="line line-2"></div>
                <div class="line line-3"></div>
            </div>

            <div class="loading-text">LOADING</div>
        </div>
    </div>
    <!--Loader End -->
        <div class="mj_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="mj_logo">
                        <a href="/home.php" >
                            <img src="/assets/images/logo.png" class="img-responsive" alt="logo" />
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mj_menu" aria-expanded="false">
                            <span class="sr-only">MENU</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="collapse navbar-collapse mj_navmenu" id="mj_menu">
                       <?php if($user_id){    ?>
                        <ul class="nav navbar-nav">
                            <?php  
                                if($userType && $userType==1){
                                    require_once("../includes/headerforadmin.php");
                                    require_once("../includes/headerforrecruiter.php");
                                    require_once("../includes/headerforjobseeker.php");                                    
                                }
                            ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right mj_right_menu mj_login_menu">
                           <li>
                                <a class="mj_profileimg"><img src="/assets/images/50X50.png" alt="user"><i class="fa fa-angle-down"></i>
                                    </a>
                            </li>
                            <div class="mj_profilediv" id="my_profile_div">
                                <ul>
                                    <li>
                                        <a href="/changepassword.php"><i class="fa fa-cog"></i>Change Password</a>
                                    </li>
                                    <li>
                                    </li>
                                    <li>
                                        <a href="/logout.php"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </ul>  
                         <?php } else{    ?>
                          <ul class="nav navbar-nav navbar-right mj_right_menu mj_withoutlogin_menu">
                                <li><a class="mj_logintoggle" href="/login.php"><i class="fa fa-user"></i> Login</a>
                             </li>
                           </ul>   
                         <?php } ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
      <h4>Bulk upload</h4>
      <div class="mj_postdiv mj_shadow_yellow mj_postpage mj_toppadder50 mj_bottompadder50">
          <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
        							<form  class="new-line-fields form-horizontal" id="upload-files" action="file.php" enctype="multipart/form-data" method="POST">
        								<fieldset>
        									<div class="form-group">
        									  <label>Select Files</label>  
        									  <input required="required" type="file" multiple name="file[]" accept=".xlsx,.xls,.doc,.docx,.pdf" class="form-control input-md" type="text">
        									</div>
        									<div class="form-group">
        									    <input id="update" type="submit" name="submit" value="submit" class="hide"/>
        									 </div>
        								</fieldset>
        							</form>
              </div>
					</div>
          <div class="mj_showmore">
                <a id="trigger-bulk-upload" class="mj_showmorebtn mj_bigbtn mj_yellowbtn">UPLOAD</a>
          </div>
			</div>
	</div>
<?php 
    include_once("../includes/foot.php");
?>
<script>
    jobportal.bulkupload={
      init:function(){
            this.initSubmitForm();
            jobportal.utilities.initTriggerElement($('#trigger-bulk-upload'),$('#update'));
      },
      initSubmitForm:function(){
        $('#upload-files').on('submit',function(e){
          e.preventDefault();
          if($('input[type=file]')[0].files.length<100){
            var formData = new FormData($('form')[0]);
            $.ajax({
                    url : 'file.php',
                    type : 'POST',
                    data : formData,
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,  // tell jQuery not to set contentType
                    success : function(data) {
                    }
            });
          }
          else{
            alert('Number of file uploaded should be less than 100');
          }
        });
      }
    }
$(document).ready(function(){
    jobportal.bulkupload.init();
});
</script>
</body>

</html>

