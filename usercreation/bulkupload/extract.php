<?php
include_once 'inc/function.find_date.php';
include_once 'inc/docx.php';
include_once 'inc/PdfParser.php';
include_once('../config/config.php');

//include_once 'inc/PdfParser/Parser.php';
include_once 'inc/class.php';
$con=mysqli_connect('localhost','root','root123');
      mysqli_select_db($con,'jp_users_db');
            $details=array();
            $records = [];
            $sql = 'select city from city';
            $res=mysqli_query($con,$sql);
            $places = [];
            while ($row = mysqli_fetch_assoc($res)) {
                $places[] = strtolower($row['city']);
            }
            $res1=mysqli_query($con,'SELECT LOWER(skill_set) as skill_set FROM skill_set');
		    $skilltomatch=array();
		    while ($row=mysqli_fetch_assoc($res1)) {
		         $skilltomatch[]=$row['skill_set'];
		    }
		    $sql2 = 'select current_designation from job_seeker_details';
		    $res2=mysqli_query($con,$sql2);
		        $current_designation = [];
		            while ($row = mysqli_fetch_assoc($res2)) {
		                $current_designation[] = strtolower($row['current_designation']);
		            }
		   $current_designation_csv = strtolower(implode(',', $current_designation));
	foreach ($resumearr as $key => $valuefile) {
		$docObj = new DocxConversion("files/".$valuefile);
		$resumeText = $docObj->convertToText();
 		$levArr=array();
 		$details=array();
    	$records = [];
    	$fileInfo = explode(PHP_EOL, trim($resumeText));
 
    	foreach ($fileInfo as $row) {

       $parts = preg_split('/(?<=[.?!])\s+(?=[a-z])/i', $row);
        foreach ($parts as $part) {
                        if ($part == '') {
                            continue;
                        }
                         $part = strtolower($part);
        				//  ***************  EMAIL  **************

                        if (strpos($part, '@') || strpos($part, 'mail')) {
                            $pattern = '/[a-z0-9_\-\+]+\.[a-z0-9_\-\+]+@[a-z0-9\-]+\.([a-z]{2,3})(?:\.[a-z]{2})?/i';
                            preg_match_all($pattern, $part, $matches);
                            foreach ($matches[0] as $match) {
                                $records['email'][] = $match;
                            }
                        }
                        if(empty($records['email']) || !isset($records['email'])){
                        	 if (strpos($part, '@') || strpos($part, 'mail')) {
	                            $pattern = '/[a-z0-9_\-\+]+@[a-z0-9\-]+\.([a-z]{2,3})(?:\.[a-z]{2})?/i';
	                            preg_match_all($pattern, $part, $matches);
	                            foreach ($matches[0] as $match) {
	                                $records['email'][] = $match;
	                            }
                        	}
                        }
                        if (isset($records['email'])) {
                              $names = preg_split( "/\\s|,|:|-/",strtolower($part));                            
                        	$shortest = -1;
                        	
	                            foreach ($names as $name) {
	                                $e = explode('@', $records['email'][0]);
	                                //$records['name'][] = $e[0];

	                          		 $lev = levenshtein($e[0], $name);
	                          		 $levArr[$lev]=$name;

									    // check for an exact match
									    if ($lev == 0) {

									        // closest word is this one (exact match)
									        $closest = $name;
									        $shortest = 0;
									        // break out of the loop; we've found an exact match
									        break;
									    }

									    // if this distance is less than the next found shortest
									    // distance, OR if a next shortest word has not yet been found
									    if ($lev <= $shortest || $shortest < 0) {
									        // set the closest match, and shortest distance
									        $closest  = $name;
									        $shortest = $lev;
									         
									    }
									  
	                       		 }

                        		}

                        	 
                        			      //  ***************  DOB  **************

                                if (preg_match('/dob|d.o.b|date of birth/', $part)) {
                                    $dob = preg_split('/:|-/', $part);

	                                    foreach ($dob as $db) {
	                                        $date = date_parse($dob[1]);
	                                      
	                                        if ($date['error_count'] == 0) {
	                                            $records['dob'][] = $date['year'].'-'.$date['month'].'-'.$date['day'];
	                                        }
	                                    }
                                	}
	                                $date = @find_date($part);
	                                if ($date) {
	                                    $dateTemp=$date['year'].'-'.$date['month'].'-'.$date['day'];
	                                   if(validateDate($dateTemp) && validateLegalAge($dateTemp)){
	                                         $records['dob'][] = $dateTemp;
	                                    }
	                                    
	                                }
	                                $p = '{.*?(\d\d?)[\\/\.\-]([\d]{2})[\\/\.\-]([\d]{4}).*}';
	                                if (preg_match($p, $part)) {
	                                    $date = preg_replace($p, '$3-$2-$1', $part);
					                    try{
	                                    	$dd = new \DateTime($date);
	                                    	$dateTemp=$dd->format('Y-m-d');
	                                    	if(validateDate($dateTemp) && validateLegalAge($dateTemp)){
	                                        	 $records['dob'][] = $dateTemp;
	                                          }
	                				    }catch(Exception $ex){
	                						echo $ex->getMessage();
	                				    }
	                                   
	                                }

				                        //  ***************  MOBILE  **************

				                        preg_match_all('/\d{10}/', $part, $matches);
				                        if (count($matches[0])) {
				                            foreach ($matches[0] as $mob) {
				                                $records['mobile'][] = $mob;
				                            }
				                        }

				                        preg_match_all('^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$^', $part, $matches);
				                        if (count($matches[0])) {
				                            foreach ($matches[0] as $mob) {
				                                $records['mobile'][] = $mob;
				                            }
				                        }
				                        preg_match_all('/(\d{3})-(\d{3})-(\d{4})/', $part, $matches);
				                        if (count($matches[0])) {
				                            foreach ($matches[0] as $mob) {
				                                $records['mobile'][] = $mob;
				                            }
				                        }

				                  // ******place**********
				                    $place1 = preg_split( "/\\s|,|:|-/",strtolower($part));
                                     foreach ($places as $key => $value) {
                                			 if (in_array(strtolower($value),$place1)){
                                     	 		$records['place'][]=$value;
                                     	 	}
                                     }

                                 // ******skill*********
	                               if (preg_match('/skillsets|skill|skills/',$part)){
							     	 $skill_done = preg_split('/\\s/', $part);
							     	foreach ($skill_done as $key => $value) {
							     		 if (in_array(strtolower($value),$skilltomatch)){
							     	 	 		$records['skill_done'][]=$value;
							     	 		}
							     		}
							     	}

							     // **********title***********
							     $str=$current_designation_csv.",manager,developer,support,executive,engineer,analyst,trainee,architect,lead,team,specialist,partner,delivery,maintenance,project,system,consultant,director,general,assistant,chief,admin,joint,platform,engineering,technician,research,talent,acquisition,trainee,human,resource,client"; 
							     $arr=explode(",",$str);
							   	 $temp="";
							     foreach ($arr as $key => $value) {
										$strpos=stripos($part,$value);
							     	 		if($strpos){
							     	 				$splitpart=preg_split('/\\s|,/', $part);
							     	 					foreach ($splitpart as $k => $v) {
							     	 						if($value==$v){
							     	 							$temp=$splitpart[$k-1]." ".$value;
							     	 							if(strlen($temp)){
							     	 								$records['title'][]=$temp;
							     	 							}
							     	 						}
							     	 						
							     	 				}
							     	 				
							     	 			}
							     }

                    		}

               
                       foreach ($records as $key => $value) {
                       	if(is_array($value)){
								$records[$key] = array_unique($value);
                       		}
                		}
      if(count($levArr)){
   			$records['namematch']=$levArr[min(array_keys($levArr))];    
      }
    }
    $details[] = $records; 
    print_r($details);
    $password=md5(Configuration::$salt.Configuration::$defaultPassword);
    $username=trim($details[0]['namematch']);
    $email=trim($details[0]['email'][0]);
    if((isset($username) && !empty($username)) && (isset($email) && !empty($email))){
		 	$query="INSERT INTO user_registration (username,password,email,user_map_id) VALUES('".$username."','".$password."','".$email."',2)";
		 	mysqli_query($con,$query);
		 	$user_id=mysqli_insert_id($con);
		$current_designation="";
		if(isset($details[0]['title'][0]) && !empty($details[0]['title'][0])){
			$current_designation=$details[0]['title'][0];
		}
		$city="";
		if(isset($details[0]['place']) && !empty($details[0]['place'])){
			$city=implode(",",$details[0]['place']);
		}
		$contact_number="";
		if(isset($details[0]['mobile']) && !empty($details[0]['mobile'])){
			$contact_number=implode(",",$details[0]['mobile']);
		}
		$dob="";
		if(isset($details[0]['dob']) && !empty($details[0]['dob'])){
			$dob=implode(",",$details[0]['dob']);
		}
		$skill_done="";
		if(isset($details[0]['skill_done']) && !empty($details[0]['skill_done'])){
			$skill_done=implode(",",$details[0]['skill_done']);
		}
		$insert_jobseeker_dump="INSERT INTO job_seeker_details_dump(user_id,first_name,DOB,contact_number,city,current_designation,skills,resume_path) VALUES (".$user_id.",'".$username."','".$dob."','".$contact_number."','".$city."','".$current_designation."','".$skill_done."','/resume/bulkupload/extractedresumes/".$valuefile."')";
		mysqli_query($con,$insert_jobseeker_dump);
		rename("files/".$valuefile,"extractedresumes/".$valuefile);
    }
    else{
    	echo "user not registered for resume ".$valuefile;
    }
 
	
}

?>
