<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
    include_once("includes/redirect_IF_not_admin_jobseeker.php");
?>
<link rel="stylesheet" type="text/css" href="/assets/css/chats.css">
<div class="mj_lightgraytbg mj_bottompadder80">
        <div class="container">
        	<div class="chat_window">
        		<div class="top_menu">
        			<div class="title"></div>
        		</div>
        		<ul class="messages" id="message-history" data-attr-id="<?php echo $user_id; ?>">
        		</ul>
        		<div class="bottom_wrapper clearfix">
        			<div class="message_input_wrapper">
        				<input class="message_input" placeholder="Type your message here..." />
        			</div>
        			<div class="send_message" id="send-chat">
        				<div class="icon"></div>
        				<div class="text">Send</div>
        			</div>
        		</div>
        	</div>
       	</div>
</div>
      
<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/messages.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.messagesrecruiter.init();
        });
    </script>
</body>

</html>