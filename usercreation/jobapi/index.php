<?php

require __DIR__ . '/vendor/autoload.php';

$providers = [
    'Careercast' => [],
    'Dice' => [],
    'Github' => [],
    'Careerjet' => [
        'affid' => '0afaf0173305e4b9',
    ],
    'Govt' => [],
    'Ieee' => [],
    'Jobinventory' => [],
    'Monster' => [],
    'Stackoverflow' => []
];//0afaf0173305e4b9
$what="";
$where="";
if(isset($_POST['what']) && !empty($_POST['what'])){
    $what = $_POST['what'];
}
if(isset($_POST['where']) && !empty($_POST['where'])){
    $where = $_POST['where'];
}
$client = new \JobApis\Jobs\Client\JobsMulti($providers);
$client->setKeyword($what)
    ->setLocation($where)
    ->setPage(1, 20);
    $jobs = $client->getAllJobs();
 $finalarray=array();
 $i=0;
 foreach ($jobs->all() as $job) {
    $jobdetail=$job->toArray();
    $finalarray[$i]['company']=$jobdetail['company'];
    $finalarray[$i]['location']=$jobdetail['location'];
    $finalarray[$i]['url']=$jobdetail['url'];
    $finalarray[$i]['title']=$jobdetail['title'];
    $finalarray[$i]['skill']=$what;
    $finalarray[$i]['description']=$jobdetail['description'];
    $finalarray[$i]['experience']=$jobdetail['experienceRequirements'];
    $finalarray[$i]['other']=1;
    $i++;
}
echo json_encode($finalarray);
  ?>