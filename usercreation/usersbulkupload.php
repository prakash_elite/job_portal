<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
    include_once("includes/redirect_IF_notadmin.php");

?>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
         <div class="mj_candidate_section">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mj_tabcontent mj_bottompadder80 woo-cart-table">
                    <div class="table-responsive">
                       <table class="table table-striped" id="list-table"> 
								<thead>
									<th>Username</th>
									<th>Email</th>									
									<th>First Name</th>
									<th>Contact Number</th>		
									<th>Title</th>		
									<th>Skills</th>	
									<th>City</th>										
									<th>Date of Birth</th>	
									<th>User Category</th>	
								</thead>             
                            <tbody>
                            </tbody>
                       </table>
                    </div>
                   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4">
                        <div class="mj_showmore"> 
                        	<a id="show-more" class="mj_showmorebtn mj_blackbtn">Show More</a> 
                        </div>
                   </div>
                 </div>
             </div>
          </div>		
	</div>
</div>

<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/usersbulkupload.js" type="text/javascript"></script>   
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.usersbulkupload.init();
        });
    </script>
</body>

</html>
