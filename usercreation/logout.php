<?php
	session_start();
	unset($_SESSION['authkey']);
	unset($_SESSION['profile_pic']);
	session_destroy();
	header('location:/index.html');
?>