<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
?>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
        	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Change Profile Pic</h4>
                    <div class="mj_postdiv mj_shadow_yellow mj_postpage mj_toppadder50 mj_bottompadder50">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                           <form id="register-form" class="new-line-fields form-horizontal" enctype="multipart/form-data" action="/changeprofilepic">
                                    <div class="form-group">
                                        <img id="uploaded-img" class="uploaded-img" src="">
                                        <input type="hidden" name="uploaded-img-hidden" id="uploaded-img-hidden" value="" /> 
                                    </div>
                                    <div class="form-group">
                                        <a class="mj_mainbtn mj_btnblue" id="trigger-profile-img" data-text="Change Profile Image "><span>Change Profile Image </span></a>
                                         <input id="profile-pic" name="profile_pic" accept="image/*" class="form-control hide" type="file">
                                    </div>

                                <button type="submit" id="update" name="update" class="hide"></button>
                            </form>
                        </div>
                    </div>
                    <div class="mj_showmore">
                        <a id="trigger-img-submit" class="mj_showmorebtn mj_bigbtn mj_yellowbtn">UPDATE</a>
                    </div>
                </div>
            </div>
    </div>
    <div class="modal fade mj_popupdesign" id="submit-success" tabindex="-1" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                        <div class="row">
                            <div class="mj_pricingtable mj_greentable mj_login_form_wrapper">
                                    <div class="mj_login_form">
                                            <p>Updated Successfully</p>
                                    </div>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="assets/images/close.png" alt="">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
    include_once("includes/foot.php");
?>
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.utilities.ajaxCalls("GET","/getprofilepic",{},function(thisVariable,data){
                $('#uploaded-img').attr('src',data);
                $('#uploaded-img-hidden').val(data);
            },"");
        	jobportal.utilities.initTriggerElement($('#trigger-profile-img'),$('#profile-pic'));
            jobportal.utilities.initTriggerElement($('#trigger-img-submit'),$('#update'));
       		$('#register-form').on('submit',function(e){
					e.preventDefault();
                    var formData = new FormData($(this)[0]);
					$.ajax({
			            type: "POST",
			            url: $(this).attr('action'),
                        data: formData,
                        processData: false, 
                        contentType: false,// serializes the form's elements.
			            beforeSend: function(request){
		                 request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
			            success: function(data){
                            $('#uploaded-img').attr('src',data);
                            $('#uploaded-img-hidden').val(data);
			           		$('#submit-success').modal('show');
                            $('.mj_profileimg .logo-size').attr('src',data);
			            }
			         });
			});	
        });
    </script>
</body>

</html>
