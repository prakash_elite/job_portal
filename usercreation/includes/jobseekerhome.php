       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="top_searchbox">
                <form>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-search"></i>
                            </div>
                             <select class="search-align"  name="search" id="search-all" >
                            </select>
                        </div>
                    </div>
                 </form>
             </div>
         </div>
         <div class="mj_candidate_section">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mj_tabcontent mj_bottompadder80 woo-cart-table">
                 <div class="table-responsive">
                   <table class="table table-striped" id="list-table"> 
                   <thead>
                            <tr>
                                <th>Job Title</th>
                                <th>Applied On</th>
                                <th><a title="Refresh" href="javascript:window.location.reload()"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                </th>
                            </tr>  
                   </thead>       
                    <tbody>
                    </tbody>
                   </table>
                   </div>
                    <div id='no-data-available'>No Data Available</div>
                   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4">
                        <div class="mj_showmore"> 
                          <a href="#" class="mj_showmorebtn mj_blackbtn">Show More</a> 
                        </div>
                   </div>
                 </div>
             </div>
          </div>