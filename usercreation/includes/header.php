<?php 
    
    require 'vendor/autoload.php';
    require_once("includes.php");
    use \Firebase\JWT\JWT;
     $user_id=false;
     $userType=false;
     $profile_pic="/assets/images/50X50.png";
     $homepage="index.html";

        if(isset($_SESSION['authkey']) && !empty($_SESSION['authkey'])){
            $jwt=$_SESSION['authkey'];
            $secretKey = base64_decode(Configuration::$webTokenPrivateKey);
                JWT::$leeway = 10; 
            $tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
            if($_SERVER['PHP_SELF']=="/login.php"){
                   unset($_SESSION['authkey']);
                    session_destroy();
            }
            else{
                 $user_id=$tokenDetails->data->userId;
                 $userType=$tokenDetails->data->userType;
                 if(isset($_SESSION['profile_pic']) && !empty($_SESSION['profile_pic'])){
                        $profile_pic=$_SESSION['profile_pic'];
                 }
                 $homepage="/home.php";
            }
        }

 ?>
    <!--Loader End -->

    <div class="mj_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="mj_logo">
                        <a href="<?php echo $homepage; ?>" >
                            <img src="/assets/images/logo.png" class="img-responsive" alt="logo" />
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mj_menu" aria-expanded="false">
                            <span class="sr-only">MENU</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="collapse navbar-collapse mj_navmenu" id="mj_menu">
                       <?php if($user_id){    ?>
                        <ul class="nav navbar-nav">
                            <?php  
                                if($userType && $userType==1){
                                    require_once("headerforadmin.php");
                                    require_once("headerforrecruiter.php");
                                    require_once("headerforjobseeker.php");                                    
                                }
                                elseif($userType && $userType==2){
                                    require_once("headerforjobseeker.php");
                                } 
                                elseif($userType && $userType==3){
                                    require_once("headerforrecruiter.php");
                                }
                            ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right mj_right_menu mj_login_menu">
                           <li>
                                <a class="mj_profileimg"><img class="logo-size" src="<?php echo $profile_pic; ?>" alt="user"><i class="fa fa-angle-down"></i>
                                </a>
                            </li>
                            <div class="mj_profilediv" id="my_profile_div">
                                <ul>
                                    <li>
                                        <a href="/changepassword.php"><i class="fa fa-cog"></i>Change Password</a>
                                    </li>
                                    <li>
                                    <?php
                                        if($userType && $userType==2){ 
                                            echo '<a href="profileseeker.php"><i class="fa fa-user"></i>Manage Profile</a>';
                                        } 
                                    ?>
                                    </li>
                                    <li>
                                    <?php
                                        if($userType && $userType==2){ 
                                        echo '<a href="viewprofile.php"><i class="fa fa-file-text-o"></i>View Profile</a>';
                                        } 
                                    ?>
                                    </li>
                                    <li>
                                        <a href="/changeprofileimg.php"><i class="fa fa-picture-o" aria-hidden="true"></i>Change profile pic</a>
                                    </li>
                                    <li>
                                        <a href="/logout.php"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </ul>  
                         <?php } else{    ?>
                          <ul class="nav navbar-nav navbar-right mj_right_menu mj_withoutlogin_menu">
                             <li>
                                <a class="mj_logintoggle" href="/login.php"><i class="fa fa-user"></i> Login</a>
                             </li>
                              <li>
                                <a class="mj_logintoggle" href="/registerjobseeker.php">Post your resume</a>
                             </li>
                           </ul>   
                         <?php } ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
