    <meta charset="utf-8" />
    <title>Job Portal</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  	<meta name="description" content="Offers all solutions for Recruiter and Jobseekers" />
    <meta property="og:title" content="Employee Search | Job Search ">
    <meta property="og:description" content="Offers all solutions for Recruiter and Jobseekers">
    <meta property="og:image" content="http://portal.chalkkeeper.com/assets/images/signup_bg1.png">
    <meta property="og:url" content="http://portal.chalkkeeper.com/index.html">
    <meta name="twitter:card" content="Offers all solutions for Recruiter and Jobseekers">
    <meta name="keywords" content="">
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320">