<li>
    <a href="#">Admin</a>
    <ul class="sub_menu">
        <li>
            <a href="/register.php">Register User</a>
        </li>
        <li>
            <a href="/users.php">All Users</a>
        </li>
        <li>
            <a href="/bulkupload/index.php">BULK UPLOAD</a>
        </li>
        <li>
            <a href="/usersbulkupload.php">USERS FROM BULK UPLOAD</a>
        </li>
    </ul>
</li>
<li>
<a href="#">DETAILS</a>
    <ul class="sub_menu">
        <li>
            <a href="/city.php">CITY</a>
        </li>
        <li>
            <a href="/country.php">COUNTRY</a>
        </li>
        <li>
            <a href="/language.php">LANGUAGE</a>
        </li>    
        <li>
            <a href="/skills.php">SKILLS</a>
        </li>    
        <li>
            <a href="/industry.php">INDUSTRY</a>
        </li>        
        <li>
            <a href="/function.php">FUNCTIONAL AREA</a>
        </li>                
    </ul>
</li>
