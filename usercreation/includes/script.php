    <script src="/assets/js/lib/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="/assets/js/lib/bootstrap.js" type="text/javascript"></script>
    <script src="/assets/js/lib/modernizr.custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="/assets/js/lib/plugins/rsslider/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="/assets/js/lib/plugins/rsslider/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="/assets/js/lib/plugins/rsslider/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="/assets/js/lib/plugins/rsslider/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="/assets/js/lib/plugins/rsslider/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="/assets/js/lib/plugins/rsslider/revolution.extension.slideanims.min.js"></script>
    <script src="/assets/js/lib/plugins/countto/jquery.countTo.js" type="text/javascript"></script>
    <script src="/assets/js/lib/plugins/owl/owl.carousel.js" type="text/javascript"></script>
    <script src="/assets/js/lib/plugins/bootstrap-slider/bootstrap-slider.js" type="text/javascript"></script>
    <script src="/assets/js/lib/plugins/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <script src="/assets/js/lib/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="/assets/js/lib/jquery.mixitup.js" type="text/javascript"></script>
    <script src="/assets/js/lib/plugins/jquery-ui/jquery-ui.js"></script>
    <script src="/assets/js/lib/plugins/isotop/isotope.pkgd.js"></script>
    <script src="/assets/js/lib/plugins/ckeditor/ckeditor.js"></script>
    <script src="/assets/js/lib/plugins/ckeditor/adapters/jquery.js"></script>
    <script src="/assets/js/lib/custom.js" type="text/javascript"></script>
    <script src="/assets/js/lib/select2.full.min.js" type="text/javascript"></script>
    <script src="/assets/js/jobportal.js?v=1" type="text/javascript"></script>



 
    