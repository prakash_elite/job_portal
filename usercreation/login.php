<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
?>
    <div class="mj_lightgraytbg mj_bottompadder80">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
                    <div class="mj_mainheading mj_toppadder80 mj_bottompadder50">
                        <h1>l<span>ogin</span> <span>with your</span> A<span>ccount</span></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3 col-sm-12 col-xs-12">
                    <h5 id="error">Enter a valid credentials</h5>
                    <div class="mj_pricingtable mj_greentable mj_login_form_wrapper">
                        <form method="post" action="/login" id="register-form">
                            <div class="mj_login_form">
                                <div class="form-group">
                                    <input type="text" name="username" placeholder="Username or Email" required="required" id="uname" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" placeholder="Your Password" required="required" id="upassword" class="form-control">
                                </div>
                    <!--             <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mj_toppadder20">
                                        <div class="form-group  pull-left">
                                            <div class="mj_checkbox">
                                                <input type="checkbox" value="1" id="check2" name="checkbox">
                                                <label for="check2"></label>
                                            </div>
                                            <span> remember me</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mj_toppadder20">
                                        <div class="form-group pull-right">
                                            <span><a href="#">forget password ?</a></span>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                             <button id="register-form-btn" type="submit" class="hide"></button>
                            <div class="mj_pricing_footer">
                                <a href="javascript:void(0)" id="register-form-link">login Now</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/login.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.login.init();
        });
    </script>
</body>

</html>