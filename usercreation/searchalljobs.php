<head>
    <?php

    include_once('includes/meta.php');
    include_once('includes/css.php');

    ?>
</head>
<body>
    <!--Loader Start -->
    <div class="mj_preloaded_serach show-must" >
        <div class="mj_preloader_serach show-must">
            <div class="lines">
                <div class="line line-1"></div>
                <div class="line line-2"></div>
                <div class="line line-3"></div>
            </div>

            <div class="loading-text">LOADING</div>
        </div>
    </div>
<?php
    include_once("includes/header.php");
    unset($_SESSION['authkey']);
    unset($_SESSION['profile_pic']);
    session_destroy();
    $what = '@@@@';
    $where = '';
    if(isset($_POST['what']) && !empty($_POST['what'])){
      $what = $_POST['what'];
    }
    if(isset($_POST['where']) && !empty($_POST['where'])){
      $where = $_POST['where'];
    }
    $skills=array();
    $skills=UsersDAO::searchSkillList($what);
    $final_result=array();
    $temp_result=array();
    $temp=array();
    if(count($skills)){
      foreach ($skills as $key => $value) {
          $temp=UsersDAO::searchAllJobs($value['id'],$where);
          if(count($temp)){
                array_push($temp_result,$temp);
          }

      }
    }
    else{
          $temp=UsersDAO::searchAllJobs("",$where);
          if(count($temp)){
                array_push($temp_result,$temp);
          }
    }
      foreach ($temp_result as $key => $value) {
            foreach ($value as $k => $v) {
              $final_result[$v['id']]['other']=0;
              $final_result[$v['id']]['id']=$v['id'];
              $final_result[$v['id']]['jobname']=$v['jobname'];
              $final_result[$v['id']]['company_name']=$v['company_name'];
              $final_result[$v['id']]['address']=$v['address'];
              $final_result[$v['id']]['experience']=$v['experience'];
              $final_result[$v['id']]['skills']=$v['skill_set'];
              $final_result[$v['id']]['industry']=$v['industry'];
              $final_result[$v['id']]['functional_area']=$v['function_area'];
              $final_result[$v['id']]['description']=$v['description'];

            }        
        }
?>

<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
        	  <div class="mj_candidate_section">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container"  id="job-list">
                  <?php 
                    foreach ($final_result as $key => $value) {
                      echo "
                          <div class='row' style='margin-top:10px;'>
                            <div class='mj_postdiv mj_jobdetail mj_toppadder10'>
                              <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12' id='list-table'>
                                <div class='row'>
                                  <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 job_details_sec'>
                                    <h2>".$value['jobname']."</h2>
                                    <div class='mj_detaildiv'>
                                      <blockquote class='mj_blueblockquote'>
                                        <span class='company_name_span'>".$value['company_name']."</span>
                                        <span><i class='fa fa-suitcase'></i> ".$value['experience']." years</span>
                                        <div class='mj_detaildiv'>
                                          <ul class='mj_selected_content'>
                                            <li><a href='#'>".$value['skills']."</a></li>
                                          </ul>
                                        </div>
                                        <h5>Description</h5>
                                        <p class='job_desc'>".$value['description']."</p>
                                        <span><i class='fa fa-map-marker'></i> ".$value['address']."</span>";
                                    if($value['other']){
                                      echo "<a href='".$value['id']."' class='mj_mainbtn mj_btnblue job-apply-btn' data-text='Apply'><span>Apply</span></a>
                                      ";
                                    }
                                    else{
                                   echo "<a href='#' class='mj_mainbtn mj_btnblue job-apply-btn contact-seeker' data-url='/candidateapplyJob' data-attr-id='".$value['id']."' data-text='Apply'><span>Apply</span></a>";
                                      }
                                    echo "</blockquote>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>";
                    }
                  ?>
                </div>             
              </div>
          </div>
      </div>
	</div>
            <!-- Modal Part -->
<div class="modal fade mj_popupdesign" id="submit-success" tabindex="-1" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2"> R<span>egister</span>Y<span>ourself</span></h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                        <div class="row">
                            <div class="mj_pricingtable mj_greentable mj_login_form_wrapper">
                              <form class="register-form" action="/jobseekerprofileOnHome">
                                    <div class="mj_freelancer_form">
                                       <h5 class="error"></h5>      
                                        <div class="form-group">
                                            <input type="text"  id="name" name="name" required="required" placeholder="Username" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="password"  id="password" required="required" name="password" placeholder="Password" class="form-control">
                                        </div>                                                
                                        <div class="form-group">
                                            <input type="email" id="email" required="required" name="email" placeholder="Email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" id="user_map" required="required" name="user_map" value="2">
                                        </div>
                                        <div class="form-group">
                                        <a class="mj_mainbtn mj_btnblue" id="trigger-upload-resume" data-text="Upload resume "><span>Upload resume </span></a>
                                         <input id="resume" name="resume" accept=".doc,.docx,.pdf" class="form-control hide" type="file">
                                        </div>
                                        <div class="form-group mj_toppadder20">
                                            <div class="mj_checkbox">
                                                <input type="checkbox" required="required" value="1" id="check3" >
                                                <label for="check3"></label>
                                            </div>
                                            <span> I have read, understand and agree to the meshjobs Terms of Service, including the <a href="#">User Agreement</a> and <a href="#">Privacy Policy</a>.</span>
                                        </div>
                                    </div>
                                     <button type="submit" class="hide register-form-btn"></button>
                                    <div class="mj_pricing_footer">
                                        <a href="#">Get Started Now!</a>
                                    </div>
                                </form>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="assets/images/close.png" alt="">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/searchcandidates_jobs.js" type="text/javascript"></script>
    <script src="assets/js/searchalljobs.js" type="text/javascript"></script>   
    <script type="text/javascript">

        
        $(document).ready(function(){
            $('.contact-seeker').on('click',function(){
              $('#submit-success').modal('show');
            });
             jobportal.searchalljobs.init();
          
        });
    </script>
</body>

</html>
