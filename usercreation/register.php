<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
?>

    <div class="mj_lightgraytbg mj_bottompadder80">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
                    <div class="mj_mainheading mj_toppadder80 mj_bottompadder50">
                        <h1>R<span>egister</span> U<span>ser</span></h1>
                       
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3 col-sm-12 col-xs-12">
                    <div class="mj_pricingtable mj_yellowtable mj_createaccount_form_wrapper">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#company" aria-controls="company" role="tab" data-toggle="tab">Recruiter</a>
                            </li>
                            <li role="presentation"><a href="#individual" aria-controls="individual" role="tab" data-toggle="tab">Individual sing up</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="company">
                                <form class="register-form" action="/register">
                                            <div class="mj_createaccount_form">
                                                <h5 class="error"></h5>                             
                                                <div class="form-group">
                                                    <input type="text"  id="name" name="name" required="required" placeholder="Username" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password"  id="password" required="required" name="password" placeholder="Password" class="form-control">
                                                </div>                                                
                                                <div class="form-group">
                                                    <input type="email" id="email" required="required" name="email" placeholder="Email" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                     <input type="hidden" id="user_map" required="required" name="user_map" value="3">
                                                </div>
                                                <div class="form-group mj_toppadder20">
                                                    <div class="mj_checkbox">
                                                        <input type="checkbox" value="1" required="required" id="check2" >
                                                        <label for="check2"></label>
                                                    </div>
                                                    <span> I have read, understand and agree to the meshjobs Terms of Service, including the <a href="#">User Agreement</a> and <a href="#">Privacy Policy</a>.</span>
                                                </div>
                                            </div>
                                               <button type="submit" class="hide register-form-btn"></button>
                                            <div class="mj_pricing_footer">
                                                <a href="#">Get Started Now!</a>
                                            </div>
                                        </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="individual">
                            <h5 id=""></h5>
                                <form class="register-form" action="/register">
                                    <div class="mj_freelancer_form">
                                       <h5 class="error"></h5>      
                                        <div class="form-group">
                                            <input type="text"  id="name" name="name" required="required" placeholder="Username" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="password"  id="password" required="required" name="password" placeholder="Password" class="form-control">
                                        </div>                                                
                                        <div class="form-group">
                                            <input type="email" id="email" required="required" name="email" placeholder="Email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" id="user_map" required="required" name="user_map" value="2">
                                        </div>
                                        <div class="form-group mj_toppadder20">
                                            <div class="mj_checkbox">
                                                <input type="checkbox" required="required" value="1" id="check3" >
                                                <label for="check3"></label>
                                            </div>
                                            <span> I have read, understand and agree to the meshjobs Terms of Service, including the <a href="#">User Agreement</a> and <a href="#">Privacy Policy</a>.</span>
                                        </div>
                                    </div>
                                     <button type="submit" class="hide register-form-btn"></button>
                                    <div class="mj_pricing_footer">
                                        <a href="#">Get Started Now!</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php 
    include_once("includes/foot.php");
?>
 <script src="assets/js/login.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.register.init();
        });
    </script>
</body>

</html>