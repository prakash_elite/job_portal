<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
    include_once("includes/redirect_IF_not_admin_recruiter.php");
    $getAllpath="/addCompany";
    if($userType==1){
        $getAllpath="/companies";
    }

?>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	         <div class="add-new-button-wrapper">
	         	  <a id="add-new" class="mj_mainbtn mj_btnblue" data-text="Add New"><span>Add New</span></a>
	         </div>
             <div class="top_searchbox">
             <input type="hidden" id="get-all-path" value="<?php echo $getAllpath; ?>">
                <form>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-search"></i>
                            </div>
                             <select class="search-align"  name="search" id="search-all" >
                            </select>
                        </div>
                    </div>
                 </form>
             </div>
         </div>
         <div class="mj_candidate_section">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mj_tabcontent mj_bottompadder80 woo-cart-table">
                    <div class="table-responsive">
                       <table class="table table-striped" id="list-table"> 
                       <thead>
                                <tr>
                                    <th></th>
                                    <th>Company name</th>
                                    <th>Address</th>
                                    <th>Added By</th>
                                    <th>Industry</th>
                                    <th></th>
                                </tr>  
                       </thead>                 
                            <tbody>
                            </tbody>
                       </table>
                   </div>
                    <div id='no-data-available'>No Data Available</div>                   
                   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4">
                        <div class="mj_showmore"> 
                        	<a  id="show-more" class="mj_showmorebtn mj_blackbtn">Show More</a> 
                        </div>
                   </div>
                 </div>
             </div>
          </div>		
	</div>
</div>
            <!-- Modal Part -->
<div class="modal fade mj_popupdesign" id="submit-success" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2"> A<span>dd</span> C<span>ompany</span></h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
       					<div class="row">
                            <div class="mj_pricingtable mj_greentable mj_login_form_wrapper">
                                <form method="post" action="/addCompany/1" class="register-form" enctype="multipart/form-data" class="form-horizontal" style="margin-top: 20px;">
                                    <div class="mj_login_form">
										<!-- Text input-->
										<div class="form-group">
										  <label class="pull-left">Company name</label>  
										  <input id="company_name" name="company_name" required="required" placeholder="Company name" class="form-control" type="text">
										 </div>

										<div class="form-group">
										  <label class="pull-left">Address</label>  
										  <input id="address" name="address" placeholder="address" class="form-control" required="" type="text">
										</div>
                                        <div class="form-group">
                                            <a class="mj_mainbtn mj_btnblue" id="trigger-profile-img" data-text="Add logo"><span>Add Logo </span></a>
                                             <input id="profile-pic" name="profile_pic" accept="image/*" class="form-control hide" type="file">
                                        </div>
										<!-- Text input-->
										<div class="form-group">
										  <label class="pull-left">Number of employees</label>  
										  <input id="number_of_employees"  name="number_of_employees" placeholder="Number of employees" class="form-control" min="0" type="number">
										</div>

										<div class="form-group">
										  <label class="pull-left">Industry</label>  
										  <select required="required" id="current_industry" name="industry" class="form-control" >
										  </select>  
										   <p class="previously-selected" id="industry_selected">None Selected</p> 
										</div>
									<button id="update" name="update" class="hide"></button>
									</div>
	                                <div id="trigger-update" class="mj_pricing_footer">
	                                    <a >ADD</a>
	                                </div>
                            	</form>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="assets/images/close.png" alt="">
                            </button>
                        </div>
                     </div>
                   </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade mj_popupdesign" id="edit-submit-success" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2"> E<span>dit</span> C<span>ompany</span></h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
       					<div class="row">
                            <div class="mj_pricingtable mj_greentable mj_login_form_wrapper">
                                <form method="post" action="/companies/" class="register-form" class="form-horizontal" style="margin-top: 20px;" enctype="multipart/form-data">
                                    <div class="mj_login_form">
										<!-- Text input-->
										<div class="form-group">
										  <label class="pull-left">Company name</label>  
										  <input id="company_name-edit" name="company_name" required="required" placeholder="Company name" class="form-control" type="text">
										 </div>

										<div class="form-group">
										  <label class="pull-left">Address</label>  
										  <input id="address-edit" name="address" placeholder="address" class="form-control" required="" type="text">
										</div>
                                        <div class="form-group">
                                            <img id="uploaded-img" class="uploaded-img" src="">
                                            <input type="hidden" name="uploaded-img-hidden" id="uploaded-img-hidden" value="" /> 
                                        </div>
                                        <div class="form-group">
                                            <a class="mj_mainbtn mj_btnblue" id="trigger-profile-img-edit" data-text="Change Logo"><span>Change Logo</span></a>
                                             <input id="profile-pic-edit" name="profile_pic" accept="image/*" class="form-control hide" type="file">
                                        </div>
										<!-- Text input-->
										<div class="form-group">
										  <label class="pull-left">Number of employees</label>  
										  <input id="number_of_employees-edit"  name="number_of_employees" placeholder="Number of employees" class="form-control" min="0" type="number">
										</div>

										<div class="form-group">
										  <label class="pull-left">Industry</label>  
										  <select required="required" id="current_industry-edit" name="industry" class="form-control" >
										  </select>  
										   <p class="previously-selected" id="industry_selected-edit">None Selected</p> 
										</div>
									<button id="update-edit" name="update" class="hide"></button>
									</div>
	                                <div id="trigger-update-edit" class="mj_pricing_footer">
	                                    <a >UPDATE</a>
	                                </div>
                            	</form>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="assets/images/close.png" alt="">
                            </button>
                        </div>
                     </div>
                   </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/company.js" type="text/javascript"></script>   
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.company.init();
        });
    </script>
</body>

</html>
