<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
    include_once("includes/redirect_IF_not_admin_jobseeker.php");

?>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
        	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Add Job</h4>
                    <div class="mj_postdiv mj_shadow_yellow mj_postpage mj_toppadder50 mj_bottompadder50">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                           <form method="post" action="/searchjob" id="register-form"  id="register-form" class="new-line-fields form-horizontal" enctype="multipart/form-data">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                          <label>Skill</label>
                                           <select id="skills" name="skills[]" required="required" class="form-control input-md"></select>
                                        </div>

                                        <div class="form-group">
                                          <label>Experience(in Months)</label>  
                                          <input id="experience" required="required" name="experience" placeholder="Experience" class="form-control input-md" min="0" type="number">
                                          </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                          <label>Industry</label>  
                                          <select id="industry" name="industry" class="form-control input-md" >
                                          </select>   
                                        </div>

                                        <div class="form-group">
                                          <label>Functional area</label>  
                                          <select id="functional_area" name="functional_area"  class="form-control input-md" >
                                          </select> 
                                        </div>
                                     </div>
                                        <button type="submit" id="update" name="update" class="hide"></button>
                                </form>
                            </div>
                        </div>
                    <div class="mj_showmore">
                        <a id="trigger-search" class="mj_showmorebtn mj_bigbtn mj_yellowbtn">Search</a>
                    </div>
            </div>
            <div class="mj_candidate_section">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mj_tabcontent mj_bottompadder80 woo-cart-table">
                    <div class="table-responsive">
                       <table class="table table-striped" id="list-table"> 
                       <thead>
                                <tr>
                                    <th>Job Title</th>
                                    <th>Posted By</th>
                                    <th>Email</th>
                                    <th></th>  
                                </tr>  
                       </thead>               
                            <tbody>
                            </tbody>
                       </table>
                      </div>
                      <div id='no-data-available'>No Data Available</div>
                    </div>
                </div>
            </div>
	</div>
</div>
            <!-- Modal Part -->
<div class="modal fade mj_popupdesign" id="submit-success" tabindex="-1" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                        <div class="row">
                            <div class="mj_pricingtable mj_greentable mj_login_form_wrapper">
                                    <div class="mj_login_form">
                                          <p>Succesfully Applied</p>
                                    </div>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="assets/images/close.png" alt="">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/searchcandidates_jobs.js" type="text/javascript"></script>   
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.searchJobs.init();
        });
    </script>
</body>

</html>
