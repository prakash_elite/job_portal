<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
    include_once("includes/redirect_IF_not_admin_recruiter.php");

?>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
        	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Add Job</h4>
                    <div class="mj_postdiv mj_shadow_yellow mj_postpage mj_toppadder50 mj_bottompadder50">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                           <form id="register-form" class="new-line-fields form-horizontal" enctype="multipart/form-data" action="/postjob">
                                        <div class="form-group">
                                          <label>Job Title</label>  
                                          <input id="jobname" name="jobname" required="required" placeholder="Job Title" class="form-control" type="text">
                                        </div>
                                         <div class="form-group">
                                          <label>Job Description</label>  
                                          <textarea class="form-control" name="job_description" required="required"></textarea>
                                        </div>
                                        <div class="form-group">
                                          <label>Address</label> 
                                          <input id="address" name="address" required="required" placeholder="address" class="form-control" required="" type="text">
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label>Experience(in Months)</label>  
                                          <input id="experience" required="required" name="experience" placeholder="Experience" class="form-control" min="0" type="number">
                                        </div>

                                        <div class="form-group">
                                          <label required="required" class="col-xs-12 remove-left-padding">Skill</label>
                                           <select id="skills" name="skills[]" required="required" class="form-control"></select>
                                        </div>

                                        <div class="form-group">
                                          <label>Industry</label>  
                                          <select required="required" id="current_industry" required="required" name="industry" class="form-control" >
                                          </select>   
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label>Functional area</label>  
                                          <select required="required" id="current_functional_area" required="required" name="functional_area"  class="form-control">
                                          </select> 
                                        </div>
                                    
                                        <div class="form-group">
                                          <label>Company</label>  
                                          <select id="company-by-user" required="required" name="company"  class="form-control" >
                                          </select> 
                                        </div>
                                <button type="submit" id="update" name="update" class="hide">ADD</button>
                            </form>
                        </div>
                    </div>
                    <div class="mj_showmore">
                        <a id="trigger-job-upload" class="mj_showmorebtn mj_bigbtn mj_yellowbtn">ADD</a>
                    </div>
                </div>
            </div>
	</div>
</div>
    </div>
<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/jobopr.js" type="text/javascript"></script>   
    <script type="text/javascript">
        $(document).ready(function(){
            localStorage.removeItem('job');
            jobportal.jobopr.init();
        });
    </script>
</body>

</html>
