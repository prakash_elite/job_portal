<?php
use \Firebase\JWT\JWT;
$app->get('/searchjobtitle/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$userId=$tokenDetails->data->userId;
			$searchParam=$args['param'];
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::SearchAllJobsForAdmin($searchParam));
			}	
			elseif ($tokenDetails->data->userType==3) {
						echo json_encode(UsersDAO::listJobsByUserIdSearch($userId,$searchParam));
			}
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->get('/searchusers/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::SearchAllUsersForAdmin($args['param']));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
$app->get('/getalltitles/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1 || $tokenDetails->data->userType==3){		
					echo json_encode(UsersDAO::SearchCurrentTitle($args['param']));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
$app->get('/searchcompanies/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$searchParam=$args['param'];
			$userId=$tokenDetails->data->userId;
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::SearchAllCompaniesForAdmin($searchParam));
			}	
			elseif ($tokenDetails->data->userType==3) {
				echo json_encode(UsersDAO::listCompanySearchByUserId($userId,$searchParam));
			}
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});


$app->get('/searchcities/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::SearchAllCitiessForAdmin($args['param']));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->get('/searchlang/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::SearchAllLangForAdmin($args['param']));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->get('/searchskills/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::SearchAllSkillForAdmin($args['param']));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->get('/searchcountries/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::SearchAllCountryForAdmin($args['param']));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->get('/searchfunctionalarea/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::SearchAllFunctionalAreasForAdmin($args['param']));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->get('/searchindustry/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			if($tokenDetails->data->userType==1){		
					echo json_encode(UsersDAO::SearchAllIndustriesForAdmin($args['param']));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});

$app->get('/searchappliedjobs/{param}', function ($request, $response, $args) {
		try {
			$jwt=$request->getHeaderLine('Authorization');
			$secretKey = base64_decode(Configuration::$webTokenPrivateKey);
			JWT::$leeway = 10; 
			$tokenDetails = JWT::decode($jwt, $secretKey, array('HS512'));
			$searchParam=$args['param'];
			$userId=$tokenDetails->data->userId;
			$searchParam=$args['param'];
			if($tokenDetails->data->userType==2){		
					echo json_encode(UsersDAO::SearchAllAppliedJobsForJobSeeker($userId,$searchParam));
			}	
			else{
					return $response->withStatus(400)->write('Unauthorized');
			}
		} catch (Exception $e) {
		                /*
		                 * the token was not able to be decoded.
		                 * this is likely because the signature was not able to be verified (tampered token)
		                 */
		                header('HTTP/1.0 401 Unauthorized');
		   }
});
?>