<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
?>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
        	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Add Job</h4>
                    <div class="mj_postdiv mj_shadow_yellow mj_postpage mj_toppadder50 mj_bottompadder50">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                           <form id="register-form" class="new-line-fields form-horizontal" enctype="multipart/form-data" action="/changepassword">
                                    <div class="form-group">
                                          <label>New Password</label>  
                                          <input id="password" name="password" required="required" placeholder="New Password" class="form-control" type="password">
                                        </div>

                                <button type="submit" id="update" name="update" class="hide">ADD</button>
                            </form>
                        </div>
                    </div>
                    <div class="mj_showmore">
                        <a id="trigger-change-password" class="mj_showmorebtn mj_bigbtn mj_yellowbtn">UPDATE</a>
                    </div>
                </div>
            </div>
	</div>
</div>
    </div>
<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/jobopr.js" type="text/javascript"></script>   
    <script type="text/javascript">
        $(document).ready(function(){
        	jobportal.utilities.initTriggerElement($('#trigger-change-password'),$('#update'));
       		$('#register-form').on('submit',function(e){
					e.preventDefault();
					var datatoSend=$(this).serialize();
					$.ajax({
			           type: "POST",
			           url: $(this).attr('action'),
			           data: datatoSend, // serializes the form's elements.
			           beforeSend: function(request){
		                request.setRequestHeader("Authorization",localStorage.getItem('sess'));
		      			 },
			           success: function(data)
			           {
			           		window.location.href="logout.php";
			           }
			        });
			});	
        });
    </script>
</body>

</html>
