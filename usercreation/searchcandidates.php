<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
    include_once("includes/redirect_IF_not_admin_recruiter.php");

?>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
        	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <h4>Filters</h4>
                    <div class="mj_postdiv mj_shadow_yellow mj_postpage mj_toppadder50 mj_bottompadder50">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <form method="post" action="/searchcandidate" id="register-form"  id="register-form" class="new-line-fields form-horizontal" enctype="multipart/form-data">
                                        <div class="form-group">
                                          <label>Skill</label>
                                           <select id="skills" name="skills[]" class="form-control input-md"></select>
                                        </div>

                                        <div class="form-group">
                                          <label>Job Title</label>  
                                          <select id="current-title" name="current_designation"  class="form-control input-md" >
                                          </select> 
                                        </div>

                                        <div class="form-group">
                                          <label>Highest Education</label>  
                                          <select id="highest_education" name="highest_education"  class="form-control input-md" >
                                            <option value="1">High school</option>
                                            <option value="2">Bachelors</option>
                                            <option value="3">Graduate</option>
                                          </select> 
                                        </div>

                                        <div class="form-group">
                                          <label>Experience(in Months)</label>  
                                          <input id="experience"  name="experience" placeholder="Experience" class="form-control input-md" min="0" value=null type="number">
                                        </div>

                                        <div class="form-group">
                                          <label>Compensation(Less than)</label>  
                                          <input id="compensation" name="expected_CTC" placeholder="Compensation" class="form-control input-md" min="0" type="number">
                                        </div>
                                        <div class="form-group">
                                          <label>Industry</label>  
                                          <select id="current_industry" name="industry" class="form-control input-md" >
                                          </select>   
                                        </div>

                                        <div class="form-group">
                                          <label>Functional area</label>  
                                          <select id="current_functional_area" name="functional_area"  class="form-control input-md" >
                                          </select> 
                                        </div>

                                         <div class="form-group">
                                          <label>Company</label>  
                                          <select id="company" name="current_company"  class="form-control input-md" >
                                          </select> 
                                        </div>

                                         <div class="form-group">
                                          <label>Location</label>  
                                          <select id="city" name="city"  class="form-control input-md" >
                                          </select> 
                                        </div>

                                         <div class="form-group">
                                            <label>Desired employment type</label>
                                              <select id="desired_employment_type" name="desired_employment_type" class="form-control">
                                                <option value="1">Full time</option>
                                                <option value="2">Contract</option>
                                                <option value="3">Internship</option>
                                              </select>
                                          </div>

                                        <div class="form-group">
                                          <label>Active Since(in Months)</label>  
                                          <input id="active_since" name="last_login" placeholder="Active Since" class="form-control input-md" min="0" type="number">
                                        </div>

                                        <button type="submit" id="update" name="update" class="hide"></button>
                                </form>
                            </div>
                        </div>
                    <div class="mj_showmore">
                        <a id="trigger-search" class="mj_showmorebtn mj_bigbtn mj_yellowbtn">Search</a>
                    </div>
            </div>
             <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <div class="mj_candidate_section">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="mj_tabcontent mj_bottompadder80 woo-cart-table">
                       <div class="table-responsive">
                         <table class="table table-striped" id="list-table"> 
                         <thead>
                                  <tr>
                                      <th></th>
                                      <th>Name</th>
                                      <th></th>  
                                  </tr>  
                         </thead>               
                              <tbody>
                              </tbody>
                         </table>
                        </div>
                          <div id='no-data-available'>No Data Available</div>
                      </div>
                  </div>
              </div>
            </div>
	</div>
</div>
<?php 
    include_once("includes/foot.php");
?>
    <script src="/assets/js/searchcandidates_jobs.js" type="text/javascript"></script>   
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.searchCandidates.init();
            localStorage.removeItem('contactmapid');
            localStorage.removeItem('contactseeker');
        });
    </script>
</body>

</html>
