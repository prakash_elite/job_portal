<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
?>
    <div class="mj_lightgraytbg mj_bottompadder80">
        <div class="container">
        <?php  
            if($userType && $userType==1){
                require_once("includes/adminhome.php");                                  
            }
            elseif($userType && $userType==2){
                require_once("includes/jobseekerhome.php");
                                } 
            elseif($userType && $userType==3){
                require_once("includes/recruiterhome.php");
            }
        ?>            
        </div>
    </div>
<?php 
    include_once("includes/foot.php");
    if($userType && $userType==1){
        echo '<script src="assets/js/adminhome.js" type="text/javascript"></script>';                     
    }
    elseif($userType && $userType==2){
        echo '<script src="assets/js/jobseekerhome.js" type="text/javascript"></script>'; 
    } 
    elseif($userType && $userType==3){
        echo '<script src="assets/js/recruiterhome.js" type="text/javascript"></script>'; 
    }
?>
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.home.init();
        });
    </script>
</body>

</html>