# README #

Installation Steps

1) Install Composer
	curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

2) Install Slim
	composer require slim/slim "^3.0"

3) Install php-jwt
	composer require firebase/php-jwt

4) Install mcrypt
if php7 add 7.0 instead of 5 //gearman sudo apt-get install gearman
	i)sudo apt-get install php7.0-mbstring,php7.0-zip --> no need to install these package if you are using php5 or ubuntu-16

    i) sudo apt-get install php5 nginx php5-fpm php5-mysql mcrypt php5-mcrypt mysql-client mysql-server php5-cli mcrypt php5-mcrypt php-pear php5-dev gearman-job-server libgearman-dev
    ii)pecl install gearman
    iii) add line "extension=mcrypt.so","extension=gearman.so" in /etc/php5/fpm/php.ini
    iv) restart php5-fpm and nginx
    v)"extension=gearman.so" in /etc/php5/cli/php.ini

5) rewrite in nginx for routes
    try_files $uri /index.php$is_args$args;

6) Create database and add user
	i) CREATE USER 'newuser'@'%' IDENTIFIED BY 'password';
	ii) CREATE DATABASE jp_users_db DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
	iii) GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON jp_users_db.* TO mysqluser@'%' IDENTIFIED BY 'password';
	iv) change bind address on /etc/mysql/my.cnf to 0.0.0.0 and restart mysql

7) Run migrations from migrations folder
	i) schema.sql
	ii) populate.sql
	iii) country.sql 
	iv) city.sql
	v) industry.sql
	vi) functional_area.sql
	vii) skill_set.sql
	viii) languages.sql
	ix) run all migration_*.sql

8) add required values in config/config.php

9) create folders inside usercreation/static/

   company_logo,profile_pics,resumes
   
10) Install and setup supervisor

	Installation steps:

		sudo apt-get install supervisor

	Setup:

		Open supervisor config file and add below lines. 

		sudo vim /etc/supervisor/supervisord.conf

			[inet_http_server]
			port = 9001
			username = job ; Basic auth username
			password = portal ; Basic auth password
	
		cd /etc/supervisor/conf.d
		sudo vim bulk_uploader.conf
		---------------------------------
		[program:bulk_upload_worker]
		command=/usr/bin/php5 path_to_bulkupload/worker.php
		directory=path_to_bulkupload
		autostart=true
		autorestart=true
		user=ubuntu
		----------------------------------
		sudo service supervisor restart

11) for all job search api
	go inside jobapi folder and run below command
	composer require jobapis/jobs-multi
	go to following file
		/home/ubuntu/job_portal/usercreation/jobapi/vendor/jobapis/jobs-multi/src/JobsMulti.php
	comment if condition from line 157 to 159
	comment line 450

