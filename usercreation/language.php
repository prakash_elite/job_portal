<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
    include_once("includes/redirect_IF_notadmin.php");

?>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	         <div class="add-new-button-wrapper">
	         	  <a id="add-new" class="mj_mainbtn mj_btnblue" data-text="Add New"><span>Add New</span></a>
	         </div>
             <div class="top_searchbox">
                <form>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-search"></i>
                            </div>
                             <select class="search-align"  name="search" id="search-all" >
                            </select>
                        </div>
                    </div>
                 </form>
             </div>
         </div>
         <div class="mj_candidate_section">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mj_tabcontent mj_bottompadder80 woo-cart-table">
                    <div class="table-responsive">
                       <table class="table table-striped" id="list-table"> 
                       <thead>
                                <tr>
                                    <th>Language</th>
                                    <th></th>
                                </tr>  
                       </thead>                
                            <tbody>
                            </tbody>
                       </table>
                    </div>
                    <div id='no-data-available'>No Data Available</div>
                   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4">
                        <div class="mj_showmore"> 
                        	<a id="show-more" class="mj_showmorebtn mj_blackbtn">Show More</a> 
                        </div>
                   </div>
                 </div>
             </div>
          </div>		
	</div>
</div>
            <!-- Modal Part -->
<div class="modal fade mj_popupdesign" id="submit-success" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2"> A<span>dd</span> L<span>anguage</span></h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
       					<div class="row">
                            <div class="mj_pricingtable mj_greentable mj_login_form_wrapper">
                                <form method="post" action="/languageopr/" class="register-form" class="form-horizontal" style="margin-top: 20px;">
                                    <div class="mj_login_form">
										<!-- Text input-->
										<div class="form-group">
										  <label class="pull-left">Language</label>  
										  <input id="language" name="name" required="required" placeholder="Language" class="form-control" type="text">
										 </div>
									<button id="update" name="update" class="hide"></button>
									</div>
	                                <div id="trigger-update" class="mj_pricing_footer">
	                                    <a >ADD</a>
	                                </div>
                            	</form>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="assets/images/close.png" alt="">
                            </button>
                        </div>
                     </div>
                   </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade mj_popupdesign" id="edit-submit-success" role="dialog" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2"> E<span>dit</span> L<span>anguage</span></h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
       					<div class="row">
                            <div class="mj_pricingtable mj_greentable mj_login_form_wrapper">
                                <form method="post" action="/languageopr/" class="register-form" class="form-horizontal" style="margin-top: 20px;">
                                    <div class="mj_login_form">
										<!-- Text input-->
										<div class="form-group">
										  <label class="pull-left">Language</label>  
										  <input id="language-edit" name="name" required="required" placeholder="Language" class="form-control" type="text">
										 </div>
									<button id="update-edit" name="update" class="hide"></button>
									</div>
	                                <div id="trigger-update-edit" class="mj_pricing_footer">
	                                    <a >UPDATE</a>
	                                </div>
                            	</form>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="assets/images/close.png" alt="">
                            </button>
                        </div>
                     </div>
                   </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/language.js" type="text/javascript"></script>   
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.language.init();
        });
    </script>
</body>

</html>
