<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
?>
<div class="mj_lightgraytbg mj_bottompadder80">
        <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mj_postdiv mj_shadow_yellow mj_postpage mj_toppadder50 mj_bottompadder50">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                           <form id="register-form" class="form-horizontal" enctype="multipart/form-data" action="/jobseekerprofile">
								<fieldset>
								<div class="col-md-6">
								<!-- Text input-->
									<div class="form-group">
									  <label>User Name</label>
									  <h5 id="error-username" class="error">Username already exists!!!</h5> 
									  <input id="name" name="name" placeholder="Username" class="form-control " required="" type="text">
									</div>

									<div class="form-group">
									  <label>Password</label>  
									  <input id="password" name="password" placeholder="Password" class="form-control " required="" type="password">
									</div>

									<div class="form-group">
									  <label>First Name</label>  
									  <input id="first_name" name="first_name" placeholder="First Name" class="form-control " required="" type="text">
									</div>

									<div class="form-group">
									  <label>Last Name</label>  
									  <input id="last_name" name="last_name" placeholder="Last Name" class="form-control " required="" type="text">
									</div>

									<div class="form-group">
									  <label>Email</label> 
									  <h5 id="error-email" class="error">Email already exists!!!</h5> 
									  <input id="email" name="email" placeholder="Email" class="form-control " required="" type="text">
									</div>
									<!-- Select Basic -->
		

									<!-- Text input-->
									<div class="form-group">
									  <label>Date of birth</label>  
									  <div class="mj_datepicker">
                                             <input id="DOB" name="DOB" placeholder="Date of birth" class="form-control " required="" type="text">
                                       </div>
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label>Contact number</label>  
									  <h5 id="error-contactnumber" class="error">Contact already exists!!!</h5> 
									  <input id="contact_number" name="contact_number" placeholder="Contact number" class="form-control " required="" type="text">
									</div>

									<div class="form-group">
									  <label>Address line 1</label>  
									  <input id="address_line_1" name="address_line_1" placeholder="Address line 1" class="form-control "  type="text">
									</div>
									<div class="form-group">
									  <label>Address line 2</label>  
									  <input id="address_line_2" name="address_line_2" placeholder="Address line 2" class="form-control "  type="text">
									</div>
									<div class="form-group">
									  <label>State</label>  
									  <input id="state" name="state" placeholder="state" class="form-control "  type="text">
									</div>
									<!-- Text input-->
									<div class="form-group">
									  <label>Zipcode</label> 
									  <input id="pincode" name="pincode" placeholder="Zipcode" class="form-control " type="text">
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label>City</label>  
									  <select id="city" name="city"  class="form-control ">
									  </select>
									   <p id="city-selected">None Selected</p>
									</div>

					

									<!-- Text input-->
									<div class="form-group">
									  <label>Job Title</label>  
									  <input id="current_designation" name="current_designation" placeholder="Job Title" class="form-control " type="text">
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label>Experience (in Months)</label>  
									  <input type="text" id="experience" name="experience" placeholder="Experience" class="form-control " min="0" type="number">
									</div>	
								</div>
								<div class="col-md-6">
									<div class="form-group">
									  <label>Current company</label>  
									  <input id="current_company" name="current_company" placeholder="Current company" class="form-control " type="text">
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label>Existing Salary</label>  
									  <input id="current_CTC" name="current_CTC" placeholder="Existing Salary" class="form-control " type="text">
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label>Expected Salary</label>  
									  <input id="expected_CTC" name="expected_CTC" placeholder="Expected Salary" class="form-control " type="text">
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label>Duration notice period(in months)</label>  
									  <input id="duration_notice_period" name="duration_notice_period" placeholder="Duration notice period" class="form-control " type="text">
									</div>
									<!-- Text input-->
									<div class="form-group">
									  <label>Skills</label>  
									 <select id="skills" name="skills[]"  class="form-control " >
									 </select> 
									 <p id="skills-selected">None Selected</p>
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label >Current Industry</label>  
									  <select id="current_industry" name="current_industry" class="form-control " >
									  </select>   
									 <p id="current_industry-selected">None Selected</p>
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label>Current functional area</label>  
									  <select id="current_functional_area" name="current_functional_area"  class="form-control " >
									  </select> 
									  <p id="current_functional_area-selected">None Selected</p>
									</div>
									<div class="form-group">
									  <label >Highest Education</label>  
									  <select id="highest_education" name="highest_education"  class="form-control " >
									  	<option value="1">High school</option>
									  	<option value="2">Bachelors</option>
									  	<option value="3">Graduate</option>
									  </select> 
									</div>
									<!-- Text input-->


									<!-- Text input-->
									<div class="form-group">
									  <label">University/School</label>  
									  <input id="university_or_college" name="university_or_college" placeholder="University/School" class="form-control " type="text">
									</div>

					

									<!-- Select Basic -->
									<div class="form-group">
									  <label>Desired employment type</label>
									    <select id="desired_employment_type" name="desired_employment_type" class="form-control">
									      <option value="1">Full time</option>
									      <option value="2">Contract</option>
									      <option value="3">Internship</option>
									    </select>
									</div>

									<!-- Select Basic -->
									<div class="form-group">
									  <label>Desired job type</label>
									    <select id="desired_job_type" name="desired_job_type" class="form-control">
									      <option value="1">Permanent</option>
									      <option value="2">Temporary</option>
									      <option value="3">Both</option>
									    </select>
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label>Prefered Location</label>  
									  <select id="prefered_location" name="prefered_location[]"  class="form-control " >
									  </select>
									  <p id="prefered_location-selected">None Selected</p>
									</div>
									<div class="form-group">
									  <label>Website Link (if any)</label>  
									  <input type="text" id="website_link" name="website_link"  class="form-control " >
									</div>
									<div class="form-group">
										<label id="resume-selected"><a>View/Download Resume</a></label>
									</div>
								  	<div class="form-group">
	                                    <a class="mj_mainbtn mj_btnblue" id="trigger-upload-resume" data-text="Upload resume "><span>Upload resume </span></a>
	                                     <input id="resume" name="resume" accept=".doc,.docx,.pdf" class="form-control hide" type="file">
                                	</div>
								</div>
								<button type="submit" id="update" name="update" class="hide">UPDATE</button>
							</form>
                        </div>
                    </div>
                    <div class="mj_showmore">
                    	<a id="trigger-profile-upload" class="mj_showmorebtn mj_bigbtn mj_yellowbtn">UPDATE</a>
                    </div>
                </div>
            </div>
<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/registernewjobseeker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.registernewjobseeker.init();
        });
    </script>
</body>

</html>