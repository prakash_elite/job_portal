<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<?php
    include_once("includes/head.php");
    include_once("includes/header.php");
    include_once("includes/redirectuser.php");
    include_once("includes/redirect_IF_not_admin_recruiter.php");

?>
<div class="mj_lightgraytbg mj_bottompadder80">
	<div class="container">
        	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h5>Assign Company</h5>
                    <div class="mj_postdiv mj_shadow_yellow mj_postpage mj_toppadder50 mj_bottompadder50">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                           <form id="register-form" class="new-line-fields form-horizontal" enctype="multipart/form-data" action="/assigncompany">
                             <?php if($userType==1) { ?>
                                <div class="form-group">
                                    <label class="col-xs-12 remove-left-padding">User</label>  
                                    <select id="users" required="required" name="user" class="form-control"></select> 
                                </div>   
                              <?php } ?>                       
                                <div class="form-group">
                                    <label>Company</label>  
                                    <select id="company" required="required" name="company"  class="form-control" >
                                    </select> 
                                </div>
                                <button type="submit" id="update" name="update" class="hide">ADD</button>
                            </form>
                        </div>
                    </div>
                    <div class="mj_showmore">
                        <a id="trigger-assign-comp" class="mj_showmorebtn mj_bigbtn mj_yellowbtn">ADD</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h5>Assign List</h5>
                    <div class="mj_postdiv mj_shadow_yellow mj_postpage">
                            <div class="mj_tabcontent woo-cart-table">
                                <div class="table-responsive" id="assign-company-wrapper">
                                   <table class="table table-striped" id="list-table"> 
                                   <thead>
                                            <tr>
                                                <th></th>
                                                <th>User name</th>
                                                <th>Company</th>
                                                <th></th>
                                            </tr>  
                                   </thead>                 
                                        <tbody>
                                        </tbody>
                                   </table>
                                </div>
                                <div id='no-data-available'>No Data Available</div>
                            </div>
                           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-lg-offset-4 col-md-offset-4">
                                <div class="mj_showmore"> 
                                    <a id="show-more" class="mj_showmorebtn mj_blackbtn">Show More</a> 
                                </div>
                           </div>
                    </div>
                </div>
       </div>
    </div>
<?php 
    include_once("includes/foot.php");
?>
    <script src="assets/js/assigncompany.js" type="text/javascript"></script>   
    <script type="text/javascript">
        $(document).ready(function(){
            jobportal.assigncompany.init();
        });
    </script>
</body>

</html>
